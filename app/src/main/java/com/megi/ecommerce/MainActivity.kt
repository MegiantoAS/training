package com.megi.ecommerce

import android.Manifest
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.OvershootInterpolator
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import androidx.core.os.LocaleListCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.snackbar.Snackbar
import com.megi.ecommerce.databinding.ActivityMainBinding
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.ui.main.home.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var sharedPref: SharedPreference
    private val navHost by lazy {
        supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
    }
    private val navController by lazy {
        navHost.navController
    }

    private val viewModel: HomeViewModel by viewModels()

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen().apply {
            setKeepOnScreenCondition {
                !viewModel.isReady.value
            }
            setOnExitAnimationListener { screen ->
                val zoomX = ObjectAnimator.ofFloat(
                    screen.iconView,
                    View.SCALE_X,
                    1f,
                    0.0f
                )
                zoomX.interpolator = OvershootInterpolator()
                zoomX.duration = 500L
                zoomX.doOnEnd { screen.remove() }

                val zoomY = ObjectAnimator.ofFloat(
                    screen.iconView,
                    View.SCALE_Y,
                    1f,
                    0.0f
                )
                zoomY.interpolator = OvershootInterpolator()
                zoomY.duration = 500L
                zoomY.doOnEnd { screen.remove() }

                zoomX.start()
                zoomY.start()
            }
        }
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = SharedPreference(this)

        checkDarkMode()
        checkLanguage()

        if (!isNotificationPermissionsGranted()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                pushNotificationPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    // check DarkMode running first
    private fun checkDarkMode() {
        AppCompatDelegate.setDefaultNightMode(
            if (sharedPref.getDarkMode()) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO
        )
    }

    // check language running first
    private fun checkLanguage() {
        val isLanguageIn = sharedPref.getLanguage()
        val languageToSet = if (isLanguageIn) "in" else "en"
        val languageToApply: LocaleListCompat = LocaleListCompat.forLanguageTags(languageToSet)

        AppCompatDelegate.setApplicationLocales(languageToApply)
    }

    // notification permission
    private val pushNotificationPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { granted ->
        if (granted) {
            Snackbar.make(binding.root, "Permintaan izin diterima", Snackbar.LENGTH_SHORT)
                .show()
        } else {
            Snackbar.make(binding.root, "Permintaan izin ditolak", Snackbar.LENGTH_SHORT).show()
        }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    private fun isNotificationPermissionsGranted() =
        ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.POST_NOTIFICATIONS
        ) == PackageManager.PERMISSION_GRANTED

    fun logOut() {
        sharedPref.logout()
        navController.navigate(R.id.action_main_to_prelog)
        clearDb()
    }

    private fun clearDb() {
        lifecycleScope.launch(Dispatchers.IO) {
            viewModel.clearDb()
        }
    }

    fun toDetailProduct(bundle: Bundle) {
        navController.navigate(R.id.action_main_to_detailproductFragment, bundle)
    }

    fun toViewReview(bundle: Bundle) {
        navController.navigate(R.id.action_detailProductFragment_to_reviewFragmentCompose, bundle)
    }

    fun cartToDetail(bundle: Bundle) {
        navController.navigate(R.id.action_cartFragment_to_detailFragment, bundle)
    }

    fun detailToCheckout(bundle: Bundle) {
        navController.navigate(R.id.action_detailProductFragment_to_checkoutFragment, bundle)
    }

    fun goToSuccessPayment(bundle: Bundle) {
        navController.navigate(R.id.action_mainTransaction_to_successFragment, bundle)
    }
}
