package com.megi.ecommerce.ui.main.store.review

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.GetReview
import com.megi.ecommerce.repository.StoreRepository
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(
    private val storeRepository: StoreRepository,
    preference: SharedPreference,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val productId = savedStateHandle.get<String>("id_product")
    private val accessToken = preference.getAccessToken() ?: throw Exception("null token")
    private val _dataReview = MutableLiveData<ResultResponse<GetReview>>(null)

    val dataReview: LiveData<ResultResponse<GetReview>> = _dataReview

    init {
        getReviewData()
    }

    fun getReviewData() {
        viewModelScope.launch {
            storeRepository.getReviewProduct(accessToken, productId.toString()).observeForever {
                _dataReview.value = it
            }
        }
    }
}
