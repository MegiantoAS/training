package com.megi.ecommerce.ui.main.wishlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.repository.WishlistRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WishListViewModel @Inject constructor(
    private val wishlistRepository: WishlistRepository,
    private val cartRepository: CartRepository
) :
    ViewModel() {

    var isList: Boolean = true
    fun addToCart(
        productId: String,
        productName: String,
        productPrice: Int,
        image: String,
        store: String,
        sale: Int,
        stock: Int?,
        rating: Int,
        productRating: Float,
        variantName: String,
        variantPrice: Int?,
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val productItem = ProductLocalDb(
                productId,
                productName,
                productPrice,
                image,
                "",
                "",
                store,
                sale,
                stock,
                rating,
                0,
                0,
                productRating,
                variantName,
                variantPrice
            )
            cartRepository.addToCart(productItem)
        }
    }

    fun getWishlistProduct(): LiveData<List<WishlistProduct>> {
        return wishlistRepository.getWishlistProduct()
    }

    fun deleteWishList(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            wishlistRepository.deleteWishList(id)
        }
    }

    // cart
    fun getCartById(id: String): ProductLocalDb? {
        return cartRepository.getCartById(id)
    }

    fun updateCartItemQuantity(productId: String, newQuantity: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            cartRepository.updateCartItemQuantity(productId, newQuantity)
        }
    }
}
