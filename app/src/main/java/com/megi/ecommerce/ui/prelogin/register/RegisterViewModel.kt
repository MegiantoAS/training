package com.megi.ecommerce.ui.prelogin.register

import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.network.Auth
import com.megi.ecommerce.repository.PreLoginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(private val repository: PreLoginRepository) :
    ViewModel() {
    fun doRegister(token: String, auth: Auth) = repository.register(token, auth)
}
