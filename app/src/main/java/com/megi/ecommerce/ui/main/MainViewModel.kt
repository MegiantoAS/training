package com.megi.ecommerce.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.repository.NotificationRepository
import com.megi.ecommerce.repository.WishlistRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val cartRepository: CartRepository,
    private val wishlistRepository: WishlistRepository,
    private val notificationRepository: NotificationRepository,
    private val pref: SharedPreference,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    var newData = savedStateHandle.get<Boolean>("newData") ?: false

    fun getCartItem(): LiveData<List<ProductLocalDb>> {
        return cartRepository.getCartItem()
    }

    fun getWishlistProduct(): LiveData<List<WishlistProduct>> {
        return wishlistRepository.getWishlistProduct()
    }

    fun getAllNotification(): LiveData<List<NotificationEcommerce>>? {
        return runBlocking {
            withContext(Dispatchers.IO) {
                notificationRepository.getAllNotification()
            }
        }
    }

    fun getAccessToken(): String? {
        return pref.getAccessToken()
    }

    fun getNameProfile(): String? {
        return pref.getNameProfile()
    }
}
