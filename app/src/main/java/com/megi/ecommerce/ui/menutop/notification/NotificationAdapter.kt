package com.megi.ecommerce.ui.menutop.notification

import android.content.res.Configuration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.ItemNotifBinding
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce

class NotificationAdapter :
    ListAdapter<NotificationEcommerce, NotificationAdapter.NotificationViewHolder>(
        NotificationDiffCallback()
    ) {
    private var onItemClickCallback: OnItemClickCallback? = null

    interface OnItemClickCallback {
        fun onItemClick(position: NotificationEcommerce)
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        return NotificationViewHolder(
            ItemNotifBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).root
        )
    }

    override fun onBindViewHolder(
        holder: NotificationAdapter.NotificationViewHolder,
        position: Int
    ) {
        val item = getItem(position)
        holder.bind(item)
    }

    inner class NotificationViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        private val binding = ItemNotifBinding.bind(view)
        fun bind(item: NotificationEcommerce) {
            binding.apply {
                tvType.text = item.type
                tvDate.text = item.date
                tvTitle.text = item.title
                tvBody.text = item.body
                Glide.with(binding.root.context)
                    .load(item.image)
                    .into(ivNotification)
            }

            if (item.isRead) {
                when (itemView.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                    Configuration.UI_MODE_NIGHT_YES ->
                        binding.notificationListItem.setBackgroundColor(
                            itemView.resources.getColor(
                                R.color.black,
                                null
                            )
                        )

                    else -> {
                        binding.notificationListItem.setBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                android.R.color.white
                            )
                        )
                    }
                }
            } else {
                when (itemView.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                    Configuration.UI_MODE_NIGHT_YES ->
                        binding.notificationListItem.setBackgroundColor(
                            itemView.resources.getColor(
                                R.color.purple,
                                null
                            )
                        )
                }
            }

            binding.root.setOnClickListener {
                onItemClickCallback?.onItemClick(item)
            }
        }
    }

    class NotificationDiffCallback : DiffUtil.ItemCallback<NotificationEcommerce>() {
        override fun areItemsTheSame(
            oldItem: NotificationEcommerce,
            newItem: NotificationEcommerce,
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: NotificationEcommerce,
            newItem: NotificationEcommerce,
        ): Boolean {
            return oldItem == newItem
        }
    }
}
