package com.megi.ecommerce.ui.main.store.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideIn
import androidx.compose.foundation.Image
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.R
import com.megi.ecommerce.datasource.network.GetProductDetailItemResponse
import com.megi.ecommerce.datasource.network.GetProductReviewItemResponse
import com.megi.ecommerce.ui.main.store.theme.EcommerceTheme
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class ReviewProductCompose : Fragment() {

    private val viewModel: ReviewViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                EcommerceTheme {
                    val productDetailReviewState = viewModel.dataReview.observeAsState()
                    val result = productDetailReviewState.value

                    var isVisible by remember { mutableStateOf(false) }
                    var data:  List<GetProductReviewItemResponse>? = null

                    when (result) {
                        is ResultResponse.Success -> {
                            data = result.data.data
                            isVisible = true
                        }

                        is ResultResponse.Loading -> {
                            LoadingScreen()
                        }

                        is ResultResponse.Error -> {
                            ErrorLayout(result.exception)
                        }

                        else -> {}
                    }

                    AnimatedVisibility(
                        visible = isVisible,
                        enter = slideIn(
                            animationSpec = tween(durationMillis = 600),
                            initialOffset = { fullSize -> IntOffset(0, -fullSize.height) }
                        )
                    ) {
                        ReviewProduct(data!!, findnavControler = findNavController())
                    }
                }
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun ErrorLayout(t: Throwable) {
        val poppins = FontFamily(
            Font(R.font.poppins_medium, FontWeight.Medium),
            Font(R.font.poppins_semibold, FontWeight.SemiBold),
            Font(R.font.poppins_bold, FontWeight.Bold)
        )
        Scaffold(
            // top bar error
            topBar = {
                TopAppBar(
                    title = {
                        Text(
                            text = stringResource(id = R.string.txt_reviewBar),
                            fontFamily = poppins,
                            fontWeight = FontWeight.Medium,
                            fontSize = 22.sp
                        )
                    },
                    navigationIcon = {
                        IconButton(
                            onClick = {
                                findNavController().navigateUp()
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Default.ArrowBack,
                                contentDescription = null
                            )
                        }
                    }
                )
                Divider(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 62.dp)
                )
            },

            // content
            content = { innerPadding ->
                Column(
                    modifier = Modifier
                        .padding(innerPadding)
                        .fillMaxSize(),
                    verticalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    EcommerceTheme {
                        Column(
                            Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Column(
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Image(
                                    painter = painterResource(id = R.drawable.error_img),
                                    contentDescription = "error"
                                )
                                when (t) {
                                    is retrofit2.HttpException -> {
                                        if (t.response()?.code() == 404) {
                                            ErrorContent(
                                                code = stringResource(id = R.string.txt_empty),
                                                message = stringResource(id = R.string.no_data),
                                                buttonText = stringResource(id = R.string.txt_refresh)
                                            )
                                        } else if (t.response()?.code() == 401) {
                                            ErrorContent(
                                                code = t.response()?.code().toString(),
                                                message = t.response()?.message().toString(),
                                                buttonText = stringResource(id = R.string.txt_refresh)
                                            )
                                            (requireActivity() as MainActivity).logOut()
                                        } else {
                                            ErrorContent(
                                                code = t.response()?.code().toString(),
                                                message = t.response()?.message().toString(),
                                                buttonText = stringResource(id = R.string.txt_refresh)
                                            )
                                        }
                                    }

                                    is IOException -> {
                                        ErrorContent(
                                            code = stringResource(R.string.txt_conenction),
                                            message = stringResource(R.string.not_connection),
                                            buttonText = stringResource(id = R.string.txt_refresh)
                                        )
                                    }

                                    else -> {
                                        ErrorContent(
                                            code = stringResource(R.string.internal_error),
                                            message = t.message.toString(),
                                            buttonText = stringResource(id = R.string.txt_refresh)
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        )
    }

    @Composable
    fun ErrorContent(code: String, message: String, buttonText: String) {
        val poppins = FontFamily(
            Font(R.font.poppins_medium, FontWeight.Medium),
            Font(R.font.poppins_semibold, FontWeight.SemiBold),
            Font(R.font.poppins_bold, FontWeight.Bold)
        )
        Text(
            text = code,
            modifier = Modifier
                .padding(top = 8.dp),
            style = TextStyle(
                fontFamily = poppins,
                fontWeight = FontWeight.Medium,
                fontSize = 32.sp,
            )
        )
        Text(
            text = message,
            modifier = Modifier
                .padding(top = 4.dp),
            fontFamily = poppins,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp,
        )
        Button(
            onClick = {
                viewModel.getReviewData()
            },
            modifier = Modifier.padding(top = 8.dp)
        ) {
            Text(
                text = buttonText,
                fontFamily = poppins
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ReviewProduct(listReview: List<GetProductReviewItemResponse>, findnavControler: NavController) {
    val poppins = FontFamily(
        Font(R.font.poppins_regular, FontWeight.Medium),
        Font(R.font.poppins_semibold, FontWeight.SemiBold),
        Font(R.font.poppins_bold, FontWeight.Bold)
    )
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = stringResource(id = R.string.txt_reviewBar),
                        fontFamily = poppins,
                        fontWeight = FontWeight.Medium,
                        fontSize = 22.sp
                    )
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            findnavControler.navigateUp()
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = null
                        )
                    }
                }
            )
            Divider(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 62.dp)
            )
        }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            EcommerceTheme {
                Review(listReview)
            }
        }
    }
}

@Composable
fun Review(listReview: List<GetProductReviewItemResponse>) {
    LazyColumn(Modifier.fillMaxSize()) {
        items(listReview) { reviewItem ->
            ListItemReview(reviewItem)
        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun ListItemReview(
    data: GetProductReviewItemResponse
) {
    val poppins = FontFamily(
        Font(R.font.poppins_regular, FontWeight.Medium),
        Font(R.font.poppins_semibold, FontWeight.SemiBold),
        Font(R.font.poppins_bold, FontWeight.Bold)
    )
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = 16.dp)
            .padding(horizontal = 16.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(bottom = 6.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            GlideImage(
                model = "data.userImage",
                contentDescription = null,
                modifier = Modifier
                    .size(40.dp)
                    .clip(
                        CircleShape
                    )
            ) {
                it.load(data.userImage).error(R.drawable.product_img)
            }
            Column(
                modifier = Modifier
                    .weight(1f)
                    .padding(start = 8.dp)
            ) {
                Text(
                    text = data.userName,
                    fontFamily = poppins,
                    style = MaterialTheme.typography.titleMedium,
                )
                Row {
                    repeat(times = 5) { iteration ->
                        Icon(
                            imageVector = Icons.Default.Star,
                            contentDescription = null,
                            tint = if (data.userRating > iteration) {
                                MaterialTheme.colorScheme.onPrimaryContainer
                            } else {
                                MaterialTheme.colorScheme.surfaceVariant
                            },
                            modifier = Modifier.size(12.dp)
                        )
                    }
                }
            }
        }
        Text(
            text = data.userReview,
            fontFamily = poppins,
            fontWeight = FontWeight.Normal,
            fontSize = 12.sp,
            color = if (isSystemInDarkTheme()) darkColorScheme().onBackground else lightColorScheme().onBackground,
        )
    }
    Divider(
        color = Color.LightGray,
        thickness = 1.dp,
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
fun LoadingScreen() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier
                .size(48.dp),
            strokeWidth = 4.dp
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ReviewPreview() {
    EcommerceTheme {
    }
}
