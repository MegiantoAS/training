package com.megi.ecommerce.ui.main.transaction.payment

import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.SharedPreference
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(
    private val pref: SharedPreference
) : ViewModel() {
    fun getAccessToken(): String? {
        return pref.getAccessToken()
    }
}
