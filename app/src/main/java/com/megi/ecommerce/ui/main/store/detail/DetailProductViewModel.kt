package com.megi.ecommerce.ui.main.store.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import com.megi.ecommerce.datasource.network.GetProductDetailResponse
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.repository.StoreRepository
import com.megi.ecommerce.repository.WishlistRepository
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailProductViewModel @Inject constructor(
    private val storeRepository: StoreRepository,
    private val cartRepository: CartRepository,
    private val wishlistRepository: WishlistRepository,
    pref: SharedPreference,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val productId = savedStateHandle.get<String>("id_product")
    private val accessToken = pref.getAccessToken() ?: throw IllegalArgumentException("null token")
    private val _dataDetail = MutableLiveData<ResultResponse<GetProductDetailResponse>>(null)
    val dataDetail: LiveData<ResultResponse<GetProductDetailResponse>> = _dataDetail

    init {
        getDetailData()
    }

    fun getDetailData() {
        viewModelScope.launch {
            storeRepository.getProductDetail(accessToken, productId.toString()).observeForever {
                _dataDetail.value = it
            }
        }
    }

    // wishList
    fun addToWishList(
        productId: String,
        productName: String,
        productPrice: Int,
        image: String,
        store: String,
        sale: Int,
        stock: Int?,
        rating: Int,
        productRating: Float,
        variantName: String,
        variantPrice: Int?,
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val wishlistItem = WishlistProduct(
                productId,
                productName,
                productPrice,
                image,
                "",
                "",
                store,
                sale,
                stock,
                rating,
                0,
                0,
                productRating,
                variantName,
                variantPrice
            )
            wishlistRepository.addWishList(wishlistItem)
        }
    }

    fun getProductWishlistById(id: String): WishlistProduct? {
        return wishlistRepository.getProductWishlistById(id)
    }

    fun deleteWishList(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            wishlistRepository.deleteWishList(id)
        }
    }

    // cart
    fun getCartById(id: String): ProductLocalDb? {
        return cartRepository.getCartById(id)
    }

    fun updateCartItemQuantity(productId: String, newQuantity: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            cartRepository.updateCartItemQuantity(productId, newQuantity)
        }
    }

    fun addToCart(
        productId: String,
        productName: String,
        productPrice: Int,
        image: String,
        store: String,
        sale: Int,
        stock: Int?,
        rating: Int,
        productRating: Float,
        variantName: String,
        variantPrice: Int?,
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val productItem = ProductLocalDb(
                productId,
                productName,
                productPrice,
                image,
                "",
                "",
                store,
                sale,
                stock,
                rating,
                0,
                0,
                productRating,
                variantName,
                variantPrice
            )
            cartRepository.addToCart(productItem)
        }
    }
}
