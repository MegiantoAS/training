package com.megi.ecommerce.ui.main.store.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentStoreBinding
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.GetProductsItemsResponse
import com.megi.ecommerce.ui.main.store.StoreLoadingAdapter
import com.megi.ecommerce.ui.main.store.filter.StoreAdapter
import com.megi.ecommerce.ui.main.store.filter.StoreBottomSheet
import com.megi.ecommerce.ui.main.store.filter.Type
import com.megi.ecommerce.ui.main.store.search.SearchDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import java.text.NumberFormat
import java.util.Locale

@Suppress("DEPRECATION")
@AndroidEntryPoint
class StoreFragment : Fragment() {
    private var _binding: FragmentStoreBinding? = null
    private val binding get() = _binding
    private lateinit var adapter: StoreAdapter
    private val viewModel: StoreViewModel by viewModels()
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var sharedPref: SharedPreference
    private var dataChip = mutableListOf<String>()
    private var search: String? = null
    private var sort: String? = null
    private var category: String? = null
    private var lowest: String? = null
    private var highest: String? = null
//    private var isList: Boolean = true
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
        sharedPref = SharedPreference(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentStoreBinding.inflate(inflater, container, false)
        return binding?.root
    }

    @SuppressLint("SuspiciousIndentation")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refreshPage()

        adapter = StoreAdapter(requireContext())
        setChip()
        getProduct()
        loadDataState()
        showBottomDialog()
        binding?.swipeRefresh?.setOnRefreshListener {
            adapter.refresh()
            binding!!.swipeRefresh.isRefreshing = false
        }

        toSearch()
        setSearch()
        binding?.ivLayout?.setOnClickListener {
            viewModel.isList = !viewModel.isList
            changeToggle()
        }

        binding?.restartButton?.setOnClickListener {
            refreshAfterError()
        }
    }

    // get list product
    private fun getProduct() {
        binding?.apply {
            adapter = StoreAdapter(requireContext())

            viewModel.products.observe(viewLifecycleOwner) { paging ->
                adapter.submitData(viewLifecycleOwner.lifecycle, paging)
                var bundle = arrayOf(bundleOf())

                changeToggle()
                adapter.snapshot().items.forEach {
                    bundle += bundleOf(
                        FirebaseAnalytics.Param.ITEM_ID to it.productId,
                        FirebaseAnalytics.Param.ITEM_BRAND to it.brand,
                        FirebaseAnalytics.Param.ITEM_NAME to it.productName,
                        FirebaseAnalytics.Param.PRICE to it.productPrice
                    )
                }

                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST) {
                    param(FirebaseAnalytics.Param.ITEMS, bundle)
                }
            }

            gridLayoutManager = GridLayoutManager(requireContext(), 1)
            rvProduct.layoutManager = gridLayoutManager
            rvProduct.adapter = adapter

            adapter.setOnItemClickCallback(object : StoreAdapter.OnItemClickCallback {
                override fun onItemClicked(data: GetProductsItemsResponse) {
                    val bundle = bundleOf("id_product" to data.productId)
                    (requireActivity() as MainActivity).toDetailProduct(bundle)

                    // firebase analytic to detail
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                        param(FirebaseAnalytics.Param.ITEM_LIST_ID, data.productId)
                        param(FirebaseAnalytics.Param.ITEM_NAME, data.productName)
                    }
                }
            })
            loadingFooter()
        }
    }

    private fun loadDataState() {
        adapter.addLoadStateListener {
            when (it.refresh) {
                is LoadState.Loading -> {
                    setShimmerLayout()
                    binding?.shimmerFilter?.visibility = View.VISIBLE
                    binding?.swipeRefresh?.visibility = View.INVISIBLE
                    binding?.linearLayout?.visibility = View.INVISIBLE
                    binding?.linearErrorLayout?.visibility = View.INVISIBLE
                }

                is LoadState.Error -> {
                    setOffShimmerLayout()
                    binding?.shimmerFilter?.visibility = View.INVISIBLE
                    binding?.linearErrorLayout?.visibility = View.VISIBLE
                    binding?.linearLayout?.visibility = View.GONE
                    val error = (it.refresh as LoadState.Error).error
                    setErrorMessage(error)
                }

                is LoadState.NotLoading -> {
                    setOffShimmerLayout()
                    binding?.shimmerFilter?.visibility = View.INVISIBLE
                    binding?.swipeRefresh?.visibility = View.VISIBLE
                    binding?.linearLayout?.visibility = View.VISIBLE
                    binding?.linearErrorLayout?.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun setShimmerLayout() {
        if (viewModel.isList) {
            binding?.shimmerLinear?.visibility = View.VISIBLE
        } else {
            binding?.shimmerGrid?.visibility = View.VISIBLE
        }
    }
//
    private fun setOffShimmerLayout() {
        if (viewModel.isList) {
            binding?.shimmerLinear?.visibility = View.GONE
        } else {
            binding?.shimmerGrid?.visibility = View.GONE
        }
    }


    private fun setErrorMessage(error: Throwable) {
        when (error) {
            is HttpException -> {
                if (error.response()?.code() == 404) {
                    binding?.errorTypeText?.text = getString(R.string.txt_empty)
                    binding?.errorTypeInfo?.text = getString(R.string.no_data)
                    binding?.restartButton?.text = getString(R.string.txt_reset)
                } else if (error.response()?.code() == 401) {
                    binding?.errorTypeText?.text = error.response()?.code().toString()
                    binding?.errorTypeInfo?.text = error.response()?.message()
                    (requireActivity() as MainActivity).logOut()
                    clearDb()
                } else {
                    binding?.errorTypeText?.text = error.response()?.code().toString()
                    binding?.errorTypeInfo?.text = error.response()?.message()
                    binding?.restartButton?.text = getString(R.string.txt_refresh)
                }
            }

            is IOException -> {
                binding?.errorTypeText?.text = getString(R.string.txt_conenction)
                binding?.errorTypeInfo?.text = getString(R.string.not_connection)
                binding?.restartButton?.text = getString(R.string.txt_refresh)
            }

            else -> {
                binding?.errorTypeText?.text = getString(R.string.internal_error)
                binding?.errorTypeInfo?.text = error.message
                binding?.restartButton?.text = getString(R.string.txt_logout)
            }
        }
    }

    private fun refreshAfterError() {
        binding?.linearErrorLayout?.visibility = View.INVISIBLE
        if (binding?.restartButton?.text == getString(R.string.txt_reset)) {
            binding?.chipGroup?.removeAllViews()
            viewModel.resetParam()
            binding?.fieldSearch?.text?.clear()
        } else if (binding?.restartButton?.text == getString(R.string.txt_logout)) {
            logOut()
        }

        adapter.refresh()
        setShimmerLayout()
        binding?.shimmerFilter?.visibility = View.VISIBLE
        binding?.swipeRefresh?.visibility = View.INVISIBLE
        binding?.linearLayout?.visibility = View.INVISIBLE
    }

    private fun logOut() {
        binding?.apply {
            Firebase.messaging.unsubscribeFromTopic("promo")
            firebaseAnalytics.logEvent("btn_logout_clicked", null)
            (requireActivity() as MainActivity).logOut()
            clearDb()
        }
    }

    private fun clearDb() {
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            viewModel.clearDb()
        }
    }

    private fun loadingFooter() {
        val footerAdapter = StoreLoadingAdapter { adapter.retry() }
        binding?.rvProduct?.adapter = adapter.withLoadStateFooter(footer = footerAdapter)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position < adapter.itemCount) {
                    1
                } else {
                    gridLayoutManager.spanCount
                }
            }
        }
    }

    // dialog bottom
    private fun showBottomDialog() {
        binding?.apply {
            chipFilter.setOnClickListener {
                firebaseAnalytics.logEvent("FilterChip_Clicked", null)
                val bottomSheet = StoreBottomSheet.newInstance(
                    translateChipText(category ?: "") ?: category.toString(),
                    translateChipText(sort ?: "") ?: sort.toString(),
                    translateChipText(highest ?: "") ?: highest.toString(),
                    translateChipText(lowest ?: "") ?: lowest.toString()
                )
                bottomSheet.show(parentFragmentManager, StoreBottomSheet.TAG)
            }
        }
    }

    // setup chip
    private fun setChip() {
        binding.apply {
            adapter = StoreAdapter(requireContext())
            setFragmentResultListener(FILTER) { _, bundle ->
                sort = bundle.getString(CHIP_SORT)
                category = bundle.getString(CHIP_CATEGORY)
                lowest = bundle.getString(CHIP_LOWEST)
                highest = bundle.getString(CHIP_HIGHEST)
                viewModel.queryProduct(
                    category,
                    lowest?.toIntOrNull(),
                    highest?.toIntOrNull(),
                    sort
                )
            }
            viewModel.param.observe(viewLifecycleOwner) { it ->
                it?.let { params ->
                    sort = params.sort
                    category = params.brand
                    lowest = params.lowest?.let { formatCurrency(it.toDouble(), ">") }
                    highest = params.highest?.let { formatCurrency(it.toDouble(), "<") }
                    dataChip.clear()

                    category?.let { translatedCategory ->
                        translateChipText(translatedCategory)?.let { translatedText ->
                            dataChip.add(translatedText)
                        } ?: dataChip.add(translatedCategory)
                    }

                    sort?.let { translatedSort ->
                        translateChipText(translatedSort)?.let { translatedText ->
                            dataChip.add(translatedText)
                        } ?: dataChip.add(translatedSort)
                    }

                    lowest?.let { dataChip.add(it) }
                    highest?.let { dataChip.add(it) }
                    intentChip(dataChip)
                }
            }
        }
    }

    //    change language
    private fun translateChipText(chipId: String): String? {
        val translationMapEn = mapOf(
            "Ulasan" to "Review",
            "Penjualan" to "Sale",
            "Harga terendah" to "Lowest price",
            "Harga tertinggi" to "Highest price",
        )

        val translationMapIn = mapOf(
            "Review" to "Ulasan",
            "Sale" to "Penjualan",
            "Lowest price" to "Harga terendah",
            "Highest price" to "Harga tertinggi",
        )

        val isLanguageIn = sharedPref.getLanguage()
        val translationMap = if (isLanguageIn) translationMapIn else translationMapEn
        return translationMap[chipId]
    }

    private fun formatCurrency(value: Double, prefix: String): String {
        val currencyFormat = NumberFormat.getNumberInstance(Locale("id", "ID"))
        val formattedValue = currencyFormat.format(value)
        return "$prefix Rp$formattedValue"
    }

    private fun intentChip(names: List<String>) {
        binding?.apply {
            chipGroup.removeAllViewsInLayout()
            for (name in names) {
                val chip = Chip(requireContext())
                chip.apply {
                    text = name
                    isChipIconVisible = false
                    isCloseIconVisible = false
                }
                chipGroup.addView(chip as View)
            }
        }
    }

    private fun toSearch() {
        binding?.apply {
            fieldSearch.setOnClickListener {
                if (search == null) {
                    viewModel.param.observe(viewLifecycleOwner) {
                        search = it.search
                    }
                }
                val fragmentManager = requireActivity().supportFragmentManager
                val newFragment = SearchDialogFragment.newInstance(
                    search.toString()
                )
                val transaction = fragmentManager.beginTransaction()
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                transaction
                    .add(android.R.id.content, newFragment)
                    .addToBackStack(null)
                    .commit()
                newFragment.show(parentFragmentManager, SearchDialogFragment.TAG)
            }
        }
    }

    private fun setSearch() {
        requireActivity().supportFragmentManager.setFragmentResultListener(
            FILTER,
            viewLifecycleOwner
        ) { _, bundle ->
            search = bundle.getString(SEARCH)
            binding?.fieldSearch?.setText(search)
            viewModel.setSearch(search)
        }
    }

    // toggle linear or grid
    private fun changeToggle() {
//        isList = !isList
        adapter.currentViewType = if (viewModel.isList) Type.LINEAR else Type.GRID

        val imageRes = ContextCompat.getDrawable(
            requireContext(),
            if (viewModel.isList) R.drawable.ic_list_bulleted else R.drawable.ic_grid
        )
        binding?.ivLayout?.setImageDrawable(imageRes)

        gridLayoutManager.spanCount = if (viewModel.isList) 1 else 2
    }

    // refresh page
    private fun refreshPage() {
        binding?.apply {
            swipeRefresh.setOnRefreshListener {
                binding?.swipeRefresh?.isRefreshing = false
            }
        }
    }

    companion object {
        const val CHIP_SORT = "chipSort"
        const val CHIP_CATEGORY = "chipCategory"
        const val CHIP_LOWEST = "chipLowest"
        const val CHIP_HIGHEST = "chipHighest"
        const val FILTER = "filter"
        const val SEARCH = "search"
    }
}
