package com.megi.ecommerce.ui.main.wishlist

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.ItemWishlistGridBinding
import com.megi.ecommerce.databinding.ItemWishlistLinearBinding
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import com.megi.ecommerce.helper.CurrencyUtils

enum class ViewType {
    LINEAR, GRID
}

class WishListAdapter(
    private val context: Context,
    private val wishListViewModel: WishListViewModel,
    private val firebaseAnalytics: FirebaseAnalytics
) : ListAdapter<WishlistProduct, RecyclerView.ViewHolder>(WishlistComparator) {
    var items = true
    var currentType = ViewType.LINEAR
    private var onItemClickCallback: OnItemClickCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (getItemViewType(viewType)) {
            1 -> {
                val binding = ItemWishlistLinearBinding.inflate(layoutInflater, parent, false)
                LinearViewHolder(binding)
            }

            0 -> {
                val binding = ItemWishlistGridBinding.inflate(layoutInflater, parent, false)
                GridViewHolder(binding)
            }

            else -> {
                throw IllegalArgumentException("Invalid ViewType")
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = getItem(position)
        if (data != null) {
            when (holder) {
                is LinearViewHolder -> {
                    holder.bind(data)
                }

                is GridViewHolder -> {
                    holder.bind(data)
                }
            }
        }
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    override fun getItemViewType(position: Int): Int {
        return when (currentType) {
            ViewType.LINEAR -> 1
            ViewType.GRID -> 0
        }
    }

    inner class LinearViewHolder(private val binding: ItemWishlistLinearBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: WishlistProduct) {
            items = true
            binding.apply {
                Glide.with(context)
                    .load(product.image)
                    .into(ivProduct)
                tvProductName.text = product.productName
                val sum = product.productPrice.plus(product.variantPrice!!)
                tvProductPrice.text = CurrencyUtils.formatRupiah(sum)
                tvStore.text = product.store
                val soldString = itemView.context.getString(R.string.sold)
                val ratingText = "${product.productRating} | $soldString ${product.sale}"
                tvRating.text = ratingText
//                tvSell.text = product.sale.toString()
                binding.root.setOnClickListener {
                    onItemClickCallback?.onItemClickCard(product.productId)
                }
                btnAddToCart.setOnClickListener {
                    onItemClickCallback?.onItemClick(product)
                    cardWishlist.startAnimation(
                        AnimationUtils.loadAnimation(
                            itemView.context,
                            R.anim.scale_up
                        )
                    )
                }
                btnDelete.setOnClickListener {
                    firebaseAnalytics.logEvent("delete_wishlist_clicked", null)
                    wishListViewModel.deleteWishList(product.productId)

                    val snackBar = Snackbar.make(
                        binding.root,
                        R.string.sndelete_wishlist,
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snackBar.show()

                    Handler(Looper.getMainLooper()).postDelayed({
                        snackBar.dismiss()
                    }, 2000)
                }

                cardWishlist.startAnimation(
                    AnimationUtils.loadAnimation(
                        itemView.context,
                        R.anim.slide_in
                    )
                )
            }

        }
    }

    inner class GridViewHolder(private val binding: ItemWishlistGridBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: WishlistProduct) {
            items = true
            binding.apply {
                Glide.with(context)
                    .load(product.image)
                    .into(ivProduct)
                tvProductName.text = product.productName
                val sum = product.productPrice.plus(product.variantPrice!!)
                tvProductPrice.text = CurrencyUtils.formatRupiah(sum)
                tvStore.text = product.store
                val soldString = itemView.context.getString(R.string.sold)
                val ratingText = "${product.productRating} | $soldString ${product.sale}"
                tvRating.text = ratingText
                binding.root.setOnClickListener {
                    onItemClickCallback?.onItemClickCard(product.productId)
                }
                btnAddToCart.setOnClickListener {
                    onItemClickCallback?.onItemClick(product)
                }
                btnDelete.setOnClickListener {
                    wishListViewModel.deleteWishList(product.productId)
                }

                cardGridWish.startAnimation(
                    AnimationUtils.loadAnimation(
                        itemView.context,
                        if (absoluteAdapterPosition % 2 == 0) {
                            R.anim.slide_in
                        } else {
                            R.anim.fall_down
                        }
                    )
                )
            }
        }
    }

    interface OnItemClickCallback {
        fun onItemClick(position: WishlistProduct)
        fun onItemClickCard(data: String)
    }
}

object WishlistComparator : DiffUtil.ItemCallback<WishlistProduct>() {
    override fun areItemsTheSame(
        prevItem: WishlistProduct,
        nextItem: WishlistProduct,
    ): Boolean {
        return prevItem.productId == nextItem.productId
    }

    override fun areContentsTheSame(
        prevItem: WishlistProduct,
        nextItem: WishlistProduct,
    ): Boolean {
        return prevItem == nextItem
    }
}
