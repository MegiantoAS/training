package com.megi.ecommerce.ui.main.store.review

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.megi.ecommerce.databinding.ItemProductReviewBinding
import com.megi.ecommerce.datasource.network.GetProductReviewItemResponse

class ReviewProductAdapter :
    ListAdapter<GetProductReviewItemResponse, ReviewProductAdapter.ProductReviewViewHolder>(
        ProductReviewDiffCallback()
    ) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ReviewProductAdapter.ProductReviewViewHolder {
        val binding =
            ItemProductReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductReviewViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: ReviewProductAdapter.ProductReviewViewHolder,
        position: Int
    ) {
        val item = getItem(position)
        holder.bind(item)
    }

    inner class ProductReviewViewHolder(private val binding: ItemProductReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: GetProductReviewItemResponse) {
            binding.apply {
                tvName.text = item.userName
                tvReview.text = item.userReview
                tvRating.rating = item.userRating.toFloat()
                Glide.with(root.context)
                    .load(item.userImage)
                    .into(imageReview)
            }
        }
    }
}

class ProductReviewDiffCallback : DiffUtil.ItemCallback<GetProductReviewItemResponse>() {
    override fun areItemsTheSame(
        oldItem: GetProductReviewItemResponse,
        newItem: GetProductReviewItemResponse,
    ): Boolean {
        return oldItem.userName == newItem.userName
    }

    override fun areContentsTheSame(
        oldItem: GetProductReviewItemResponse,
        newItem: GetProductReviewItemResponse,
    ): Boolean {
        return oldItem == newItem
    }
}
