package com.megi.ecommerce.ui.main.store.filter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.ItemProductGridBinding
import com.megi.ecommerce.databinding.ItemProductLinearBinding
import com.megi.ecommerce.datasource.network.GetProductsItemsResponse
import com.megi.ecommerce.helper.CurrencyUtils

enum class Type {
    LINEAR, GRID
}

class StoreAdapter(private val context: Context) :
    PagingDataAdapter<GetProductsItemsResponse, RecyclerView.ViewHolder>(ProductComparator) {

    private var onItemClickCallback: OnItemClickCallback? = null
    var currentViewType = Type.LINEAR
    var items = true

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: GetProductsItemsResponse)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val getData = getItem(position)

        if (getData != null) {
            when (holder) {
                is LinearViewHolder -> {
                    holder.bind(getData)
                }

                is GridViewHolder -> {
                    holder.bind(getData)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (currentViewType) {
            Type.LINEAR -> LINEAR
            Type.GRID -> GRID
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (getItemViewType(viewType)) {
            LINEAR -> {
                val binding = ItemProductLinearBinding.inflate(layoutInflater, parent, false)
                LinearViewHolder(binding)
            }

            GRID -> {
                val binding = ItemProductGridBinding.inflate(layoutInflater, parent, false)
                GridViewHolder(binding)
            }

            else -> {
                throw IllegalArgumentException("Invalid ViewType")
            }
        }
    }


    // Linear view Holder
    inner class LinearViewHolder(private val binding: ItemProductLinearBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: GetProductsItemsResponse) {
            items = true
            binding.apply {
                Glide.with(context)
                    .load(product.image)
                    .into(ivProduct)
                tvProductName.text = product.productName
                tvProductPrice.text = CurrencyUtils.formatRupiah(product.productPrice)
                tvStore.text = product.store
                val soldString = itemView.context.getString(R.string.sold)
                val ratingText = "${product.productRating} | $soldString ${product.sale}"
                tvRating.text = ratingText

                cardProduct.startAnimation(
                    AnimationUtils.loadAnimation(
                        itemView.context,
                        R.anim.slide_in
                    )
                )
            }
            binding.root.setOnClickListener {
                onItemClickCallback?.onItemClicked(product)

            }
        }
    }

    // grid view holder
    inner class GridViewHolder(private val binding: ItemProductGridBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: GetProductsItemsResponse) {
            items = false
            binding.apply {
                Glide.with(context)
                    .load(product.image)
                    .into(ivProduct)
                tvProductName.text = product.productName
                tvProductPrice.text = CurrencyUtils.formatRupiah(product.productPrice)
                tvStore.text = product.store
                val soldString = itemView.context.getString(R.string.sold)
                val ratingText = "${product.productRating} | $soldString ${product.sale}"
                tvRating.text = ratingText

                cardGrid.startAnimation(
                    AnimationUtils.loadAnimation(
                        itemView.context,
                        if (absoluteAdapterPosition % 2 == 0) {
                            R.anim.slide_in
                        } else {
                            R.anim.fall_down
                        }
                    )
                )
            }
            binding.root.setOnClickListener {
                onItemClickCallback?.onItemClicked(product)
            }
        }
    }

    object ProductComparator : DiffUtil.ItemCallback<GetProductsItemsResponse>() {
        override fun areItemsTheSame(
            prevItem: GetProductsItemsResponse,
            nextItem: GetProductsItemsResponse,
        ): Boolean {
            return prevItem.productId == nextItem.productId
        }

        override fun areContentsTheSame(
            prevItem: GetProductsItemsResponse,
            nextItem: GetProductsItemsResponse,
        ): Boolean {
            return prevItem == nextItem
        }
    }

    companion object {
        const val LINEAR = 1
        const val GRID = 0
    }
}
