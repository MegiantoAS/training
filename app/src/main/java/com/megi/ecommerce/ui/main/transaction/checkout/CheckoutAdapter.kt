package com.megi.ecommerce.ui.main.transaction.checkout

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.ItemCheckoutBinding
import com.megi.ecommerce.datasource.network.CheckoutProduct
import com.megi.ecommerce.datasource.network.PaymentItem
import com.megi.ecommerce.helper.CurrencyUtils

class CheckoutAdapter(private val viewModel: CheckoutViewModel) :
    ListAdapter<CheckoutProduct, CheckoutAdapter.ProductCheckoutViewHolder>(
        ProductCheckoutDiffCallback()
    ) {

    private var itemClickListener: CheckoutClickListener? = null

    interface CheckoutClickListener {
        fun onItemClick(label: List<PaymentItem>)
    }

    fun setItemClickListener(listener: CheckoutClickListener) {
        itemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductCheckoutViewHolder {
        val binding =
            ItemCheckoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductCheckoutViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductCheckoutViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    inner class ProductCheckoutViewHolder(private val binding: ItemCheckoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private fun updateCount() {
            binding.quantity.tvCounter.text = counterQuantity.toString()
        }

        private var counterQuantity = 0

        @SuppressLint("ResourceAsColor")
        fun bind(item: CheckoutProduct?) {
            binding.apply {
                val sum = item?.productPrice?.plus(item.variantPrice!!)

                counterQuantity = item!!.quantity
                updateCount()
                tvProductName.text = item.productName
                tvVarian.text = item.variantName

                // condition
                tvRemainingProduk.text = " ${item.stock}"
                if (item.stock!! < 10) {
                    tvRemaining.setTextColor(Color.RED)
                    tvRemainingProduk.setTextColor(Color.RED)
                } else {
                    tvRemaining.setTextColor(
                        ContextCompat.getColor(
                            root.context,
                            R.color.stockColor
                        )
                    )
                    tvRemainingProduk.setTextColor(
                        ContextCompat.getColor(
                            root.context,
                            R.color.stockColor
                        )
                    )
                }

                adsPrice.text = CurrencyUtils.formatRupiah(sum)

                Glide.with(root.context)
                    .load(item.image)
                    .into(ivProductCart)

                // validation for quantity
                quantity.btnDecrease.setOnClickListener {
                    if (counterQuantity <= 1) {
                        val contextView = binding.root
                        Snackbar.make(
                            contextView,
                            R.string.not_null,
                            Snackbar.LENGTH_SHORT
                        ).show()
                    } else {
                        counterQuantity--
                        updateCount()
                        viewModel.updateCartItemQuantity(item.productId, counterQuantity)
                        itemClickListener?.onItemClick(
                            listOf(
                                PaymentItem(
                                    item.productId,
                                    item.variantName,
                                    counterQuantity
                                )
                            )
                        )
                        quantity.btnIncrease.isSelected = false
                    }
                }

                quantity.btnIncrease.setOnClickListener {
                    if (item.stock!! <= counterQuantity) {
                        val contextView = binding.root
                        Snackbar.make(
                            contextView,
                            R.string.emptyStock,
                            Snackbar.LENGTH_SHORT
                        ).show()
                    } else {
                        counterQuantity++
                        updateCount()
                        viewModel.updateCartItemQuantity(item.productId, counterQuantity)
                        itemClickListener?.onItemClick(
                            listOf(
                                PaymentItem(
                                    item.productId,
                                    item.variantName,
                                    counterQuantity
                                )
                            )
                        )
                        quantity.btnIncrease.isSelected = false
                    }
                }
            }
        }
    }
}

class ProductCheckoutDiffCallback : DiffUtil.ItemCallback<CheckoutProduct>() {
    override fun areItemsTheSame(
        oldItem: CheckoutProduct,
        newItem: CheckoutProduct,
    ): Boolean {
        return oldItem.productId == newItem.productId
    }

    override fun areContentsTheSame(
        oldItem: CheckoutProduct,
        newItem: CheckoutProduct,
    ): Boolean {
        return oldItem == newItem
    }
}
