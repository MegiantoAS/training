package com.megi.ecommerce.ui.menutop.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.repository.CartRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(private val repository: CartRepository) : ViewModel() {

    fun getCartItem(): LiveData<List<ProductLocalDb>> {
        return repository.getCartItem()
    }

    fun updateCartItemQuantity(productId: String, newQuantity: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            repository.updateCartItemQuantity(productId, newQuantity)
        }
    }

    fun deleteFromCart(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            repository.deleteFromCart(id)
        }
    }

    fun updateCartItemCheckbox(productId: List<String>, isSelected: Boolean) {
        CoroutineScope(Dispatchers.IO).launch {
            repository.updateCartItemCheckbox(productId, isSelected)
        }
    }

    fun removeFromCartAll(productId: List<String>) {
        CoroutineScope(Dispatchers.IO).launch {
            repository.removeFromCartAll(productId)
        }
    }
}
