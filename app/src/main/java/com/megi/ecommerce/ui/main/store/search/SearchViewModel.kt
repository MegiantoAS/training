package com.megi.ecommerce.ui.main.store.search

import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.repository.StoreRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val repository: StoreRepository,
    private val pref: SharedPreference
) :
    ViewModel() {
    fun searchProduct(token: String, query: String) = repository.searchProduct(token, query)

    fun getAccessToken(): String? {
        return pref.getAccessToken()
    }
}
