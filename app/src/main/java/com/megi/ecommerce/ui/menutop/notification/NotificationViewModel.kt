package com.megi.ecommerce.ui.menutop.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce
import com.megi.ecommerce.repository.NotificationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(private val notificationRepository: NotificationRepository) :
    ViewModel() {

    fun addNotification(notification: NotificationEcommerce) {
        return runBlocking {
            withContext(Dispatchers.IO) {
                notificationRepository.addNotification(notification)
            }
        }
    }

    fun getAllNotification(): LiveData<List<NotificationEcommerce>>? {
        return runBlocking {
            withContext(Dispatchers.IO) {
                notificationRepository.getAllNotification()
            }
        }
    }

    fun updateNotification(notification: NotificationEcommerce) {
        CoroutineScope(Dispatchers.IO).launch {
            notificationRepository.updateNotification(notification)
        }
    }
}
