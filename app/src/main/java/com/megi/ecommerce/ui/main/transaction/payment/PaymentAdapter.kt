package com.megi.ecommerce.ui.main.transaction.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.megi.ecommerce.databinding.ListItemPaymentBinding
import com.megi.ecommerce.datasource.network.PaymentMethodItemResponse

class PaymentAdapter(
    private val dataList: List<PaymentMethodItemResponse>, // show data
    private val itemClickListener: PaymentItemClickListener // click listener
) : RecyclerView.Adapter<PaymentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListItemPaymentBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding, itemClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataList[position]
        holder.bind(item)
    }

    override fun getItemCount() = dataList.size

    // interface click item
    interface PaymentItemClickListener {
        fun onItemClick(item: PaymentMethodItemResponse)
    }

    inner class ViewHolder(
        private val binding: ListItemPaymentBinding,
        private val itemClickListener: PaymentItemClickListener
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: PaymentMethodItemResponse) {
            binding.apply {
                Glide.with(root.context)
                    .load(item.image)
                    .into(ivBank)
                tvBankName.text = item.label

                if (!item.status) {
                    root.alpha = 0.5f
                    root.setOnClickListener(null)
                } else {
                    root.alpha = 1.0f
                    root.setOnClickListener { itemClickListener.onItemClick(item) }
                }
            }
        }
    }
}
