package com.megi.ecommerce.ui.main.transaction.checkout

import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.Payment
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.repository.TransactionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val transactionRepository: TransactionRepository,
    private val cartRepository: CartRepository,
    private val pref: SharedPreference
) : ViewModel() {

    fun buyProducts(token: String, payment: Payment) =
        transactionRepository.buyProducts(token, payment)

    fun getAccessToken(): String? {
        return pref.getAccessToken()
    }

    fun updateCartItemQuantity(productId: String, newQuantity: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            cartRepository.updateCartItemQuantity(productId, newQuantity)
        }
    }

    fun removeFromCartAll(productId: List<String>) {
        CoroutineScope(Dispatchers.IO).launch {
            cartRepository.removeFromCartAll(productId)
        }
    }
}
