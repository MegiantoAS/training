package com.megi.ecommerce.ui.menutop.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.megi.ecommerce.databinding.FragmentNotificationBinding
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationFragment : Fragment() {
    private var _binding: FragmentNotificationBinding? = null
    private val binding get() = _binding
    private val viewModel: NotificationViewModel by viewModels()
    private val adapter: NotificationAdapter by lazy { NotificationAdapter() }
//    private lateinit var adapter: NotificationAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNotificationBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.topAppBar?.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        val linearLayout = LinearLayoutManager(requireContext())
        binding?.rvNotification?.layoutManager = linearLayout
        linearLayout.reverseLayout = true
        binding?.rvNotification?.adapter = adapter
        viewModel.getAllNotification()?.observe(viewLifecycleOwner) {
            if (!it.isEmpty()) {
                adapter.submitList(it)
            } else {
                binding?.linearErrorLayout?.visibility = View.VISIBLE
            }
        }

        adapter.setOnItemClickCallback(object : NotificationAdapter.OnItemClickCallback {
            override fun onItemClick(position: NotificationEcommerce) {
                viewModel.updateNotification(
                    NotificationEcommerce(
                        position.id,
                        position.type,
                        position.date,
                        position.title,
                        position.body,
                        position.image,
                        true
                    )
                )
            }
        })
    }
}
