package com.megi.ecommerce.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils.attachBadgeDrawable
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding

    private val mainViewModel: MainViewModel by viewModels()

    private val navHost by lazy {
        childFragmentManager.findFragmentById(R.id.navview) as NavHostFragment
    }

    private val navController by lazy {
        navHost.findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkUserNameExist()
        checkSession()
        badge()
        binding.apply {
            binding?.bottomNav?.setupWithNavController(navController)
        }

        if (mainViewModel.newData) {
            binding?.bottomNav?.selectedItemId = R.id.transactionFragment
            mainViewModel.newData = false
        }

        binding?.apply {
            topAppBar.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.top_notifcation -> {
                        findNavController().navigate(R.id.action_main_to_notifFragment)
                    }

                    R.id.top_cart -> {
                        findNavController().navigate(R.id.action_main_to_cartFragment)
                    }

                    R.id.top_menu -> {
//                        findNavController().navigate(R.id.action_main_to_screenFragment)
                    }
                }
                true
            }
        }
        binding?.navRail?.setupWithNavController(navController)
        binding?.navview840?.setupWithNavController(navController)
        binding?.bottomNav?.setupWithNavController(navController)
        binding?.bottomNav?.setOnItemReselectedListener { }
        binding?.navRail?.setOnItemReselectedListener { }
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun badge() {
        // badge cart
        mainViewModel.getCartItem().observe(viewLifecycleOwner) {
            val badgeDrawable = BadgeDrawable.create(requireContext())
            val numberOfItemsInCart = it.size
            badgeDrawable.number = numberOfItemsInCart

            if (numberOfItemsInCart > 0) {
                attachBadgeDrawable(badgeDrawable, binding!!.topAppBar, R.id.top_cart)
            } else {
                badgeDrawable.clearNumber()
            }
        }

        // badge wishlist
        mainViewModel.getWishlistProduct().observe(viewLifecycleOwner) {
            val numberOfItemsInCart = it.size
            val bottomBadge = binding?.bottomNav?.getOrCreateBadge(R.id.wishlistFragment)
            if (numberOfItemsInCart > 0) {
                bottomBadge?.number = numberOfItemsInCart
                bottomBadge?.isVisible = true
            } else {
                bottomBadge?.isVisible = false
                bottomBadge?.clearNumber()
            }
        }

        // badge notification
        mainViewModel.getAllNotification()?.observe(viewLifecycleOwner) {
            val notRead = it.filter { !it.isRead }
            val badgeDrawable = BadgeDrawable.create(requireContext())
            val items = notRead.size
            badgeDrawable.number = items

            if (items > 0) {
                attachBadgeDrawable(badgeDrawable, binding!!.topAppBar, R.id.top_notifcation)
            } else {
                badgeDrawable.clearNumber()
            }
        }
    }

    private fun checkSession() {
        val session = mainViewModel.getAccessToken()
        if (session.isNullOrEmpty()) {
            (requireActivity() as MainActivity).logOut()
        }
    }

    private fun checkUserNameExist() {
        val userName = mainViewModel.getNameProfile()
        if (userName.isNullOrEmpty()) {
            findNavController().navigate(R.id.action_main_to_profileFragment)
        } else {
            binding?.tvUserName?.text = userName
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
