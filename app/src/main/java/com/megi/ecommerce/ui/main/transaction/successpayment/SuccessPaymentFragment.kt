package com.megi.ecommerce.ui.main.transaction.successpayment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentSuccessPaymentBinding
import com.megi.ecommerce.datasource.network.PaymentDataResponse
import com.megi.ecommerce.datasource.network.RatingReview
import com.megi.ecommerce.helper.CurrencyUtils
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.AndroidEntryPoint

@Suppress("DEPRECATION")
@AndroidEntryPoint
class SuccessPaymentFragment : Fragment() {

    private var _binding: FragmentSuccessPaymentBinding? = null
    private val binding get() = _binding
    private val viewModel: SuccessPaymentViewModel by viewModels()
    private var dataPayment: PaymentDataResponse? = null
    private var originFragment: String? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments.let {
            dataPayment = it?.getParcelable("data_response")
            originFragment = it?.getString("originFragment")
        }
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    toTransaction()
                }
            }
        )

        _binding = FragmentSuccessPaymentBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnDone?.setOnClickListener {
            getStatus()
        }
        setInvoice()
    }

    private fun getStatus() {
        binding?.apply {
            firebaseAnalytics.logEvent("btn_finish_toTransaction_clicked", null)

            // if rate null
            val ratings = rbReview.rating.toInt()
            val ratingValue = if (ratings == 0) null else ratings

            // if review null
            val review = fieldReview.text.toString()
            val getReview = review.ifEmpty { null }

            progressCircular.visibility = View.VISIBLE

            viewModel.getRateProduct(
                viewModel.getAccessToken().toString(),
                RatingReview(dataPayment?.invoiceId.toString(), ratingValue, getReview)
            ).observe(viewLifecycleOwner) { result ->
                when (result) {
                    is ResultResponse.Success -> toMainFragment()
                    is ResultResponse.Error -> progressCircular.hide()
                    is ResultResponse.Loading -> progressCircular.show()
                }
            }
        }
    }

    private fun toMainFragment() {
        binding?.progressCircular?.hide()
        if (originFragment != null) {
            val bundle = bundleOf("newData" to true)
            findNavController().navigate(
                R.id.action_successPaymentFragment_to_main_navigation,
                bundle
            )
        } else {
            findNavController().navigate(
                R.id.action_successPaymentFragment_to_main_navigation,
                null
            )
        }
    }

    private fun toTransaction() {
        binding?.progressCircular?.hide()
        if (originFragment != null) {
            findNavController().navigateUp()
        } else {
            findNavController().navigate(
                R.id.action_successPaymentFragment_to_main_navigation,
                null
            )
        }
    }

    private fun setInvoice() {
        binding?.apply {
            tvIdTransaksi.text = dataPayment?.invoiceId
            tvTanggal.text = dataPayment?.date
            tvWaktu.text = dataPayment?.time
            tvMetodePembayaran.text = dataPayment?.payment
            tvTotalPembayaran.text = CurrencyUtils.formatRupiah(dataPayment?.total)

            dataPayment?.let { payment ->
                tvStatus.text = getString(if (payment.status) R.string.success else R.string.failed)

                payment.rating?.let { rating ->
                    rbReview.rating = rating.toFloat()
                }

                dataPayment?.review?.let { review ->
                    fieldReview.setText(if (review == "null") "" else review)
                }
            }
        }
    }
}
