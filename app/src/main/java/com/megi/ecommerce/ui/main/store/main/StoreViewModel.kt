package com.megi.ecommerce.ui.main.store.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.network.ProductParam
import com.megi.ecommerce.repository.StoreRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(
    private val productDatabase: ProductDatabase,
    private val storeRepository: StoreRepository,
    pref: SharedPreference
) : ViewModel() {

    private val accessToken = pref.getAccessToken() ?: MainActivity().logOut()
    private val _param = MutableLiveData(ProductParam(accessToken.toString()))
    private val params: LiveData<ProductParam> = _param
    var isList: Boolean = true

    val products = params.switchMap { query ->
        storeRepository.getProductsPaging(
            query.token,
            query.search,
            query.brand,
            query.lowest,
            query.highest,
            query.sort
        )
    }.cachedIn(viewModelScope)

    val param: LiveData<ProductParam> = _param

    fun queryProduct(
        brand: String? = null,
        lowest: Int? = null,
        highest: Int? = null,
        sort: String? = null,
    ) {
        _param.value = _param.value?.copy(
            brand = brand,
            lowest = lowest,
            highest = highest,
            sort = sort
        )
    }

    fun resetParam() {
        _param.value = _param.value?.copy(
            search = null,
            brand = null,
            lowest = null,
            highest = null,
            sort = null
        )
    }

    fun setSearch(
        search: String? = null,
    ) {
        _param.value = _param.value?.copy(
            search = search,
        )
    }

    fun clearDb() {
        productDatabase.clearAllTables()
    }
}
