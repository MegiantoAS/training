package com.megi.ecommerce.ui.main.transaction.checkout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentCheckoutBinding
import com.megi.ecommerce.datasource.network.CheckoutProduct
import com.megi.ecommerce.datasource.network.Payment
import com.megi.ecommerce.datasource.network.PaymentItem
import com.megi.ecommerce.datasource.network.PaymentMethodItemResponse
import com.megi.ecommerce.helper.CurrencyUtils
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.AndroidEntryPoint

@Suppress("DEPRECATION")
@AndroidEntryPoint
class CheckoutFragment : Fragment() {

    private var _binding: FragmentCheckoutBinding? = null
    private val binding get() = _binding
    private val viewModel: CheckoutViewModel by viewModels()
    private lateinit var adapter: CheckoutAdapter
    private var dataProduct = listOf<CheckoutProduct>()
    private var listPayment = listOf<PaymentItem>()
    private var dataPayment: PaymentMethodItemResponse? = null
    private var itemCount: Int? = 0
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        arguments?.let {
            dataProduct = it.getParcelableArrayList("data_product") ?: emptyList()
            val bundle =
                findNavController().currentBackStackEntry?.savedStateHandle?.get<Bundle>("payment")
            dataPayment = bundle?.getParcelable("payment")
        }
        _binding = FragmentCheckoutBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.topAppBar?.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        binding?.cvPaymentMethods?.setOnClickListener {
            findNavController().navigate(R.id.action_checkoutFragment_to_paymentMethodFragment)
        }

        getProductData()
        rateReview()
    }

    private fun getProductData() {
        adapter = CheckoutAdapter(viewModel)
        binding?.apply {
            // RecyclerView setup
            val linearLayout = LinearLayoutManager(requireContext())
            rvCheckout.layoutManager = linearLayout
            rvCheckout.adapter = adapter

            // Submitting data to the adapter
            adapter.submitList(dataProduct)

            // Item click listener
            adapter.setItemClickListener(object : CheckoutAdapter.CheckoutClickListener {
                override fun onItemClick(label: List<PaymentItem>) {
                    label.forEach { paymentItem ->
                        val productToUpdate =
                            dataProduct.find { it.productId == paymentItem.productId }
                        productToUpdate?.quantity = paymentItem.quantity
                    }

                    // Calculate total price and update payment list
                    val totalSelectedPrice =
                        dataProduct.sumBy { (it.productPrice + it.variantPrice!!) * it.quantity }
                    listPayment =
                        dataProduct.map { PaymentItem(it.productId, it.variantName, it.quantity) }
                    updateTotalPrice(totalSelectedPrice)
                }
            })

            // Initialize listPayment
            listPayment = dataProduct.map { PaymentItem(it.productId, it.variantName, it.quantity) }

            // Calculate and display total price
            val totalSelectedPrice =
                dataProduct.sumBy { (it.productPrice + it.variantPrice!!) * it.quantity }
            val formattedPrice = CurrencyUtils.formatRupiah(totalSelectedPrice)
            tvHargaCheckout.text = formattedPrice.toString()

            // Update bank information if available
            dataPayment?.let {
                Glide.with(requireContext())
                    .load(it.image)
                    .into(ivBankImage)
                tvBankName.text = it.label
            }
        }
    }

    private fun rateReview() {
        binding?.apply {
            if (dataPayment != null) {
                btnCheckout.isEnabled = true
                dataProduct.map { itemCount = it.quantity }

                btnCheckout.setOnClickListener {
                    progressCircular.visibility = View.VISIBLE

                    viewModel.buyProducts(
                        viewModel.getAccessToken().toString(),
                        Payment(dataPayment!!.label, listPayment)
                    )
                        .observe(viewLifecycleOwner) {
                            when (it) {
                                is ResultResponse.Success -> {
                                    progressCircular.hide()

                                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.PURCHASE) {
                                        param(
                                            FirebaseAnalytics.Param.TRANSACTION_ID,
                                            it.data.data?.invoiceId!!
                                        )
                                        param(
                                            FirebaseAnalytics.Param.SUCCESS,
                                            it.data.data!!.status.toString()
                                        )
                                        param(
                                            FirebaseAnalytics.Param.ITEM_NAME,
                                            it.data.data!!.payment
                                        )
//                                        param(
//                                            FirebaseAnalytics.Param.CURRENCY,
//                                            it.data.data!!.total.toCurrencyFormat()
//                                        )
                                    }

                                    val bundle = Bundle().apply {
                                        putParcelable("data_response", it.data.data)
                                    }

                                    // delete product from cart after success
                                    viewModel.removeFromCartAll(dataProduct.map { it.productId })

                                    findNavController().navigate(
                                        R.id.action_checkoutFragment_to_successPaymentFragment,
                                        bundle
                                    )
                                }

                                is ResultResponse.Loading -> {
                                    progressCircular.show()
                                    btnCheckout.isEnabled = false
                                }

                                is ResultResponse.Error -> {
                                    progressCircular.hide()
                                    showSnackbar(R.string.sn_lose)
                                    btnCheckout.isEnabled = true
                                }

                                else -> {}
                            }
                        }
                }
            } else {
                btnCheckout.isEnabled = false
            }
        }
    }

    private fun showSnackbar(messageResId: Int) {
        Snackbar.make(
            requireView(),
            requireContext().getString(messageResId),
            Snackbar.LENGTH_SHORT
        ).show()
    }

    private fun updateTotalPrice(item: Int) {
        binding?.tvHargaCheckout?.text = CurrencyUtils.formatRupiah(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
