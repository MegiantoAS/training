package com.megi.ecommerce.ui.main.store.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentDetailProductBinding
import com.megi.ecommerce.datasource.local.room.AppExecutor
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.ProductDao
import com.megi.ecommerce.datasource.network.GetProductDetailItemResponse
import com.megi.ecommerce.datasource.network.ProductVariant
import com.megi.ecommerce.datasource.network.asCheckoutProduct
import com.megi.ecommerce.datasource.network.asProductLocalDb
import com.megi.ecommerce.datasource.network.asWishlistProduct
import com.megi.ecommerce.helper.CurrencyUtils
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.AndroidEntryPoint

@Suppress("DEPRECATION")
@AndroidEntryPoint
class DetailProductFragment : Fragment() {
    private var _binding: FragmentDetailProductBinding? = null
    private val binding get() = _binding
    private val viewModel: DetailProductViewModel by viewModels()
    private var idProduct: String? = null
    private var variantName: String? = null
    private var productPrice: Int? = 0
    private var variantPrice: Int? = 0
    private var product: GetProductDetailItemResponse? = null
    private lateinit var appExecutors: AppExecutor
    private lateinit var database: ProductDatabase
    private lateinit var productDao: ProductDao
    private var counter = 0
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        idProduct = arguments?.getString("id_product")

        _binding = FragmentDetailProductBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getChip()
        getDetail()
        deepLink()

        binding?.apply {
            buttonViewAll.setOnClickListener {
                firebaseAnalytics.logEvent("btn_viewAll_clicked", null)
                val bundle = bundleOf("id_product" to product?.productId)
                (requireActivity() as MainActivity).toViewReview(bundle)
            }
        }

        binding?.apply {
            topAppBar.setNavigationOnClickListener {
                findNavController().navigateUp()
            }
        }

        // to checkout
        binding?.btnBeliLangsung?.setOnClickListener {
            val productLocalDb = product?.asProductLocalDb(variantName, variantPrice)
            val productCheckout = arrayListOf(
                productLocalDb?.asCheckoutProduct(
                    variantName ?: "RAM 16GB",
                    variantPrice
                )
            )
            val bundle = bundleOf("data_product" to productCheckout)
            (requireActivity() as MainActivity).detailToCheckout(bundle)

            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT) {
                param(FirebaseAnalytics.Param.ITEM_ID, productLocalDb?.productId!!)
                param(FirebaseAnalytics.Param.ITEM_NAME, productLocalDb.productName)
            }
        }
    }

    // get Detail
    @SuppressLint("SetTextI18n")
    private fun getDetail() {
        appExecutors = AppExecutor()
        database = ProductDatabase.getDatabase(requireContext())
        productDao = database.productDao()

        binding?.apply {
            progressCircular.visibility = View.VISIBLE
            idProduct?.let {
                viewModel.dataDetail.observe(viewLifecycleOwner) { it ->
                    when (it) {
                        // if response success
                        is ResultResponse.Success -> {
                            scrollView.visibility = View.VISIBLE
                            viewBottom.visibility = View.VISIBLE
                            progressCircular.hide()

                            val listVariant = ArrayList<ProductVariant>()
                            product = it.data.data
                            productPrice = product?.productPrice
                            tvProductPrice.text = CurrencyUtils.formatRupiah(product?.productPrice)
                            tvProductName.text = product?.productName
                            tvSale.text = product?.sale.toString()
                            tvRatingUp.text = product?.productRating.toString()
                            tvRate.text = "(${product?.totalRating})"
                            tvProductRating.text = product?.productRating.toString()
                            tvDescription.text = product?.description
                            tvPercent.text = "${product?.totalSatisfaction}%"
                            tvTotalRate.text = "${product?.totalRating} Rating"
                            tvTotalReview.text = "${product?.totalReview} Ulasan"
                            product?.productVariant?.forEach {
                                listVariant.add(it)
                            }

                            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM) {
                                param(FirebaseAnalytics.Param.ITEM_ID, product!!.productId)
                                param(FirebaseAnalytics.Param.ITEM_NAME, product!!.productName)
                            }

                            createChip(listVariant)
                            val images = product?.image
                            val viewpagerAdapter = DetailProductAdapter(images!!)
                            vpImageProduct.adapter = viewpagerAdapter

                            if (images.size <= 1) {
                                tabDots.visibility = View.GONE
                            } else {
                                tabDots.visibility = View.VISIBLE
                                TabLayoutMediator(tabDots, vpImageProduct) { _, _ -> }.attach()
                            }

                            addWishList()
                            addCart()
                        }

                        // if response Error
                        is ResultResponse.Error -> {
                            progressCircular.hide()
                            binding.apply {
                                viewBottom.visibility = View.GONE
                                scrollView.visibility = View.GONE
                                linearError.visibility = View.VISIBLE
                                btnRefresh.setOnClickListener {
                                    viewModel.dataDetail
                                }
                            }
                        }

                        // if response Loading
                        is ResultResponse.Loading -> {
                            progressCircular.show()
                            scrollView.visibility = View.GONE
                            viewBottom.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    // add wishList
    private fun addWishList() {
        appExecutors.diskIO.execute {
            val wishlistLocalDb = product?.asWishlistProduct(variantName, variantPrice)
            val checkWishlistExist = viewModel.getProductWishlistById(wishlistLocalDb?.productId!!)
            var isChecked = checkWishlistExist?.productId != null

            with(binding?.toggleFavorite) {
                this?.isChecked = isChecked
                this?.setBackgroundResource(if (isChecked) R.drawable.ic_favorite else R.drawable.ic_fav_border)
                this?.setOnClickListener {
                    isChecked = !isChecked

                    // firebase add to cart
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_WISHLIST) {
                        param(FirebaseAnalytics.Param.ITEM_ID, wishlistLocalDb.productId)
                        param(FirebaseAnalytics.Param.ITEM_NAME, wishlistLocalDb.productName)
                        param(FirebaseAnalytics.Param.CURRENCY, "IDR")
                    }

                    if (isChecked) {
                        viewModel.addToWishList(
                            wishlistLocalDb.productId,
                            wishlistLocalDb.productName,
                            wishlistLocalDb.productPrice,
                            wishlistLocalDb.image,
                            wishlistLocalDb.store,
                            wishlistLocalDb.sale,
                            wishlistLocalDb.stock,
                            wishlistLocalDb.totalRating,
                            wishlistLocalDb.productRating,
                            variantName ?: "RAM 16GB",
                            variantPrice
                        )
                        setBackgroundResource(R.drawable.ic_favorite)
                        showSnackbar(R.string.snAdd_wishList)
                    } else {
                        viewModel.deleteWishList(wishlistLocalDb.productId)
                        setBackgroundResource(R.drawable.ic_fav_border)
                        showSnackbar(R.string.sndelete_wishlist)
                    }
                    this.isChecked = isChecked
                }
            }
        }
    }

    // add to cart
    private fun addCart() {
        binding?.btnCart?.setOnClickListener {
            appExecutors.diskIO.execute {
                val productLocalDb = product?.asWishlistProduct(variantName, variantPrice)
                val checkProductExist = viewModel.getCartById(productLocalDb?.productId!!)

                // firebase add to cart
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
                    param(FirebaseAnalytics.Param.ITEM_ID, productLocalDb.productId)
                    param(FirebaseAnalytics.Param.ITEM_NAME, productLocalDb.productName)
                    param(FirebaseAnalytics.Param.CURRENCY, "IDR")
                }

                if (checkProductExist?.productId != null) {
                    if (checkProductExist.quantity < checkProductExist.stock!!) {
                        counter = checkProductExist.quantity
                        counter++
                        viewModel.updateCartItemQuantity(
                            checkProductExist.productId,
                            counter
                        )
                        showSnackbar(R.string.sn_cart)
                    } else {
                        showSnackbar(R.string.emptyStock)
                    }
                } else {
                    viewModel.addToCart(
                        productLocalDb.productId,
                        productLocalDb.productName,
                        productLocalDb.productPrice,
                        productLocalDb.image,
                        productLocalDb.store,
                        productLocalDb.sale,
                        productLocalDb.stock,
                        productLocalDb.totalRating,
                        productLocalDb.productRating,
                        variantName ?: "RAM 16GB",
                        variantPrice
                    )
                    showSnackbar(R.string.sn_cart)
                }
            }
        }
    }

    private fun showSnackbar(messageResId: Int) {
        Snackbar.make(
            requireView(),
            requireContext().getString(messageResId),
            Snackbar.LENGTH_SHORT
        ).show()
    }

    // chip active
    private fun createChip(variants: List<ProductVariant>) {
        binding?.apply {
            chipGroupVarian.removeAllViews()
            for ((index, variant) in variants.withIndex()) {
                val chip = Chip(requireContext())
                chip.apply {
                    text = variant.variantName
                    isChipIconVisible = false
                    isCloseIconVisible = false
                    isCheckable = true
                    tag = variant
                    isChecked = index == 0
                }
                chipGroupVarian.addView(chip as View)
            }
        }
    }

    // choose chip variant
    private fun getChip() {
        binding?.apply {
            chipGroupVarian.setOnCheckedChangeListener { group, checkedId ->
                val checkedChip = group.findViewById<Chip>(checkedId)
                if (checkedChip != null) {
                    val selectedVariant = checkedChip.tag as ProductVariant
                    variantName = selectedVariant.variantName
                    variantPrice = selectedVariant.variantPrice
                    val updateTvProductPrice = productPrice?.plus(variantPrice!!)
                    tvProductPrice.text = CurrencyUtils.formatRupiah(updateTvProductPrice)
                }
            }
        }
    }

    // create deep link
    private fun deepLink() {
        binding?.btnShare?.setOnClickListener {
            firebaseAnalytics.logEvent("btn_share_clicked", null)
            val deepLink = "Product : ${product?.productName}\n" +
                "Price : ${CurrencyUtils.formatRupiah(product?.productPrice)}\n" +
                "Link : http://ecommerce.com/products/$idProduct"

            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, deepLink)

            startActivity(Intent.createChooser(shareIntent, null))
        }
    }
}
