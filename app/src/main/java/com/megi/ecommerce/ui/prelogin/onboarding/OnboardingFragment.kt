package com.megi.ecommerce.ui.prelogin.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentOnboardingBinding
import com.megi.ecommerce.datasource.local.SharedPreference

class OnboardingFragment : Fragment() {
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    private var _binding: FragmentOnboardingBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPref = SharedPreference(requireContext())
        firebaseAnalytics = Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toRegister()
        toLogin()
        viewPager()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentOnboardingBinding.inflate(inflater, container, false)
        return binding.root
    }

    // Create View Pager
    private fun viewPager() {
        val items = listOf(R.drawable.onboard1, R.drawable.onboard2, R.drawable.onboard3)
        val pagerAdapter = OnboardingAdapter(items)
        binding.apply {
            viewPager.adapter = pagerAdapter
            TabLayoutMediator(tabLayout, viewPager) { _, _ -> }.attach()

            binding.btnNextOnBoarding.setOnClickListener {
                val currentItem = binding.viewPager.currentItem
                val nextItem = currentItem + 1

                if (nextItem < items.size) {
                    binding.viewPager.setCurrentItem(nextItem, true)
                }

                if (nextItem == items.size - 1) {
                    binding.btnNextOnBoarding.visibility = View.GONE
                }
            }

            binding.viewPager.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    if (position < items.size - 1) {
                        binding.btnNextOnBoarding.visibility = View.VISIBLE
                    } else {
                        binding.btnNextOnBoarding.visibility = View.GONE
                    }
                }
            })
        }
    }

    // navigate to Register
    private fun toRegister() {
        binding.btnJoinNow.setOnClickListener {
            firebaseAnalytics.logEvent("btn_join_toRegister_clicked", null)
            findNavController().navigate(R.id.action_onboardingFragment_to_registerFragment)
            sharedPref.onBoarding(false)
        }
    }

    // navigate to Login
    private fun toLogin() {
        binding.btnSkipOnBoarding.setOnClickListener {
            firebaseAnalytics.logEvent("btn_skip_toRegister_clicked", null)
            findNavController().navigate(R.id.action_onboardingFragment_to_loginFragment)
            sharedPref.onBoarding(false)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
