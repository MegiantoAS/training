package com.megi.ecommerce.ui.main.profile

import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.repository.PreLoginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import okhttp3.MultipartBody
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repository: PreLoginRepository,
    private val pref: SharedPreference
) :
    ViewModel() {
    fun doProfile(token: String, name: MultipartBody.Part, image: MultipartBody.Part?) =
        repository.saveToProfile(token, name, image)

    fun getAccessToken(): String? {
        return pref.getAccessToken()
    }
}
