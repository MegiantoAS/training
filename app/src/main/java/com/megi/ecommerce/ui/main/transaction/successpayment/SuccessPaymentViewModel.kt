package com.megi.ecommerce.ui.main.transaction.successpayment

import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.RatingReview
import com.megi.ecommerce.repository.TransactionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SuccessPaymentViewModel @Inject constructor(
    private val transactionRepository: TransactionRepository,
    private val pref: SharedPreference
) : ViewModel() {

    fun getRateProduct(token: String, rate: RatingReview) =
        transactionRepository.ratingProduct(token, rate)

    fun getAccessToken(): String? {
        return pref.getAccessToken()
    }
}
