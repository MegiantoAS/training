package com.megi.ecommerce.ui.prelogin.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.google.gson.Gson
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentLoginBinding
import com.megi.ecommerce.datasource.network.Auth
import com.megi.ecommerce.datasource.network.ErrorResponse
import com.megi.ecommerce.utils.Constant.API_KEY
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException

@Suppress("DEPRECATION")
@AndroidEntryPoint
class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private val viewModel: LoginViewModel by viewModels()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var tokenFcm: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics

        checkFirstInstall()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toRegister()
        upLogin()
        fieldValidationCheck()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    // hit login
    private fun upLogin() {
        binding.apply {
            btnLogin.setOnClickListener {
                progressCircular.visibility = View.VISIBLE
                val email = binding.fieldEmail.text.toString()
                val password = binding.fieldPassword.text.toString()

                getTokenNotification { token ->
                    val auth = Auth(email, password, token)
                    tokenFcm = token

                    viewModel.doLogin(API_KEY, auth)
                        .observe(viewLifecycleOwner) { result ->
                            when (result) {
                                is ResultResponse.Success -> onLoginSuccess()
                                is ResultResponse.Error -> onLoginError(result.exception)
                                is ResultResponse.Loading ->
                                    progressCircular.visibility =
                                        View.VISIBLE
                            }
                            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN) {
                                param(FirebaseAnalytics.Param.METHOD, "email")
                            }
                        }
                }
            }
        }
    }

    private fun getTokenNotification(callback: (String?) -> Unit) {
        Firebase.messaging.token.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val token = task.result
                tokenFcm = token
                val msg = "Generate Token success, $token"
                Log.d("MainActivity", msg)

                Firebase.messaging.subscribeToTopic("promo")
                    .addOnCompleteListener { subscriptionTask ->
                        var subscriptionMsg = "Subscribed"
                        if (!subscriptionTask.isSuccessful) {
                            subscriptionMsg = "Subscribe failed"
                        }
                        Log.d("MainActivity Subs", subscriptionMsg)
                        callback(token)
                    }
            } else {
                Log.w("MainActivity", "Fetching FCM registration token failed", task.exception)
                callback(null)
            }
        }
    }

    // on login success
    private fun onLoginSuccess() {
        binding.apply {
            progressCircular.visibility = View.INVISIBLE
            findNavController().navigate(R.id.action_prelog_to_mainFragment)
            Snackbar.make(
                requireView(),
                "Success",
                Snackbar.LENGTH_SHORT
            ).show()
        }
        getTokenNotification {}
    }

    // on Login Error
    private fun onLoginError(e: Exception) {
        binding.apply {
            progressCircular.visibility = View.INVISIBLE
            Snackbar.make(
                requireView(),
                getApiErrorMessage(e).toString(),
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    // navigation to register
    private fun toRegister() {
        binding.apply {
            btnRegister.setOnClickListener {
                firebaseAnalytics.logEvent("btn_login_toRegister", null)
                findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }
        }
    }

    // install Check
    private fun checkFirstInstall() {
        val isFirstInstall = viewModel.getFirsInstall()
        if (isFirstInstall) {
            findNavController().navigate(R.id.action_loginFragment_to_onboardingFragment)
        }
    }

    // check validation Field
    private fun fieldValidationCheck() {
        binding.fieldEmail.addTextChangedListener(emailValidationField)
        binding.fieldPassword.addTextChangedListener(passwordValidationField)
    }

    // validation for field email
    private val emailValidationField: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            val email: String = charSequence.toString()
            if (!isValidEmail(email)) {
                binding.inputEmail.error = getString(R.string.error_email)
            } else {
                binding.inputEmail.error = null
            }
            if (email.isEmpty()) {
                binding.inputEmail.error = null
            }
            checkFormValidity()
        }

        override fun afterTextChanged(editable: Editable) {}
    }

    // validation for field password
    private val passwordValidationField: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            val password: String = charSequence.toString()
            if (password.length < 8) {
                binding.inputPassword.error = getString(R.string.error_pass)
            } else {
                binding.inputPassword.error = null
            }
            if (password.isEmpty()) {
                binding.inputPassword.error = null
            }

            checkFormValidity()
        }

        override fun afterTextChanged(editable: Editable) {}
    }

    // Button active
    private fun checkFormValidity() {
        val email: String = binding.fieldEmail.text.toString()
        val password: String = binding.fieldPassword.text.toString()
        binding.btnLogin.isEnabled =
            email.isNotEmpty() && isValidEmail(email) && password.length >= 8
    }

    // matches validation email
    private fun isValidEmail(email: CharSequence): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun getApiErrorMessage(e: Exception): String? {
        var message = e.message
        if (e is HttpException) {
            val errorResponse =
                Gson().fromJson(
                    e.response()?.errorBody()?.string(),
                    ErrorResponse::class.java
                ) ?: ErrorResponse()
            errorResponse.message?.let { message = it }
        }
        return message
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
