package com.megi.ecommerce.ui.main.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val productDatabase: ProductDatabase,
    private val pref: SharedPreference
) : ViewModel() {

    private val _isReady = MutableStateFlow(false)
    val isReady = _isReady.asStateFlow()

    init {
        viewModelScope.launch {
            delay(300L)
            _isReady.value = true
        }
    }

    fun getTheme(): Boolean {
        return pref.getDarkMode()
    }

    fun getLanguage(): Boolean {
        return pref.getLanguage()
    }

    fun saveDarkMode(i: Boolean) {
        return pref.saveDarkMode(i)
    }

    fun saveLanguage(i: Boolean) {
        return pref.saveLanguage(i)
    }

    fun clearDb() {
        productDatabase.clearAllTables()
    }
}
