package com.megi.ecommerce.ui.main.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.TransactionResponse
import com.megi.ecommerce.repository.TransactionRepository
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val transactionRepository: TransactionRepository,
    private val pref: SharedPreference
) : ViewModel() {

    private val _dataTransaction = MutableLiveData<ResultResponse<TransactionResponse>>(null)
    val dataTransaction: LiveData<ResultResponse<TransactionResponse>> = _dataTransaction

    init {
        getTransaction(getAccessToken().toString())
    }

    fun getTransaction(token: String) {
        viewModelScope.launch {
            transactionRepository.getTransaction(token).observeForever {
                _dataTransaction.value = it
            }
        }
    }

    fun getAccessToken(): String? {
        return pref.getAccessToken()
    }
}
