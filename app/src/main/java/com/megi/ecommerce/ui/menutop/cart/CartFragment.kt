package com.megi.ecommerce.ui.menutop.cart

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentCartBinding
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.datasource.network.CheckoutProduct
import com.megi.ecommerce.datasource.network.asCheckoutProduct
import com.megi.ecommerce.helper.CurrencyUtils
import dagger.hilt.android.AndroidEntryPoint

@Suppress("DEPRECATION")
@AndroidEntryPoint
class CartFragment : Fragment() {
    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding
    private val cartViewModel: CartViewModel by viewModels()
    private var listCart = listOf<CheckoutProduct>()
    private lateinit var adapter: CartAdapter
    private var selectedItemCount = 0
    private var totalItemCount = 0
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getProductCart()

        binding?.topAppBar?.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        binding?.checkAll?.setOnClickListener {
            selectAllItems(binding?.checkAll!!.isChecked)
        }

        // to checkout with bundle list data
        binding?.btnBuy?.setOnClickListener {
            val bundle = bundleOf("data_product" to listCart)
            findNavController().navigate(R.id.action_cartFragment_to_checkoutFragment, bundle)
        }
    }

    private fun getProductCart() {
        adapter = CartAdapter(cartViewModel)

        val linearLayout = LinearLayoutManager(requireContext())
        binding?.rvCart?.layoutManager = linearLayout
        binding?.rvCart?.adapter = adapter

        cartViewModel.getCartItem().observe(viewLifecycleOwner) { items ->
            adapter.submitList(items)

            selectedItemCount = items.count { it.selected }
            totalItemCount = items.size

            // firebase
            items.forEach {
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT) {
                    param(FirebaseAnalytics.Param.ITEM_ID, it.productId)
                    param(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                }
            }

            // condition hidden layout
            binding?.apply {
                if (selectedItemCount > 0) {
                    btnDeleteAll.visibility = View.VISIBLE
                    btnBuy.isEnabled = true
                } else {
                    btnDeleteAll.visibility = View.GONE
                    btnBuy.isEnabled = false
                }

                if (!(totalItemCount > 0 && selectedItemCount == totalItemCount)) {
                    checkAll.isChecked = false
                } else {
                    checkAll.isChecked = true
                    linearError.visibility = View.GONE
                }

                if (totalItemCount <= 0) {
                    linearError.visibility = View.VISIBLE
                    checkBoxContainer.visibility = View.GONE
                    view.visibility = View.GONE
                } else {
                    linearError.visibility = View.GONE
                }
            }

            // checkout
            val selectedProduct = items.filter { it.selected }.map {
                it.asCheckoutProduct(it.variantName, it.variantPrice)
            }
            listCart = selectedProduct

            // delete cart
            adapter.setOnItemClickCallback(object : CartAdapter.OnItemDeleteClickCallback {
                override fun onItemDeleteClick(position: ProductLocalDb) {
                    cartViewModel.deleteFromCart(position.productId)
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.REMOVE_FROM_CART) {
                        param(FirebaseAnalytics.Param.ITEM_ID, position.productId)
                    }
                }
            })

            // cart to detail with bundle id
            adapter.setOnItemClickCallback(object : CartAdapter.OnItemClickCallback {
                override fun onItemClick(position: ProductLocalDb) {
                    val bundle = bundleOf("id_product" to position.productId)
                    (requireActivity() as MainActivity).cartToDetail(bundle)
                }
            })

            // firebase
            items.forEach {
                val viewCart = Bundle()
                viewCart.putString(FirebaseAnalytics.Param.ITEM_ID, it.productId)
                viewCart.putString(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                viewCart.putString(
                    FirebaseAnalytics.Param.VALUE,
                    (it.quantity * it.productPrice).toString()
                )
                viewCart.putString(FirebaseAnalytics.Param.CURRENCY, "IDR")
                val params = Bundle()
                params.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(viewCart))

                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_CART, params)
            }

            val selectedIds = items
                .filter { it.selected }
                .map { it.productId }

            // btn delete All in Checklist
            binding?.btnDeleteAll?.setOnClickListener {
                cartViewModel.removeFromCartAll(selectedIds)
                selectedIds.map {
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.REMOVE_FROM_CART) {
                        param(FirebaseAnalytics.Param.ITEM_ID, it)
                    }
                }
            }

            // sum price
            val totalSelectedPrice = items.filter { it.selected }
                .sumOf { (it.productPrice + it.variantPrice!!) * it.quantity }

            val formattedPrice = CurrencyUtils.formatRupiah(totalSelectedPrice)
            binding?.tvHargaCart?.text = formattedPrice.toString()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun selectAllItems(isSelected: Boolean) {
        binding?.apply {
            val cartItems = adapter.currentList
            val selectedIds = mutableListOf<String>()

            for (item in cartItems) {
                item.selected = isSelected
                selectedIds.add(item.productId)
                if (cartItems.size > 0 && item.selected) {
                    btnDeleteAll.visibility = View.VISIBLE
                } else {
                    btnDeleteAll.visibility = View.GONE
                }
            }
            cartViewModel.updateCartItemCheckbox(selectedIds, isSelected)
            adapter.notifyDataSetChanged()
        }
    }
}
