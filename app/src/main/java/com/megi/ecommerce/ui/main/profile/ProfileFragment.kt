package com.megi.ecommerce.ui.main.profile

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentProfileBinding
import com.megi.ecommerce.datasource.network.ErrorResponse
import com.megi.ecommerce.helper.reduceFileImage
import com.megi.ecommerce.helper.uriToFile
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.HttpException
import java.io.File

@AndroidEntryPoint
class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private var uri: Uri? = null
    private var file: File? = null
    private val viewModel: ProfileViewModel by viewModels()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    // set Camera
    private val startCamera =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { isSuccess: Boolean? ->
            if (isSuccess == true) {
                binding.vectorImg.visibility = View.GONE
                Glide.with(binding.root.context)
                    .load(uri)
                    .into(binding.ivPhoto)
                val file = uriToFile(uri!!, requireContext())
                val reducedFile = reduceFileImage(file)
                this.file = reducedFile
            }
        }

    // set Galery
    private val startGallery =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            if (uri != null) {
                val file = uriToFile(uri, requireContext())
                val reducedFile = reduceFileImage(file)
                this.file = reducedFile
                binding.vectorImg.visibility = View.GONE
                Glide.with(binding.root.context)
                    .load(uri)
                    .into(binding.ivPhoto)
            } else {
                Log.d("PhotoPicker", "No media selected")
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupPhoto()
        btnEnable()
        binding.btnDone.setOnClickListener {
            firebaseAnalytics.logEvent("btn_profile_toMain", null)
            setProfile(file)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    //    set profile
    private fun setProfile(file: File?) {
        binding.apply {
            progressCircular.visibility = View.VISIBLE
            val requestUser =
                MultipartBody.Part.createFormData("userName", binding.fieldName.text.toString())
            val requestImage = file?.let {
                MultipartBody.Part.createFormData(
                    "userImage",
                    file.name,
                    file.asRequestBody("image/jpeg".toMediaTypeOrNull())
                )
            }
            val token = viewModel.getAccessToken()
            viewModel.doProfile(token!!, requestUser, requestImage)
                .observe(viewLifecycleOwner) {
                    when (it) {
                        is ResultResponse.Success -> onProfileSuccess()
                        is ResultResponse.Error -> {
                            progressCircular.hide()
                            findNavController().navigate(R.id.action_addProfileFragment_to_main_navigation)
                            onProfileError(it.exception)
                        }

                        is ResultResponse.Loading -> {
                            progressCircular.show()
                        }
                    }
                }
        }
    }

    // take photo and Gallery
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }

    // dialog setup photo
    private fun setupPhoto() {
        binding.apply {
            personPhoto.setOnClickListener {
                if (!allPermissionsGranted()) {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        REQUIRED_PERMISSIONS,
                        REQUEST_CODE_PERMISSIONS
                    )
                }
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(R.string.clicked_photos)
                    .setBackground(
                        ContextCompat.getDrawable(
                            requireContext(),
                            R.drawable.backround_dialog
                        )
                    )
                    .setItems(R.array.choose_pict) { _, which ->
                        when (which) {
                            0 -> {
                                startCamera()
                            }

                            1 -> {
                                startGallery()
                            }
                        }
                    }
                    .show()
            }
        }
    }

    private fun startCamera() {
        uri = buildNewUri()
        startCamera.launch(uri)
    }

    private fun startGallery() {
        startGallery.launch("image/*")
    }

    private fun buildNewUri(): Uri {
        val photosDir = File(requireContext().cacheDir, "photos")
        photosDir.mkdirs()
        val photoFile = File(photosDir, "photos-${System.currentTimeMillis()}.jpg")
        val authority = "${requireContext().packageName}.fileprovider"
        return FileProvider.getUriForFile(requireContext(), authority, photoFile)
    }

    // validation button
    private fun btnEnable() {
        binding.btnDone.isEnabled = false
        binding.fieldName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                binding.btnDone.isEnabled = s.isNotEmpty()
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int,
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

    private fun onProfileSuccess() {
        binding.apply {
            progressCircular.visibility = View.INVISIBLE
            findNavController().navigate(R.id.action_profileFragment_to_main_navigation)
            Snackbar.make(
                requireView(),
                "Success",
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    private fun onProfileError(e: Exception) {
        binding.apply {
            progressCircular.visibility = View.INVISIBLE
            Snackbar.make(
                requireView(),
                getApiErrorMessage(e).toString(),
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    private fun getApiErrorMessage(e: Exception): String? {
        var message = e.message
        if (e is HttpException) {
            val errorResponse =
                Gson().fromJson(
                    e.response()?.errorBody()?.string(),
                    ErrorResponse::class.java
                ) ?: ErrorResponse()
            errorResponse.message?.let { message = it }
        }
        return message
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
        private const val REQUEST_CODE_PERMISSIONS = 101
    }
}
