package com.megi.ecommerce.ui.menutop.cart

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.ItemCartBinding
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.helper.CurrencyUtils

class CartAdapter(
    private val cartViewModel: CartViewModel
) : ListAdapter<ProductLocalDb, CartAdapter.CartViewHolder>(CartProductDiffCallback()) {
    private var onItemClickCallback: OnItemClickCallback? = null
    private var onItemDeleteClickCallback: OnItemDeleteClickCallback? = null

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemDeleteClickCallback) {
        this.onItemDeleteClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClick(position: ProductLocalDb)
    }

    interface OnItemDeleteClickCallback {
        fun onItemDeleteClick(position: ProductLocalDb)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding = ItemCartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    inner class CartViewHolder(private val binding: ItemCartBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private fun updateCount() {
            binding.quantity.tvCounter.text = counterQuantity.toString()
        }

        private var counterQuantity = 0

        fun bind(item: ProductLocalDb) {
            binding.apply {
                setupProductDetails(item)
                setupRemainingStock(item)
                setupImageAndClickListeners(item)
                setupQuantityValidation(item)
                setupCheckbox(item)
            }
        }

        private fun ViewBinding.setupProductDetails(item: ProductLocalDb) {
            val sum = item.productPrice.plus(item.variantPrice!!)
            counterQuantity = item.quantity
            updateCount()

            binding.apply {
                tvProductName.text = item.productName
                tvVarian.text = item.variantName
                adsPrice.text = CurrencyUtils.formatRupiah(sum)
            }
        }

        private fun ViewBinding.setupRemainingStock(item: ProductLocalDb) {
            binding.apply {
                tvRemainingProduk.text = " ${item.stock}"
                if (item.stock!! < 10) {
                    tvRemaining.setTextColor(Color.RED)
                    tvRemainingProduk.setTextColor(Color.RED)
                } else {
                    tvRemaining.setTextColor(
                        ContextCompat.getColor(
                            root.context,
                            R.color.stockColor
                        )
                    )
                    tvRemainingProduk.setTextColor(
                        ContextCompat.getColor(
                            root.context,
                            R.color.stockColor
                        )
                    )
                }
            }
        }

        private fun ViewBinding.setupImageAndClickListeners(item: ProductLocalDb) {
            binding.apply {
                Glide.with(root.context)
                    .load(item.image)
                    .into(ivProductCart)

                deleteButton.setOnClickListener {
                    onItemDeleteClickCallback?.onItemDeleteClick(item)
                }

                root.setOnClickListener {
                    onItemClickCallback?.onItemClick(item)
                }
            }
        }

        private fun ViewBinding.setupQuantityValidation(item: ProductLocalDb) {
            binding.apply {
                quantity.btnDecrease.setOnClickListener {
                    if (counterQuantity <= 1) {
                        showSnackbar(root.context.getString(R.string.not_null))
                    } else {
                        counterQuantity--
                        updateCount()
                        cartViewModel.updateCartItemQuantity(item.productId, counterQuantity)
                        quantity.btnIncrease.isSelected = false
                    }
                }

                quantity.btnIncrease.setOnClickListener {
                    if (item.stock!! <= counterQuantity) {
                        showSnackbar(root.context.getString(R.string.emptyStock))
                    } else {
                        counterQuantity++
                        updateCount()
                        cartViewModel.updateCartItemQuantity(item.productId, counterQuantity)
                        quantity.btnIncrease.isSelected = false
                    }
                }
            }
        }

        private fun ViewBinding.setupCheckbox(item: ProductLocalDb) {
            binding.apply {
                checkBoxItems.isChecked = item.selected

                checkBoxItems.setOnClickListener {
                    cartViewModel.updateCartItemCheckbox(
                        listOf(item.productId),
                        checkBoxItems.isChecked
                    )
                }
            }
        }

        private fun ViewBinding.showSnackbar(message: String) {
            Snackbar.make(
                root,
                message,
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }
}

class CartProductDiffCallback : DiffUtil.ItemCallback<ProductLocalDb>() {
    override fun areItemsTheSame(
        oldItem: ProductLocalDb,
        newItem: ProductLocalDb,
    ): Boolean {
        return oldItem.productId == newItem.productId
    }

    override fun areContentsTheSame(
        oldItem: ProductLocalDb,
        newItem: ProductLocalDb,
    ): Boolean {
        return oldItem == newItem
    }
}
