package com.megi.ecommerce.ui.main.transaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.ItemTransactionBinding
import com.megi.ecommerce.datasource.network.TransactionDataResponse
import com.megi.ecommerce.helper.CurrencyUtils

class TransactionAdapter(private val dataList: List<TransactionDataResponse>) :
    RecyclerView.Adapter<TransactionAdapter.MainViewHolder>() {
    private var itemClickListener: TransactionDataClickListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainViewHolder {
        val binding =
            ItemTransactionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val item = dataList[position]
        holder.bind(item)
    }

    override fun getItemCount() = dataList.size

    interface TransactionDataClickListener {
        fun onItemClick(label: TransactionDataResponse)
    }

    fun setItemClickListener(listener: TransactionDataClickListener) {
        itemClickListener = listener
    }

    inner class MainViewHolder(private val binding: ItemTransactionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TransactionDataResponse) {
            binding.apply {
                tvTanggalTransaksi.text = item.date
                tvNamaBarang.text = item.name
                tvTotalBelanja.text = CurrencyUtils.formatRupiah(item.total)
                val countItem = item.items?.size
                tvJmlBarang.text = countItem.toString()

                Glide.with(binding.root.context)
                    .load(item.image)
                    .into(ivBarang)

                // Condition to determine the visibility of the button
                if (item.rating == 0 || item.review.isNullOrEmpty()) {
                    btnReviewToResponse.visibility = View.VISIBLE
                } else {
                    btnReviewToResponse.visibility = View.GONE
                }

                // Set a click listener on the button
                btnReviewToResponse.setOnClickListener {
                    itemClickListener?.onItemClick(item)
                }

                cardTransaction.startAnimation(
                    AnimationUtils.loadAnimation(
                        itemView.context,
                        R.anim.slide_in
                    )
                )
            }
        }
    }
}
