package com.megi.ecommerce.ui.prelogin.login

import androidx.lifecycle.ViewModel
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.Auth
import com.megi.ecommerce.repository.PreLoginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: PreLoginRepository,
    private val pref: SharedPreference
) :
    ViewModel() {
    fun doLogin(token: String, auth: Auth) = repository.login(token, auth)

    fun getFirsInstall(): Boolean {
        return pref.getIsFirstInstall()
    }
}
