package com.megi.ecommerce.ui.main.store.detail

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.slideIn
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.Share
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.InputChip
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SuggestionChip
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.R
import com.megi.ecommerce.datasource.local.room.AppExecutor
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.ProductDao
import com.megi.ecommerce.datasource.network.GetProductDetailItemResponse
import com.megi.ecommerce.datasource.network.asCheckoutProduct
import com.megi.ecommerce.datasource.network.asProductLocalDb
import com.megi.ecommerce.datasource.network.asWishlistProduct
import com.megi.ecommerce.helper.CurrencyUtils
import com.megi.ecommerce.ui.main.store.theme.EcommerceTheme
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException

@Suppress("DEPRECATION")
@AndroidEntryPoint
class DetailProductCompose : Fragment() {
    private val viewModel: DetailProductViewModel by viewModels()
    private var idProduct: String? = null
    private var variantName: String? = null
    private var variantPrice: Int? = 0
    private var product: GetProductDetailItemResponse? = null
    private lateinit var appExecutors: AppExecutor
    private lateinit var database: ProductDatabase
    private lateinit var productDao: ProductDao
    private var counter = 0
    private var isChecked: Boolean = false
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        idProduct = arguments?.getString("id_product")

        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                EcommerceTheme {
                    val productDetailState by viewModel.dataDetail.observeAsState()
                    if (productDetailState == null) {
                        if (product != null) {
                            viewModel.getCartById(product?.productId!!)
                        } else {
                            viewModel.getCartById(idProduct!!)
                        }
                    }

                    var isVisible by remember { mutableStateOf(false) }
                    var data: GetProductDetailItemResponse? = null

                    productDetailState?.let {
                        appExecutors = AppExecutor()
                        database = ProductDatabase.getDatabase(requireContext())
                        productDao = database.productDao()
                        when (it) {
                            is ResultResponse.Success -> {
                                LoadingScreen(isLoading = false)
                                data = it.data.data
                                isVisible = true
                                product = data
                                lifecycleScope.launch {
                                    withContext(Dispatchers.IO) {
                                        val wishlistLocalDb =
                                            data?.asWishlistProduct(variantName, variantPrice)

                                        val checkWishlist =
                                            viewModel.getProductWishlistById(wishlistLocalDb!!.productId)
                                        if (!(checkWishlist?.productId.isNullOrEmpty())) {
                                            isChecked = true
                                        } else {
                                            isChecked
                                        }
                                    }
                                }
                            }

                            is ResultResponse.Loading -> {
                                LoadingScreen(isLoading = true)
                            }

                            is ResultResponse.Error -> {
                                EcommerceTheme {
                                    ErrorLayout(it.exception)
                                }
                            }
                        }
                    }

                    AnimatedVisibility(
                        visible = isVisible,
                        enter = slideIn(
                            animationSpec = tween(durationMillis = 600),
                            initialOffset = { fullSize -> IntOffset(0, -fullSize.height) }
                        )
                    ) {
                        data?.let { DetailProduct(it) }
                    }
                }
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun DetailProduct(product: GetProductDetailItemResponse) {
        val poppins = FontFamily(
            Font(R.font.poppins_regular, FontWeight.Medium),
            Font(R.font.poppins_semibold, FontWeight.SemiBold),
            Font(R.font.poppins_bold, FontWeight.Bold)
        )
        Scaffold(
            // top bar
            topBar = {
                TopAppBar(
                    title = {
                        Text(
                            text = stringResource(id = R.string.txt_barDetailProduct),
                            fontFamily = poppins,
                            fontWeight = FontWeight.Medium,
                            fontSize = 22.sp
                        )
                    },
                    navigationIcon = {
                        IconButton(
                            onClick = {
                                findNavController().navigateUp()
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Default.ArrowBack,
                                contentDescription = null
                            )
                        }
                    }
                )
                Divider(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 62.dp)
                )
            },

            // bottom bar
            bottomBar = {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(60.dp),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    OutlinedButton(
                        onClick = {
                            val productLocalDb =
                                product.asProductLocalDb(variantName, variantPrice)
                            val productCheckout = arrayListOf(
                                productLocalDb.asCheckoutProduct(
                                    variantName ?: "RAM 16GB",
                                    variantPrice
                                )
                            )
                            val bundle = bundleOf("data_product" to productCheckout)
                            (requireActivity() as MainActivity).detailToCheckout(bundle)

                            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT) {
                                param(FirebaseAnalytics.Param.ITEM_ID, productLocalDb.productId)
                                param(FirebaseAnalytics.Param.ITEM_NAME, productLocalDb.productName)
                            }
                        },
                        modifier = Modifier
                            .padding(end = 8.dp)
                            .fillMaxWidth()
                            .padding(start = 16.dp)
                            .weight(1F, true)
                    ) {
                        Text(
                            text = stringResource(id = R.string.txt_btnBuyNow),
                            fontSize = 14.sp,
                            fontFamily = poppins,
                            fontWeight = FontWeight.Normal,
                        )
                    }
                    Button(
                        onClick = {
                            appExecutors.diskIO.execute {
                                val productLocalDb =
                                    product.asWishlistProduct(variantName, variantPrice)
                                val checkProductExist =
                                    viewModel.getCartById(productLocalDb.productId)

                                // firebase add to cart
                                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
                                    param(FirebaseAnalytics.Param.ITEM_ID, productLocalDb.productId)
                                    param(
                                        FirebaseAnalytics.Param.ITEM_NAME,
                                        productLocalDb.productName
                                    )
                                    param(FirebaseAnalytics.Param.CURRENCY, "IDR")
                                }

                                if (checkProductExist?.productId != null) {
                                    if (checkProductExist.quantity < checkProductExist.stock!!) {
                                        counter = checkProductExist.quantity
                                        counter++
                                        viewModel.updateCartItemQuantity(
                                            checkProductExist.productId,
                                            counter
                                        )
                                        showSnackbar(R.string.sn_cart)
                                    } else {
                                        showSnackbar(R.string.emptyStock)
                                    }
                                } else {
                                    viewModel.addToCart(
                                        productLocalDb.productId,
                                        productLocalDb.productName,
                                        productLocalDb.productPrice,
                                        productLocalDb.image,
                                        productLocalDb.store,
                                        productLocalDb.sale,
                                        productLocalDb.stock,
                                        productLocalDb.totalRating,
                                        productLocalDb.productRating,
                                        variantName ?: "RAM 16GB",
                                        variantPrice
                                    )
                                    showSnackbar(R.string.sn_cart)
                                }
                            }
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(end = 16.dp)
                            .padding(start = 8.dp)
                            .weight(1F, true),
                    ) {
                        Text(
                            text = stringResource(id = R.string.btn_addCart),
                            fontSize = 14.sp,
                            fontFamily = poppins,
                            fontWeight = FontWeight.Normal

                        )
                    }
                }
                Divider()
            },

            // content
            content = { innerPadding ->
                Column(
                    modifier = Modifier
                        .padding(innerPadding)
                        .fillMaxSize(),
                    verticalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    EcommerceTheme {
                        ProductContent(product)
                    }
                }
            }
        )
    }

    @OptIn(
        ExperimentalFoundationApi::class,
        ExperimentalGlideComposeApi::class,
        ExperimentalLayoutApi::class,
        ExperimentalMaterial3Api::class
    )
    @Composable
    fun ProductContent(product: GetProductDetailItemResponse) {
        appExecutors = AppExecutor()
        database = ProductDatabase.getDatabase(requireContext())
        productDao = database.productDao()

        val poppins = FontFamily(
            Font(R.font.poppins_medium, FontWeight.Medium),
            Font(R.font.poppins_semibold, FontWeight.SemiBold),
            Font(R.font.poppins_bold, FontWeight.Bold)
        )
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM) {
            param(FirebaseAnalytics.Param.ITEM_NAME, product.productName)
            param(FirebaseAnalytics.Param.CURRENCY, product.productPrice.toString())
            param(FirebaseAnalytics.Param.VALUE, product.productPrice.toString())
        }

        var selectedVariantName: String by remember {
            val data = product.productVariant.firstOrNull()!!.variantName
            mutableStateOf(
                data
            )
        }
        var selectedVariantPrice: Int by remember {
            val data = product.productVariant.firstOrNull()!!.variantPrice
            mutableStateOf(data)
        }
        variantName = selectedVariantName
        variantPrice = selectedVariantPrice

        val isDarkMode = isSystemInDarkTheme()
        val pageCount = product.image.size
        val pagerState = rememberPagerState { pageCount }

        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
                .padding(bottom = 16.dp)
        ) {
            Box(
                modifier = Modifier.fillMaxSize()
            ) {
                // image pager
                HorizontalPager(
                    state = pagerState,
                    modifier = Modifier.fillMaxWidth()
                ) { page ->
                    GlideImage(
                        model = product.image.getOrElse(page) { "" },
                        contentDescription = null,
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(360.dp),
                        contentScale = ContentScale.Crop,
                    )
                }
                Row(
                    modifier = Modifier
                        .padding(top = 340.dp)
                        .fillMaxWidth()
                        .background(color = Color.Transparent),
                    horizontalArrangement = Arrangement.Center
                ) {
                    repeat(pageCount) { iteration ->
                        if (pageCount > 1) {
                            Box(
                                modifier = Modifier
                                    .padding(4.dp)
                                    .size(8.dp)
                                    .clip(CircleShape)
                                    .background(
                                        color = if (pagerState.currentPage == iteration) primaryColor() else Color.LightGray,
                                        shape = CircleShape
                                    )
                            )
                        }
                    }
                }
            }
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                val price =
                    CurrencyUtils.formatRupiah(product.productPrice.plus(selectedVariantPrice))
                Text(
                    text = "$price",
                    modifier = Modifier.padding(start = 16.dp),
                    fontSize = 20.sp,
                    fontFamily = poppins,
                    fontWeight = FontWeight.SemiBold
                )

                Spacer(modifier = Modifier.weight(1f))
                IconButton(onClick = {
                    firebaseAnalytics.logEvent("btn_share_clicked", null)
                    val deepLink = "Product : ${product.productName}\n" +
                        "Price : ${CurrencyUtils.formatRupiah(product.productPrice)}\n" +
                        "Link : http://ecommerce.com/products/${product.productId}"

                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/plain"
                    shareIntent.putExtra(Intent.EXTRA_TEXT, deepLink)

                    startActivity(Intent.createChooser(shareIntent, null))
                }) {
                    Icon(
                        imageVector = Icons.Default.Share,
                        contentDescription = "Share",
                        tint = if (isDarkMode) {
                            Color.White
                        } else {
                            Color.Black
                        },
                        modifier = Modifier
                            .width(24.dp)
                            .height(24.dp)
                    )
                }
                var icon by remember {
                    mutableStateOf(
                        if (isChecked) {
                            Icons.Filled.Favorite
                        } else {
                            Icons.Default.FavoriteBorder
                        }
                    )
                }
                IconButton(
                    onClick = {
                        appExecutors.diskIO.execute {
                            isChecked = !isChecked
                            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
                                val wishlistLocalDb =
                                    product.asWishlistProduct(
                                        selectedVariantName,
                                        selectedVariantPrice
                                    )
                                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_WISHLIST) {
                                    param(
                                        FirebaseAnalytics.Param.ITEM_ID,
                                        wishlistLocalDb.productId
                                    )
                                    param(
                                        FirebaseAnalytics.Param.ITEM_NAME,
                                        wishlistLocalDb.productName
                                    )
                                    param(FirebaseAnalytics.Param.CURRENCY, "IDR")
                                }
                                if (isChecked) {
                                    viewModel.addToWishList(
                                        wishlistLocalDb.productId,
                                        wishlistLocalDb.productName,
                                        wishlistLocalDb.productPrice,
                                        wishlistLocalDb.image,
                                        wishlistLocalDb.store,
                                        wishlistLocalDb.sale,
                                        wishlistLocalDb.stock,
                                        wishlistLocalDb.totalRating,
                                        wishlistLocalDb.productRating,
                                        variantName ?: "RAM 16GB",
                                        variantPrice
                                    )
                                    icon = Icons.Default.Favorite
                                    showSnackbar(R.string.snAdd_wishList)
                                } else {
                                    viewModel.deleteWishList(wishlistLocalDb.productId)
                                    icon = Icons.Default.FavoriteBorder
                                    showSnackbar(R.string.sndelete_wishlist)
                                }
                            }
                        }
                    },
                    modifier = Modifier.padding(end = 8.dp)
                ) {
                    Icon(
                        imageVector = icon,
                        contentDescription = "Favorite",

                        modifier = Modifier
                            .width(24.dp)
                            .height(24.dp)
                    )
                }
            }
            Column {
                Text(
                    modifier = Modifier.padding(start = 16.dp, end = 16.dp),
                    text = product.productName,
                    fontSize = 14.sp
                )

                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 16.dp, end = 16.dp)
                ) {
                    Text(
                        text = stringResource(id = R.string.sold) + " " + product.sale,
                        fontSize = 12.sp,
                        fontFamily = poppins,
                        fontWeight = FontWeight.Normal,
                    )
                    // chip rating
                    SuggestionChip(
                        onClick = {},
                        modifier = Modifier
                            .padding(start = 8.dp)
                            .defaultMinSize(minHeight = 24.dp)
                            .clickable(enabled = false) {},
                        label = { chipRating(text = "${product.productRating} (${product.sale})") },
                        icon = {
                            Icon(
                                painter = painterResource(R.drawable.ic_star),
                                contentDescription = null,
                                modifier = Modifier
                                    .width(18.dp)
                                    .height(18.dp)
                                    .wrapContentHeight(Alignment.CenterVertically),
                            )
                        },
                    )
                }
                Divider()
                Text(
                    modifier = Modifier.padding(start = 16.dp, top = 8.dp),
                    text = stringResource(id = R.string.txt_varian),
                    fontFamily = poppins,
                    fontWeight = FontWeight.Medium,
                    fontSize = 16.sp,
                )

                FlowRow(Modifier.padding(start = 8.dp)) {
                    product.productVariant.forEach { variant ->
                        val isSelected = selectedVariantName == variant.variantName
                        // chip variant
                        InputChip(
                            label = {
                                Text(
                                    text = variant.variantName,
                                    fontFamily = poppins,
                                )
                            },
                            selected = isSelected,
                            onClick = {
                                if (!isSelected) {
                                    selectedVariantName = variant.variantName
                                    selectedVariantPrice = variant.variantPrice
                                }
                            },
                            modifier = Modifier.padding(start = 8.dp),
                        )
                    }
                }
                Divider()
                Text(
                    modifier = Modifier.padding(start = 16.dp, top = 8.dp),
                    text = stringResource(id = R.string.descProduct),
                    fontFamily = poppins,
                    fontSize = 16.sp,
                )
                Text(
                    modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 8.dp),
                    text = product.description,
                    fontFamily = poppins,
                    fontSize = 14.sp
                )
                Divider()
                Row(
                    modifier = Modifier.padding(start = 16.dp, end = 16.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = stringResource(id = R.string.txt_reviewBar),
                        fontFamily = poppins,
                        fontWeight = FontWeight.Medium,
                        fontSize = 16.sp,
                        modifier = Modifier.padding(start = 4.dp)
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    TextButton(onClick = {
                        firebaseAnalytics.logEvent("btn_seeAll_review_clicked", null)
                        val bundle = bundleOf("id_product" to product.productId)
                        (requireActivity() as MainActivity).toViewReview(bundle)
                    }) {
                        Text(
                            text = stringResource(id = R.string.txt_allReview),
                            fontFamily = poppins,
                            fontWeight = FontWeight.Medium,
                            fontSize = 12.sp
                        )
                    }
                }
            }
            Row(
                modifier = Modifier
                    .padding(start = 16.dp, end = 16.dp)
                    .fillMaxWidth()
                    .height(40.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    imageVector = Icons.Default.Star,
                    contentDescription = "Star",
                    tint = if (isDarkMode) {
                        Color.White
                    } else {
                        Color.Black
                    },
                    modifier = Modifier
                        .padding(bottom = 2.dp)
                        .width(24.dp)
                        .height(24.dp),
                )
                val customTextStyle = TextStyle(
                    fontFamily = poppins,
                )
                Text(
                    text = "${product.productRating}",
                    fontSize = 20.sp,
                    lineHeight = 16.sp,
                    textAlign = TextAlign.Center,
                    style = customTextStyle,
                    fontFamily = poppins
                )
                Text(
                    text = "/5.0",
                    fontSize = 10.sp,
                    fontFamily = poppins,
                    modifier = Modifier.padding(top = 8.dp)
                )
                Column(
                    verticalArrangement = Arrangement.Center,
                    modifier = Modifier.padding(start = 48.dp)
                ) {
                    Row(horizontalArrangement = Arrangement.Center) {
                        Text(
                            text = "${product.totalSatisfaction}%" + " " + stringResource(id = R.string.txt_review_dummy),
                            style = customTextStyle,
                            fontSize = 12.sp,
                            lineHeight = 16.sp,
                            fontFamily = poppins,
                            fontWeight = FontWeight.SemiBold
                        )
                    }
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        modifier = Modifier
                    ) {
                        Text(
                            text = "${product.totalRating} " + stringResource(id = R.string.txt_rating) + " " + "." + " " + "${product.totalReview} " + stringResource(
                                id = R.string.rate
                            ),
                            fontSize = 12.sp,
                            lineHeight = 16.sp,
                            fontFamily = poppins
                        )
                    }
                }
            }
        }
    }

    private fun showSnackbar(messageResId: Int) {
        Snackbar.make(
            requireView(),
            requireContext().getString(messageResId),
            Snackbar.LENGTH_SHORT
        ).show()
    }

    @Composable
    fun LoadingScreen(isLoading: Boolean) {
        if (isLoading) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .size(48.dp),
                    strokeWidth = 4.dp
                )
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun ErrorLayout(t: Throwable) {
        val poppins = FontFamily(
            Font(R.font.poppins_medium, FontWeight.Medium),
            Font(R.font.poppins_semibold, FontWeight.SemiBold),
            Font(R.font.poppins_bold, FontWeight.Bold)
        )
        Scaffold(
            // top bar error
            topBar = {
                TopAppBar(
                    title = {
                        Text(
                            text = stringResource(id = R.string.txt_barDetailProduct),
                            fontFamily = poppins,
                            fontWeight = FontWeight.Medium,
                            fontSize = 22.sp
                        )
                    },
                    navigationIcon = {
                        IconButton(
                            onClick = {
                                findNavController().navigateUp()
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Default.ArrowBack,
                                contentDescription = null
                            )
                        }
                    }
                )
                Divider(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 62.dp)
                )
            },

            // content
            content = { innerPadding ->
                Column(
                    modifier = Modifier
                        .padding(innerPadding)
                        .fillMaxSize(),
                    verticalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    EcommerceTheme {
                        Column(
                            Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Column(
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Image(
                                    painter = painterResource(id = R.drawable.error_img),
                                    contentDescription = "error"
                                )
                                when (t) {
                                    is retrofit2.HttpException -> {
                                        if (t.response()?.code() == 404) {
                                            ErrorContent(
                                                code = stringResource(id = R.string.txt_empty),
                                                message = stringResource(id = R.string.no_data),
                                                buttonText = stringResource(id = R.string.txt_refresh)
                                            )
                                        } else if (t.response()?.code() == 401) {
                                            ErrorContent(
                                                code = t.response()?.code().toString(),
                                                message = t.response()?.message().toString(),
                                                buttonText = stringResource(id = R.string.txt_refresh)
                                            )
                                            (requireActivity() as MainActivity).logOut()
                                        } else {
                                            ErrorContent(
                                                code = t.response()?.code().toString(),
                                                message = t.response()?.message().toString(),
                                                buttonText = stringResource(id = R.string.txt_refresh)
                                            )
                                        }
                                    }

                                    is IOException -> {
                                        ErrorContent(
                                            code = stringResource(R.string.txt_conenction),
                                            message = stringResource(R.string.not_connection),
                                            buttonText = stringResource(id = R.string.txt_refresh)
                                        )
                                    }

                                    else -> {
                                        ErrorContent(
                                            code = stringResource(R.string.internal_error),
                                            message = t.message.toString(),
                                            buttonText = stringResource(id = R.string.txt_refresh)
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        )
    }

    @Composable
    fun ErrorContent(code: String, message: String, buttonText: String) {
        val poppins = FontFamily(
            Font(R.font.poppins_medium, FontWeight.Medium),
            Font(R.font.poppins_semibold, FontWeight.SemiBold),
            Font(R.font.poppins_bold, FontWeight.Bold)
        )
        Text(
            text = code,
            modifier = Modifier
                .padding(top = 8.dp),
            style = TextStyle(
                fontFamily = poppins,
                fontWeight = FontWeight.Medium,
                fontSize = 32.sp,
            )
        )
        Text(
            text = message,
            modifier = Modifier
                .padding(top = 4.dp),
            fontFamily = poppins,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp,
        )
        Button(
            onClick = {
                viewModel.getDetailData()
            },
            modifier = Modifier.padding(top = 8.dp)
        ) {
            Text(
                text = buttonText,
                fontFamily = poppins
            )
        }
    }

    @Composable
    private fun chipRating(text: String) {
        Text(
            text = text,
            fontSize = 12.sp,
            style = MaterialTheme.typography.bodyLarge
        )
    }

    @Composable
    fun primaryColor(): Color {
        return colorResource(id = R.color.purple)
    }

    @Preview(showBackground = true)
    @Composable
    fun DetailProductPreview() {
        EcommerceTheme {
//            ProductContent(product = sampleProduct)
        }
    }
}
