package com.megi.ecommerce.ui.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        darkMode()
        language()
        logOut()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    // check dark mode
    private fun darkMode() {
        binding?.switchTheme!!.isChecked = viewModel.getTheme()
        binding!!.switchTheme.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
            viewModel.saveDarkMode(isChecked)
        }
    }

    // check language switch
    private fun language() {
        binding?.switchLanguage?.isChecked = viewModel.getLanguage()

        binding?.switchLanguage?.setOnCheckedChangeListener { _, isChecked ->
            viewModel.saveLanguage(isChecked)

            if (isChecked) {
                val languageIn: LocaleListCompat = LocaleListCompat.forLanguageTags("in")
                AppCompatDelegate.setApplicationLocales(languageIn)
            } else {
                val languageEn: LocaleListCompat = LocaleListCompat.forLanguageTags("en")
                AppCompatDelegate.setApplicationLocales(languageEn)
            }
        }
    }

    // logout
    private fun logOut() {
        binding?.apply {
            btnLogout.setOnClickListener {
                Firebase.messaging.unsubscribeFromTopic("promo")
                firebaseAnalytics.logEvent("btn_logout_clicked", null)
                (requireActivity() as MainActivity).logOut()
//                clearDb()
            }
        }
    }

    private fun clearDb() {
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            viewModel.clearDb()
        }
    }
}
