package com.megi.ecommerce.ui.main.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentTransactionBinding
import com.megi.ecommerce.datasource.network.TransactionDataResponse
import com.megi.ecommerce.datasource.network.asPaymentDataResponse
import com.megi.ecommerce.utils.ResultResponse
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException
import java.io.IOException

@AndroidEntryPoint
class TransactionFragment : Fragment() {
    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding
    private val viewModel: TransactionViewModel by viewModels()
    private lateinit var adapter: TransactionAdapter
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTransactionBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.dataTransaction
            .observe(viewLifecycleOwner) { result ->
                when (result) {
                    is ResultResponse.Success -> handleSuccess(result.data.data)
                    is ResultResponse.Loading -> {
                        binding?.progressCircular?.show()
                    }

                    is ResultResponse.Error -> {
                        handleError()
                        setErrorMessage(result.exception)
                    }
                }
            }
        binding?.restartButton?.setOnClickListener {
            refreshAfterError()
        }
    }

    private fun handleSuccess(data: List<TransactionDataResponse>) {
        binding?.progressCircular?.hide()
        if (data.isEmpty()) {
            binding?.apply {
                linearErrorLayout.visibility = View.VISIBLE
                rvTransaction.visibility = View.GONE
            }
        } else {
            adapter = TransactionAdapter(data)
            val linearLayout = LinearLayoutManager(requireContext())
            linearLayout.reverseLayout = true
            linearLayout.stackFromEnd = true
            binding?.rvTransaction?.layoutManager = linearLayout
            binding?.rvTransaction?.adapter = adapter

            adapter.setItemClickListener(object :
                TransactionAdapter.TransactionDataClickListener {
                override fun onItemClick(label: TransactionDataResponse) {
                    firebaseAnalytics.logEvent(
                        "btn_review_toStatus_clicked",
                        null
                    )
                    val convert = label.asPaymentDataResponse(label.review, label.rating)
                    val bundle = Bundle().apply {
                        putParcelable("data_response", convert)
                    }
                    val bundleId = Bundle().apply {
                        putString("originFragment", "transaction")
                    }
                    val combinedBundle = Bundle().apply {
                        putAll(bundle)
                        putAll(bundleId)
                    }
                    (requireActivity() as MainActivity).goToSuccessPayment(
                        combinedBundle
                    )
                }
            })
        }
    }

    private fun setErrorMessage(error: Throwable) {
        when (error) {
            is HttpException -> {
                if (error.response()?.code() == 404) {
                    binding?.errorTypeText?.text = getString(R.string.txt_empty)
                    binding?.errorTypeInfo?.text = getString(R.string.no_data)
                    binding?.restartButton?.text = getString(R.string.txt_refresh)
                } else if (error.response()?.code() == 401) {
                    binding?.errorTypeText?.text = error.response()?.code().toString()
                    binding?.errorTypeInfo?.text = error.response()?.message()
                    (requireActivity() as MainActivity).logOut()
                } else {
                    binding?.errorTypeText?.text = error.response()?.code().toString()
                    binding?.errorTypeInfo?.text = error.response()?.message()
                    binding?.restartButton?.text = getString(R.string.txt_refresh)
                }
            }

            is IOException -> {
                binding?.errorTypeText?.text = getString(R.string.txt_conenction)
                binding?.errorTypeInfo?.text = getString(R.string.not_connection)
                binding?.restartButton?.text = getString(R.string.txt_refresh)
            }

            else -> {
                binding?.errorTypeText?.text = getString(R.string.internal_error)
                binding?.errorTypeInfo?.text = error.message
                binding?.restartButton?.text = getString(R.string.txt_logout)
            }
        }
    }

    private fun handleError() {
        binding?.apply {
            progressCircular.hide()
            rvTransaction.visibility = View.GONE
            linearErrorLayout.visibility = View.VISIBLE
        }
    }

    private fun refreshAfterError() {
        viewModel.getTransaction(viewModel.getAccessToken().toString())

        binding?.linearErrorLayout?.visibility = View.INVISIBLE
        binding?.rvTransaction?.visibility = View.VISIBLE
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
