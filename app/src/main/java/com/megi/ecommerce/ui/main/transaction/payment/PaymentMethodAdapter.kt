package com.megi.ecommerce.ui.main.transaction.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.megi.ecommerce.databinding.ItemPaymentMethodBinding
import com.megi.ecommerce.datasource.network.PaymentMethodCategoryResponse
import com.megi.ecommerce.datasource.network.PaymentMethodItemResponse

class PaymentMethodAdapter(private val dataList: List<PaymentMethodCategoryResponse>) :
    RecyclerView.Adapter<PaymentMethodAdapter.MainViewHolder>() {

    private var itemClickListener: PaymentMethodItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val binding = ItemPaymentMethodBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val item = dataList[position]
        holder.bind(item)
    }

    override fun getItemCount() = dataList.size

    interface PaymentMethodItemClickListener {
        fun onItemClick(label: PaymentMethodItemResponse)
    }

    fun setItemClickListener(listener: PaymentMethodItemClickListener) {
        itemClickListener = listener
    }

    inner class MainViewHolder(private val binding: ItemPaymentMethodBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: PaymentMethodCategoryResponse) {
            binding.apply {
                tvPaymentMethods.text = item.title

                val adapter = PaymentAdapter(
                    item.item,
                    object : PaymentAdapter.PaymentItemClickListener {
                        override fun onItemClick(item: PaymentMethodItemResponse) {
                            if (item.status) {
                                itemClickListener?.onItemClick(item)
                            } else {
                                binding.root.setOnClickListener(null)
                            }
                        }
                    }
                )

                rvPaymentMethods.layoutManager = LinearLayoutManager(root.context)
                rvPaymentMethods.adapter = adapter
            }
        }
    }
}
