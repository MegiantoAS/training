package com.megi.ecommerce.ui.main.store.filter

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.fragment.app.setFragmentResult
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.databinding.DialogBottomSheetBinding
import com.megi.ecommerce.ui.main.store.main.StoreFragment.Companion.CHIP_CATEGORY
import com.megi.ecommerce.ui.main.store.main.StoreFragment.Companion.CHIP_HIGHEST
import com.megi.ecommerce.ui.main.store.main.StoreFragment.Companion.CHIP_LOWEST
import com.megi.ecommerce.ui.main.store.main.StoreFragment.Companion.CHIP_SORT
import com.megi.ecommerce.ui.main.store.main.StoreFragment.Companion.FILTER
import dagger.hilt.android.AndroidEntryPoint

@Suppress("DEPRECATION")
@AndroidEntryPoint
class StoreBottomSheet : BottomSheetDialogFragment() {
    private var _binding: DialogBottomSheetBinding? = null
    private val binding get() = _binding!!
    private var sort: String? = null
    private var category: String? = null
    private var lowest: String? = null
    private var highest: String? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = DialogBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        selectChipProduct()
        btnToStore()
        btnResetData()
        checkResetButtonVisibility()

        val modalBottomSheetBehavior = (dialog as BottomSheetDialog).behavior

        modalBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        binding.apply {
            chipGroupSort.setOnCheckedChangeListener { _, _ ->
                buttonReset.visibility = View.VISIBLE
            }
            chipGroupCategory.setOnCheckedChangeListener { _, _ ->
                buttonReset.visibility = View.VISIBLE
            }
        }
    }

    // button show product
    private fun btnToStore() {
        binding.showProduct.setOnClickListener {
            sendData()
            dismiss()
            firebaseAnalytic()
        }
    }

    private fun btnResetData() {
        binding.apply {
            buttonReset.setOnClickListener {
                chipGroupSort.clearCheck()
                chipGroupCategory.clearCheck()
                fieldLowest.text?.clear()
                fieldHighest.text?.clear()
                buttonReset.visibility = View.GONE
            }
        }

        binding.fieldLowest.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                checkResetButtonVisibility()
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
            }
        })

        binding.fieldHighest.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                checkResetButtonVisibility()
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
            }
        })
    }

    private fun checkResetButtonVisibility() {
        binding.apply {
            if (fieldLowest.text.isNullOrEmpty() &&
                fieldHighest.text.isNullOrEmpty() &&
                chipGroupSort.checkedChipId == View.NO_ID &&
                chipGroupCategory.checkedChipId == View.NO_ID
            ) {
                buttonReset.visibility = View.GONE
            } else {
                buttonReset.visibility = View.VISIBLE
            }
        }
    }

    // send data to store fragment page
    private fun sendData() {
        binding.apply {
            val sort: Chip? = chipGroupSort.findViewById(chipGroupSort.checkedChipId)
            val category: Chip? = chipGroupCategory.findViewById(chipGroupCategory.checkedChipId)
            val lowest = fieldLowest.text.toString()
            val highest = fieldHighest.text.toString()
            val bundle = Bundle().apply {
                putString(CHIP_SORT, sort?.text?.toString())
                putString(CHIP_CATEGORY, category?.text?.toString())
                putString(CHIP_LOWEST, lowest)
                putString(CHIP_HIGHEST, highest)
            }
            setFragmentResult(FILTER, bundle)
        }
    }

    // select chip product
    private fun selectChipProduct() {
        binding.apply {
            sort = arguments?.getString(CHIP_SORT)
            category = arguments?.getString(CHIP_CATEGORY)
            lowest = arguments?.getString(CHIP_LOWEST)
            highest = arguments?.getString(CHIP_HIGHEST)

            chipGroupSort.children.forEach { chipProduct ->
                if ((chipProduct as Chip).text == sort) {
                    chipProduct.isChecked = true
                    buttonReset.visibility = View.VISIBLE
                }
            }

            chipGroupCategory.children.forEach { chipProduct ->
                if ((chipProduct as Chip).text == category) {
                    chipProduct.isChecked = true
                    buttonReset.visibility = View.VISIBLE
                }
            }
            val highestValue = highest ?: ""
            val lowestValue = lowest ?: ""

            if (highestValue.isEmpty() || highestValue == "null") {
                fieldHighest.setText("")
            } else {
                fieldHighest.setText(highestValue.replace(Regex("[Rp<> ,.]"), ""))
            }

            if (lowestValue.isEmpty() || lowestValue == "null") {
                fieldLowest.setText("")
            } else {
                fieldLowest.setText(lowestValue.replace(Regex("[Rp<> ,.]"), ""))
            }
        }
    }

    // firebase
    private fun firebaseAnalytic() {
        binding.apply {
            val sort: Chip? = chipGroupSort.findViewById(chipGroupSort.checkedChipId)
            val category: Chip? = chipGroupCategory.findViewById(chipGroupCategory.checkedChipId)

            val sorts = Bundle()
            sorts.putString(FirebaseAnalytics.Param.ITEM_NAME, "sort")
            sorts.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "${sort?.text}")

            val cat = Bundle()
            cat.putString(FirebaseAnalytics.Param.ITEM_NAME, "category")
            cat.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "${category?.text}")

            val params = Bundle()
            params.putParcelableArray(
                FirebaseAnalytics.Param.ITEMS,
                arrayOf(sorts, cat)
            )
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, params)
        }
    }

    companion object {
        const val TAG = "BottomSheet"

        @JvmStatic
        fun newInstance(
            category: String?,
            sort: String?,
            highest: String?,
            lowest: String?,
        ): StoreBottomSheet {
            val myFragment = StoreBottomSheet()
            val args = Bundle().apply {
                putString(CHIP_SORT, sort)
                putString(CHIP_CATEGORY, category)
                putString(CHIP_HIGHEST, highest)
                putString(CHIP_LOWEST, lowest)
            }
            myFragment.arguments = args
            return myFragment
        }
    }
}
