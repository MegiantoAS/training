package com.megi.ecommerce.ui.main.store.search

import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.databinding.FragmentSearchDialogBinding
import com.megi.ecommerce.ui.main.store.main.StoreFragment
import com.megi.ecommerce.ui.main.store.main.StoreFragment.Companion.SEARCH
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.IOException

@Suppress("DEPRECATION")
@AndroidEntryPoint
class SearchDialogFragment : DialogFragment() {
    private var _binding: FragmentSearchDialogBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SearchViewModel by viewModels()
    private lateinit var adapter: SearchAdapter
    private var search: String? = ""
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSearch()
        onBack()

        binding.fieldSearch.requestFocus()
        val imm = requireContext().getSystemService(InputMethodManager::class.java)
        imm.showSoftInput(binding.fieldSearch, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun setSearch() {
        adapter = SearchAdapter()
        binding.rvSearch.apply {
            this.layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = this@SearchDialogFragment.adapter
        }

        // call argument for field search
        search = arguments?.getString(SEARCH)
        if (search.isNullOrEmpty() || search == "null") {
            binding.fieldSearch.setText("")
        } else {
            binding.fieldSearch.setText(search)
        }

        binding.fieldSearch.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                event.action == KeyEvent.ACTION_DOWN &&
                actionId == KeyEvent.KEYCODE_ENTER
            ) {
                lifecycleScope.launch {
                    delay(1000)
                    setData(v.text.toString())
                    dismiss()
                }
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        // observe search result
        binding.fieldSearch.addTextChangedListener(object : TextWatcher {
            var job: Job? = null

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                binding.progressBar.visibility = View.VISIBLE
                job?.cancel()
                job = lifecycleScope.launch {
                    delay(1000)

                    if (isNetworkAvailable()) {
                        try {
                            val response = viewModel.searchProduct(
                                viewModel.getAccessToken().toString(),
                                s.toString()
                            )

                            response.observe(viewLifecycleOwner) {
                                adapter.setSearchProduct(it.data)
                                binding.progressBar.visibility = View.GONE
                            }

                            adapter.setOnItemClickCallBack(object :
                                SearchAdapter.OnItemClickCallback {
                                override fun onItemClicked(data: String) {
                                    // firebase analytic
                                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS) {
                                        param(FirebaseAnalytics.Param.SEARCH_TERM, data)
                                    }

                                    setData(data)
                                    dismiss()
                                }
                            })
                        } catch (e: IOException) {
                            binding.progressBar.visibility = View.GONE
                            Toast.makeText(
                                requireContext(),
                                "IOException: An error occurred",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        binding.progressBar.visibility = View.GONE
                        Toast.makeText(
                            requireContext(),
                            "No internet connection",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun afterTextChanged(s: Editable?) {}

            private fun isNetworkAvailable(): Boolean {
                val connectivityManager =
                    requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connectivityManager.activeNetworkInfo
                return networkInfo != null && networkInfo.isConnected
            }
        })
    }

    private fun onBack() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    dismiss()
                }
            }
        )
    }

    private fun setData(data: String) {
        val bundle = Bundle().apply {
            putString(SEARCH, data)
        }
        requireActivity().supportFragmentManager.setFragmentResult(
            StoreFragment.FILTER,
            bundle
        )
    }

    companion object {
        const val TAG = "SearchDialogFragment"

        @JvmStatic
        fun newInstance(
            search: String?,
        ): SearchDialogFragment {
            val myFragment = SearchDialogFragment()

            val args = Bundle().apply {
                putString(SEARCH, search)
            }
            myFragment.arguments = args

            return myFragment
        }
    }
}
