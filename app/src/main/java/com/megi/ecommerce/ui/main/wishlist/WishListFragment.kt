package com.megi.ecommerce.ui.main.wishlist

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.megi.ecommerce.MainActivity
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentWishListBinding
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class WishListFragment : Fragment() {

    private var _binding: FragmentWishListBinding? = null
    private val binding get() = _binding
    private val wishListViewModel: WishListViewModel by viewModels()
    private lateinit var adapter: WishListAdapter
    private lateinit var gridLayoutManager: GridLayoutManager
    private var counter = 0
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentWishListBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getWishList()
        binding?.ivLayout?.setOnClickListener {
            wishListViewModel.isList = !wishListViewModel.isList
            changeToggle()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getWishList() {
        binding?.apply {
            adapter = WishListAdapter(requireContext(), wishListViewModel, firebaseAnalytics)
            gridLayoutManager = GridLayoutManager(requireContext(), 1)
            rvWishlist.layoutManager = gridLayoutManager
            rvWishlist.adapter = adapter

            wishListViewModel.getWishlistProduct().observe(viewLifecycleOwner) {
                adapter.submitList(it)

                binding?.apply {
                    if (it.isEmpty()) {
                        linearErrorLayout.visibility = View.VISIBLE
                        rvWishlist.visibility = View.GONE
                        quantityTogle.visibility = View.GONE
                    } else {
                        linearErrorLayout.visibility = View.GONE
                        rvWishlist.visibility = View.VISIBLE
                    }
                }

                adapter.setOnItemClickCallback(object :
                    WishListAdapter.OnItemClickCallback {
                    override fun onItemClick(position: WishlistProduct) {
                        firebaseAnalytics.logEvent("addCart_clicked", null)
                        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
                            val checkCartProduct = wishListViewModel.getCartById(position.productId)

                            if (checkCartProduct?.productId != null) {
                                if (checkCartProduct.quantity < checkCartProduct.stock!!) {
                                    counter = checkCartProduct.quantity
                                    counter++
                                    wishListViewModel.updateCartItemQuantity(
                                        checkCartProduct.productId,
                                        counter
                                    )
                                    showSnackbar(R.string.sn_cart)
                                } else {
                                    showSnackbar(R.string.emptyStock)
                                }
                            } else {
                                wishListViewModel.addToCart(
                                    position.productId,
                                    position.productName,
                                    position.productPrice,
                                    position.image,
                                    position.store,
                                    position.sale,
                                    position.stock,
                                    position.totalRating,
                                    position.productRating,
                                    position.variantName,
                                    position.variantPrice
                                )
                                showSnackbar(R.string.sn_cart)
                            }
                        }
                    }

                    override fun onItemClickCard(data: String) {
                        val bundle = bundleOf("id_product" to data)
                        (requireActivity() as MainActivity).toDetailProduct(bundle)
                    }
                })
                tvTotalBarang.text = "${it.size} "
            }
        }
    }

    private fun changeToggle() {
        adapter.currentType = if (wishListViewModel.isList) ViewType.LINEAR else ViewType.GRID

        val imageRes = ContextCompat.getDrawable(
            requireContext(),
            if (wishListViewModel.isList) R.drawable.ic_list_bulleted else R.drawable.ic_grid
        )
        binding?.ivLayout?.setImageDrawable(imageRes)

        gridLayoutManager.spanCount = if (wishListViewModel.isList) 1 else 2
    }

    private fun showSnackbar(messageResId: Int) {
        Snackbar.make(
            requireView(),
            requireContext().getString(messageResId),
            Snackbar.LENGTH_SHORT
        ).show()
    }
}
