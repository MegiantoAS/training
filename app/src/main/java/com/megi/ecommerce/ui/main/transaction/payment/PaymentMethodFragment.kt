package com.megi.ecommerce.ui.main.transaction.payment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.google.gson.GsonBuilder
import com.megi.ecommerce.R
import com.megi.ecommerce.databinding.FragmentPaymentMethodBinding
import com.megi.ecommerce.datasource.network.PaymentMethodItemResponse
import com.megi.ecommerce.datasource.network.PaymentMethodResponse
import dagger.hilt.android.AndroidEntryPoint

@Suppress("DEPRECATION")
@AndroidEntryPoint
class PaymentMethodFragment : Fragment() {
    private var _binding: FragmentPaymentMethodBinding? = null
    private val binding get() = _binding
    private lateinit var adapter: PaymentMethodAdapter
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPaymentMethodBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.topAppBar?.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        getDataPayment()
    }

    private fun getDataPayment() {
        val remoteConfig: FirebaseRemoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 3600
        }

        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_payment)

        remoteConfig.fetchAndActivate()
            .addOnCompleteListener(requireActivity()) {
                getData()
            }

        remoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                Log.d(TAG, "Updated keys: " + configUpdate.updatedKeys)
                if (configUpdate.updatedKeys.contains("payment")) {
                    remoteConfig.activate().addOnCompleteListener {
                        getData()
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                Log.w(TAG, "Config update error with code: " + error.code, error)
            }
        })
    }

    private fun getData() {
        val remoteConfig = Firebase.remoteConfig
        val dataPayment = GsonBuilder().create().fromJson(
            remoteConfig[PAYMENT_PARAM].asString(),
            PaymentMethodResponse::class.java
        )
        binding?.progressCircular?.hide()

        adapter = PaymentMethodAdapter(dataPayment.data)
        val linearLayout = LinearLayoutManager(requireContext())
        binding?.rvPaymentMethods?.layoutManager = linearLayout
        binding?.rvPaymentMethods?.adapter = adapter

        adapter.setItemClickListener(object :
            PaymentMethodAdapter.PaymentMethodItemClickListener {
            override fun onItemClick(label: PaymentMethodItemResponse) {
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO) {
                    param(FirebaseAnalytics.Param.ITEM_NAME, label.label)
                }
                val bundle = bundleOf("payment" to label)
                findNavController().previousBackStackEntry?.savedStateHandle?.set(
                    "payment",
                    bundle
                )
                findNavController().popBackStack()
            }
        })
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val PAYMENT_PARAM = "payment"
    }
}
