package com.megi.ecommerce.helper

import java.text.DecimalFormat
import java.util.Locale

object CurrencyUtils {
    private val rupiahFormat =
        DecimalFormat.getCurrencyInstance(Locale("id", "ID")) as DecimalFormat

    init {
        rupiahFormat.maximumFractionDigits = 0
        rupiahFormat.minimumFractionDigits = 0
    }

    fun setRupiah(amount: Double?): String? {
        return rupiahFormat.format(amount ?: 0.0)
    }

    fun formatRupiah(amount: Int?): String? {
        return setRupiah(amount?.toDouble())
    }
}
