package com.megi.ecommerce.datatest.viewModel.store

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import com.megi.ecommerce.datasource.network.GetProductDetailItemResponse
import com.megi.ecommerce.datasource.network.GetProductDetailResponse
import com.megi.ecommerce.datasource.network.ProductVariant
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.repository.StoreRepository
import com.megi.ecommerce.repository.WishlistRepository
import com.megi.ecommerce.ui.main.store.detail.DetailProductViewModel
import com.megi.ecommerce.utils.DummyRoom.productWishlist
import com.megi.ecommerce.utils.ResultResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class DetailViewModelTest {

    @Mock
    private lateinit var storeRepository: StoreRepository

    @Mock
    private lateinit var cartRepository: CartRepository

    @Mock
    private lateinit var wishlistRepository: WishlistRepository

    @Mock
    private lateinit var sharedPref: SharedPreference

    @Mock
    private lateinit var savedStateHandle: SavedStateHandle

    private lateinit var viewModel: DetailProductViewModel
    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainDispatcherRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        whenever(sharedPref.getAccessToken()).thenReturn("12345")

        val productId = "123"
        whenever(savedStateHandle.get<String>("id_product")).thenReturn(productId)

        viewModel = DetailProductViewModel(
            storeRepository,
            cartRepository,
            wishlistRepository,
            sharedPref,
            savedStateHandle
        )

        // Set the test coroutine dispatcher
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @After
    fun tearDown() {
        // Reset test coroutine dispatcher after the test
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun getDetailData() {
        // Expected access token to be used
        val accessToken = "12345"

        // Create mock data for the response
        val mockVariant = ProductVariant("Variant 1", 30)
        val mockItemResponse = GetProductDetailItemResponse(
            "12345",
            "Product ABC",
            50,
            listOf("image_url_1", "image_url_2"),
            "Brand XYZ",
            "Product description",
            "Store Name",
            10,
            100,
            200,
            4,
            3,
            4f,
            listOf(mockVariant)
        )

        val mockResponse = ResultResponse.Success(
            GetProductDetailResponse(
                200,
                "Success",
                mockItemResponse
            )
        )

        // Create a MutableLiveData to simulate LiveData behavior
        val liveData = MutableLiveData<ResultResponse<GetProductDetailResponse>>()
        // Set the value of LiveData with mock response
        liveData.postValue(mockResponse)

        // Mock the repository call to return the LiveData
        Mockito.`when`(storeRepository.getProductDetail(anyString(), anyString()))
            .thenReturn(liveData)

        // Create an observer for LiveData in ViewModel
        val observer = mock<Observer<ResultResponse<GetProductDetailResponse>>>()
        viewModel.dataDetail.observeForever(observer)

        // Trigger the function to be tested
        viewModel.getDetailData()

        // Verify that the correct methods are called with proper arguments
        Mockito.verify(storeRepository).getProductDetail(accessToken, "123")

        // Verify onChanged is triggered with the mock response
        Mockito.verify(observer).onChanged(mockResponse)
    }

//    @Test
//    fun addToCartTest() = runBlocking {
//        val productId = "123"
//        val productName = "Sample Product"
//        val productPrice = 100
//        val image = "sample_image_url"
//        val store = "Sample Store"
//        val sale = 10
//        val stock: Int? = 20
//        val rating = 4
//        val productRating = 4.5f
//        val variantName = "Sample Variant"
//        val variantPrice: Int? = 50
//
//        viewModel.addToCart(
//            productId,
//            productName,
//            productPrice,
//            image,
//            store,
//            sale,
//            stock,
//            rating,
//            productRating,
//            variantName,
//            variantPrice
//        )
//
//        verify(cartRepository).addToCart(any())
//    }

//    @Test
//    fun updateCartItemQuantityTest() = runBlocking {
//        val productId = "123"
//        val newQuantity = 5
//        delay(100)
//        viewModel.updateCartItemQuantity(productId, newQuantity)
//        verify(cartRepository).updateCartItemQuantity(productId, newQuantity)
//    }

    @Test
    fun addToWishListTest() = runBlocking {
        val productId = "12345"
        val productName = "Sample Product"
        val productPrice = 100
        val image = "sample_image_url"
        val store = "Sample Store"
        val sale = 10
        val stock: Int? = 20
        val rating = 4
        val productRating = 4.5f
        val variantName = "Sample Variant"
        val variantPrice: Int? = 50

        // Perform addToWishList function
        viewModel.addToWishList(
            productId,
            productName,
            productPrice,
            image,
            store,
            sale,
            stock,
            rating,
            productRating,
            variantName,
            variantPrice
        )

        val actual = argumentCaptor<WishlistProduct>()
        verify(wishlistRepository).addWishList(actual.capture())
        assertEquals(productWishlist.productId, actual.firstValue.productId)
    }

    @Test
    fun getProductWishlistByIdTest() = runBlocking {
        val productId = "123"
        whenever(wishlistRepository.getProductWishlistById(productId)).thenReturn(mock())

        val wishlistProduct = viewModel.getProductWishlistById(productId)

        verify(wishlistRepository).getProductWishlistById(productId)
        assertNotNull(wishlistProduct)
    }

    @Test
    fun deleteWishListTest() = runBlocking {
        val productId = "123"
        viewModel.deleteWishList(productId)
        verify(wishlistRepository).deleteWishList(productId)
    }

    @Test
    fun getCartByIdTest() = runBlocking {
        val productId = "123"
        whenever(cartRepository.getCartById(productId)).thenReturn(mock())

        val productLocalDb = viewModel.getCartById(productId)
        verify(cartRepository).getCartById(productId)
        assertNotNull(productLocalDb)
    }
}
