package com.megi.ecommerce.datatest.viewModel.prelogin

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.DataResponse
import com.megi.ecommerce.datasource.network.ResultResponse
import com.megi.ecommerce.repository.PreLoginRepository
import com.megi.ecommerce.ui.prelogin.login.LoginViewModel
import com.megi.ecommerce.utils.DummyRoom.requestAuth
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class LoginViewModelTest {

    private lateinit var repository: PreLoginRepository
    private lateinit var apiService: ApiService
    private lateinit var sharedPref: SharedPreference
    private lateinit var viewModel: LoginViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        apiService = mock()
        sharedPref = mock()
        repository = PreLoginRepository(apiService, sharedPref)
        viewModel = LoginViewModel(repository, sharedPref)
    }

    @Test
    fun loginViewModelTest() = runTest {
        val expectedResponse = DataResponse(
            code = 200,
            data = ResultResponse(
                "dummy",
                "dummy",
                "dummy",
                "dummy",
                600
            ),
            message = "Success"
        )
        whenever(apiService.login("123", requestAuth)).thenReturn(expectedResponse)
        val liveDataResult = viewModel.doLogin("123", requestAuth)
        liveDataResult.observeForever {
            if (it is com.megi.ecommerce.utils.ResultResponse.Success) {
                assertEquals(expectedResponse.data, it.data.data)
            }
        }
    }

    @Test
    fun loginViewModelErrorTest() = runTest {
        val error = RuntimeException()
        whenever(apiService.login("123", requestAuth)).thenThrow(error)
        val result = viewModel.doLogin("123", requestAuth)
        result.observeForever {
            if (it is com.megi.ecommerce.utils.ResultResponse.Error) {
                assertEquals(it, com.megi.ecommerce.utils.ResultResponse.Error(error))
            }
        }
    }

    @Test
    fun getFirsInstallTest() = runTest {
        sharedPref.onBoarding(false)
        val checkFirstInstallData = viewModel.getFirsInstall()
        assertEquals(false, checkFirstInstallData)
    }
}
