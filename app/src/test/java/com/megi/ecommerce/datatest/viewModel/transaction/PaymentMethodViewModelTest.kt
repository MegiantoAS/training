package com.megi.ecommerce.datatest.viewModel.transaction

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.ui.main.transaction.payment.PaymentViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class PaymentMethodViewModelTest {

    private lateinit var sharedPref: SharedPreference
    private lateinit var viewModel: PaymentViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        sharedPref = mock()

        val token = "12345"
        whenever(sharedPref.getAccessToken()).thenReturn(token)

        viewModel = PaymentViewModel(sharedPref)
    }

    @Test
    fun `test getAccessToken`() {
        val token = viewModel.getAccessToken()
        Assert.assertEquals("12345", token)
    }
}
