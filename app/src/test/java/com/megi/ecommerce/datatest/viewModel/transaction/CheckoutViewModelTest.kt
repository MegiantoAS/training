package com.megi.ecommerce.datatest.viewModel.transaction

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.Payment
import com.megi.ecommerce.datasource.network.PaymentDataResponse
import com.megi.ecommerce.datasource.network.PaymentItem
import com.megi.ecommerce.datasource.network.PaymentResponse
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.repository.TransactionRepository
import com.megi.ecommerce.ui.main.transaction.checkout.CheckoutViewModel
import com.megi.ecommerce.utils.ResultResponse
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class CheckoutViewModelTest {

    private lateinit var transactionRepository: TransactionRepository
    private lateinit var cartRepository: CartRepository
    private lateinit var sharedPref: SharedPreference
    private lateinit var viewModel: CheckoutViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        transactionRepository = mock()
        cartRepository = mock()
        sharedPref = mock()

        val token = "12345"
        whenever(sharedPref.getAccessToken()).thenReturn(token)

        viewModel = CheckoutViewModel(transactionRepository, cartRepository, sharedPref)
    }

    @Test
    fun `test buyProducts success`() {
        val token = "12345"
        val payment = Payment(
            "Credit Card",
            listOf(
                PaymentItem("product_1", "variant_1", 2),
                PaymentItem("product_2", "variant_2", 1)
            )
        )
        val responseData = PaymentResponse(
            200,
            "Success",
            PaymentDataResponse(
                "invoice_1",
                true,
                "2023-11-28",
                "12:00",
                "Credit Card",
                500,
                null,
                null
            )
        )
        val liveData = MutableLiveData<ResultResponse<PaymentResponse>>()

        Mockito.`when`(transactionRepository.buyProducts(token, payment))
            .thenReturn(liveData)

        val mockedResponse = ResultResponse.Success(responseData)
        liveData.value = mockedResponse

        viewModel.buyProducts(token, payment)

        val observedValue = viewModel.buyProducts(token, payment).value
        val expectedValue = mockedResponse

        assertEquals(expectedValue, observedValue)
    }

    @Test
    fun `test updateCartItemQuantity`() = runBlockingTest {
        val productId = "product_id"
        val newQuantity = 5

        viewModel.updateCartItemQuantity(productId, newQuantity)

        delay(100)
        verify(cartRepository).updateCartItemQuantity(productId, newQuantity)
    }

    @Test
    fun `test removeFromCartAll`() = runBlockingTest {
        val productIds = listOf("id_1", "id_2", "id_3")
        viewModel.removeFromCartAll(productIds)

        // Simulate delay to allow the CoroutineScope to execute
        delay(100)
        verify(cartRepository).removeFromCartAll(productIds)
    }

    @Test
    fun `test getAccessToken`() {
        val token = viewModel.getAccessToken()
        assertEquals("12345", token)
    }
}
