package com.megi.ecommerce.datatest.viewModel.menutop

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.ProductDao
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.ui.menutop.cart.CartViewModel
import com.megi.ecommerce.utils.DummyRoom.cartProduct
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class CartViewModelTest {
    private lateinit var viewModel: CartViewModel
    private lateinit var appDatabase: ProductDatabase
    private lateinit var cartDao: ProductDao
    private lateinit var cartRepository: CartRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(context, ProductDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        cartDao = mock()
        cartRepository = CartRepository(cartDao)
        viewModel = CartViewModel(cartRepository)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    fun getCartTest() = runTest {
        runBlocking {
            val product = listOf(cartProduct)
            val expectedResponse = MutableLiveData<List<ProductLocalDb>>()
            expectedResponse.value = product

            whenever(cartDao.getCartProducts()).thenReturn(expectedResponse)
            val checkData = viewModel.getCartItem()
            checkData.observeForever {
                val expected = expectedResponse.value?.firstOrNull()
                Assert.assertEquals(expected, it.firstOrNull())
            }
        }
    }

    @Test
    fun updateCartItemQuantityTest() = runTest {
        runBlocking {
            whenever(cartDao.updateCartItemQuantity("12345", 4)).thenReturn(Unit)
            viewModel.updateCartItemQuantity("12345", 4)
            val checkData = viewModel.updateCartItemQuantity("12345", 4)
            Assert.assertEquals(Unit, checkData)
        }
    }

    @Test
    fun deleteItemCartTest() {
        runBlocking {
            whenever(cartDao.removeFromCart("12345")).thenReturn(Unit)
            viewModel.deleteFromCart("12345")
            val checkData = viewModel.deleteFromCart("12345")
            Assert.assertEquals(Unit, checkData)
        }
    }

    @Test
    fun updateCartItemCheckboxTest() = runTest {
        whenever(cartDao.updateCartItemCheckbox(listOf("12345"), true)).thenReturn(Unit)
        viewModel.updateCartItemCheckbox(listOf("12345"), true)
        val checkData = viewModel.updateCartItemCheckbox(listOf("12345"), true)
        Assert.assertEquals(Unit, checkData)
    }

    @Test
    fun deleteCartAllTest() = runTest {
        whenever(cartDao.removeFromCartAll(listOf("12345"))).thenReturn(Unit)
        viewModel.removeFromCartAll(listOf("12345"))
        val checkData = viewModel.removeFromCartAll(listOf("12345"))
        Assert.assertEquals(Unit, checkData)
    }
}
