package com.megi.ecommerce.datatest.viewModel.transaction

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.lifecycle.liveData
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.TransactionDataResponse
import com.megi.ecommerce.datasource.network.TransactionResponse
import com.megi.ecommerce.repository.TransactionRepository
import com.megi.ecommerce.ui.main.transaction.TransactionViewModel
import com.megi.ecommerce.utils.ResultResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class TransactionViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    // Mocks
    private val transactionRepository: TransactionRepository = mock()
    private val sharedPreference: SharedPreference = mock()

    private lateinit var viewModel: TransactionViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
        viewModel = TransactionViewModel(transactionRepository, sharedPreference)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun `test getTransaction success`() = testScope.runBlockingTest {
        val accessToken = "123"
        val expectedInvoiceId = "1235"
        val mockedResponse = ResultResponse.Success(
            TransactionResponse(
                code = 200,
                message = "Success",
                data = listOf(
                    TransactionDataResponse(
                        invoiceId = expectedInvoiceId,
                        status = true,
                        date = "2023-12-14",
                        time = "15:30",
                        payment = "Credit Card",
                        total = 500,
                        items = null,
                        rating = 4,
                        review = "Great service!",
                        image = "url_image",
                        name = "Transaction Name"
                    )
                )
            )
        )

        val liveDataMock = liveData<ResultResponse<TransactionResponse>> {
            emit(mockedResponse)
        }

        whenever(sharedPreference.getAccessToken()).thenReturn(accessToken)
        whenever(transactionRepository.getTransaction(accessToken)).thenReturn(liveDataMock)

        // Create Observer
        val observer: Observer<ResultResponse<TransactionResponse>> = mock()
        viewModel.dataTransaction.observeForever(observer)

        // When
        viewModel.getTransaction(accessToken)

        // Then
        verify(transactionRepository).getTransaction(accessToken)
        verify(observer).onChanged(mockedResponse)
        verify(sharedPreference).getAccessToken()
        assertEquals(accessToken, viewModel.getAccessToken())
        assertEquals(mockedResponse, viewModel.dataTransaction.value)

        val transactionData = (mockedResponse).data.data[0]
        assertEquals(expectedInvoiceId, transactionData.invoiceId)
    }

    @Test
    fun `test getAccessToken`() {
        val expectedToken = "12345"
        val viewModel = TransactionViewModel(transactionRepository, sharedPreference)
        `when`(sharedPreference.getAccessToken()).thenReturn(expectedToken)

        val actualToken = viewModel.getAccessToken()
        assertEquals(expectedToken, actualToken)
    }
}
