package com.megi.ecommerce.datatest.viewModel.store

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.SearchResponse
import com.megi.ecommerce.repository.StoreRepository
import com.megi.ecommerce.ui.main.store.search.SearchViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.eq
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class SearchViewModelTest {

    private lateinit var repository: StoreRepository
    private lateinit var sharedPref: SharedPreference
    private lateinit var viewModel: SearchViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        repository = mock()
        sharedPref = mock()
        viewModel = SearchViewModel(repository, sharedPref)
    }

    @Test
    fun searchProductTest() {
        // Mock data
        val token = "valid_token"
        val query = "search_query"
        val expectedResponse = SearchResponse(
            200,
            "Success",
            listOf("")
        )

        whenever(sharedPref.getAccessToken()).thenReturn(token)
        val liveData = MutableLiveData<SearchResponse>()

        whenever(repository.searchProduct(eq(token), eq(query))).thenReturn(liveData)
        viewModel.searchProduct(token, query).observeForever { }

        // Verify that the repository method is called with the correct parameters
        verify(repository).searchProduct(eq(token), eq(query))
        liveData.postValue(expectedResponse)

        viewModel.searchProduct(token, query).observeForever {
            assertEquals(expectedResponse, it)
        }
    }

    @Test
    fun getAccessTokenTest() {
        val token = "valid_token"
        whenever(sharedPref.getAccessToken()).thenReturn(token)

        val accessToken = viewModel.getAccessToken()
        verify(sharedPref).getAccessToken()
        assertEquals(token, accessToken)
    }
}
