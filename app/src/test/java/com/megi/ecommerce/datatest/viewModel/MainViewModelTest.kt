package com.megi.ecommerce.datatest.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.repository.NotificationRepository
import com.megi.ecommerce.repository.WishlistRepository
import com.megi.ecommerce.ui.main.MainViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    @Mock
    lateinit var cartRepository: CartRepository

    @Mock
    lateinit var wishlistRepository: WishlistRepository

    @Mock
    lateinit var notificationRepository: NotificationRepository

    @Mock
    lateinit var pref: SharedPreference

    private lateinit var viewModel: MainViewModel
    private lateinit var savedStateHandle: SavedStateHandle

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        savedStateHandle = mock(SavedStateHandle::class.java)
        viewModel = MainViewModel(
            cartRepository,
            wishlistRepository,
            notificationRepository,
            pref,
            savedStateHandle
        )
    }

    @Test
    fun `test getCartItem`() {
        val mockCartItems = MutableLiveData<List<ProductLocalDb>>()
        Mockito.`when`(cartRepository.getCartItem()).thenReturn(mockCartItems)

        val observedCartItems = viewModel.getCartItem()
        assertEquals(mockCartItems, observedCartItems)
    }

    @Test
    fun `test getWishlistProduct`() {
        val mockWishlistItems = MutableLiveData<List<WishlistProduct>>()
        Mockito.`when`(wishlistRepository.getWishlistProduct()).thenReturn(mockWishlistItems)

        val observedWishlistItems = viewModel.getWishlistProduct()
        assertEquals(mockWishlistItems, observedWishlistItems)
    }

    @Test
    fun `test getAllNotification`() {
        val mockNotifications = MutableLiveData<List<NotificationEcommerce>>()
        Mockito.`when`(notificationRepository.getAllNotification()).thenReturn(mockNotifications)

        val observedNotifications = viewModel.getAllNotification()
        assertEquals(mockNotifications, observedNotifications)
    }

    @Test
    fun `test getAccessToken`() {
        val accessToken = "12345678"
        Mockito.`when`(pref.getAccessToken()).thenReturn(accessToken)

        val retrievedToken = viewModel.getAccessToken()
        assertEquals(accessToken, retrievedToken)
    }

    @Test
    fun `test getNameProfile`() {
        val name = "John Doe"
        Mockito.`when`(pref.getNameProfile()).thenReturn(name)

        val retrievedName = viewModel.getNameProfile()
        assertEquals(name, retrievedName)
    }
}
