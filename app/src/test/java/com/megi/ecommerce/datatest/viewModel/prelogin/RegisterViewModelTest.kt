package com.megi.ecommerce.datatest.viewModel.prelogin

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.DataResponse
import com.megi.ecommerce.datasource.network.ResultResponse
import com.megi.ecommerce.repository.PreLoginRepository
import com.megi.ecommerce.ui.prelogin.register.RegisterViewModel
import com.megi.ecommerce.utils.DummyRoom
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class RegisterViewModelTest {

    private lateinit var repository: PreLoginRepository
    private lateinit var apiService: ApiService
    private lateinit var sharedPref: SharedPreference
    private lateinit var viewModel: RegisterViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        apiService = mock()
        sharedPref = mock()
        repository = PreLoginRepository(apiService, sharedPref)
        viewModel = RegisterViewModel(repository)
    }

    @Test
    fun registerViewModelTest() = runTest {
        val expectedResponse = DataResponse(
            code = 200,
            data = ResultResponse(
                "megi",
                "megi.com",
                "12345",
                "12345",
                600
            ),
            message = "Success"
        )
        whenever(apiService.register("123", DummyRoom.requestAuth)).thenReturn(expectedResponse)
        val liveDataResult = viewModel.doRegister("123", DummyRoom.requestAuth)
        liveDataResult.observeForever {
            if (it is com.megi.ecommerce.utils.ResultResponse.Success) {
                Assert.assertEquals(expectedResponse.data, it.data.data)
            }
        }
    }

    @Test
    fun registerViewModelErrorTest() = runTest {
        val error = RuntimeException()
        whenever(apiService.register("123", DummyRoom.requestAuth)).thenThrow(error)
        val result = viewModel.doRegister("123", DummyRoom.requestAuth)
        result.observeForever {
            if (it is com.megi.ecommerce.utils.ResultResponse.Error) {
                Assert.assertEquals(it, com.megi.ecommerce.utils.ResultResponse.Error(error))
            }
        }
    }
}
