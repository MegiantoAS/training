package com.megi.ecommerce.datatest.viewModel.wishlist

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.ProductDao
import com.megi.ecommerce.datasource.local.room.dao.WishListDao
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.repository.WishlistRepository
import com.megi.ecommerce.ui.main.wishlist.WishListViewModel
import com.megi.ecommerce.utils.DummyRoom.productWishlist
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class WishListViewModelTest {
    private lateinit var viewModel: WishListViewModel
    private lateinit var appDatabase: ProductDatabase
    private lateinit var wishlistDao: WishListDao
    private lateinit var cartDao: ProductDao
    private lateinit var cartRepository: CartRepository
    private lateinit var wishlistRepository: WishlistRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(context, ProductDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        wishlistDao = mock()
        cartDao = mock()
        cartRepository = CartRepository(cartDao)
        wishlistRepository = WishlistRepository(wishlistDao)
        viewModel = WishListViewModel(wishlistRepository, cartRepository)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    fun addCartViewModelTest() = runTest {
        whenever(wishlistDao.addWishList(productWishlist)).thenReturn(Unit)
        val checkInsert = viewModel.addToCart(
            productId = "12345",
            productName = "Asus",
            productPrice = 2000000,
            image = "asus.com",
            store = "Asus Store",
            sale = 4,
            stock = 100,
            rating = 5,
            productRating = 3.5f,
            variantName = "black",
            variantPrice = 1200
        )
        assertEquals(Unit, checkInsert)
    }

    @Test
    fun getWishListTest() = runTest {
        runBlocking {
            val product = listOf(productWishlist)
            val expectedResponse = MutableLiveData<List<WishlistProduct>>()
            expectedResponse.value = product
            viewModel.addToCart(
                productId = "12345",
                productName = "Asus",
                productPrice = 2000000,
                image = "asus.com",
                store = "Asus Store",
                sale = 4,
                stock = 100,
                rating = 5,
                productRating = 3.5f,
                variantName = "black",
                variantPrice = 1200
            )

            whenever(wishlistDao.getWishListProduct()).thenReturn(expectedResponse)
            val checkData = viewModel.getWishlistProduct()
            checkData.observeForever {
                val expected = expectedResponse.value?.firstOrNull()
                assertEquals(expected, it.firstOrNull())
            }
        }
    }

    @Test
    fun deleteWishlistTest() = runTest {
        runBlocking {
            whenever(wishlistDao.deleteWishList("12345")).thenReturn(1)
            viewModel.deleteWishList("12345")
            val checkData = viewModel.deleteWishList("1245")
            assertEquals(Unit, checkData)
        }
    }

    @Test
    fun updateCartTest() = runTest {
        whenever(cartDao.updateCartItemQuantity("12345", 4)).thenReturn(Unit)
        viewModel.updateCartItemQuantity("12345", 4)
        val checkData = viewModel.updateCartItemQuantity("12345", 4)
        assertEquals(Unit, checkData)
    }
}
