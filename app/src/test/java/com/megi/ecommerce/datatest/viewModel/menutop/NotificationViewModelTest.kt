package com.megi.ecommerce.datatest.viewModel.menutop

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.NotificationDao
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce
import com.megi.ecommerce.repository.NotificationRepository
import com.megi.ecommerce.ui.menutop.notification.NotificationViewModel
import com.megi.ecommerce.utils.DummyRoom.notification
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class NotificationViewModelTest {
    private lateinit var viewModel: NotificationViewModel
    private lateinit var appDatabase: ProductDatabase
    private lateinit var notificationDao: NotificationDao
    private lateinit var notificationRepository: NotificationRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(context, ProductDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        notificationDao = mock()
        notificationDao = mock()
        notificationRepository = NotificationRepository(notificationDao)
        viewModel = NotificationViewModel(notificationRepository)
    }

    @Test
    fun testGetAllNotifications() {
        runBlocking {
            val expectedNotifications = MutableLiveData<List<NotificationEcommerce>>()
            expectedNotifications.value = listOf(notification)
            viewModel.addNotification(notification)
            whenever(notificationDao.getNotification()).thenReturn(expectedNotifications)

            val observedData = viewModel.getAllNotification()
            observedData?.observeForever {
                val expectedResponse = expectedNotifications.value?.firstOrNull()
                assertEquals(expectedResponse, it.firstOrNull())
            }
        }
    }

    @Test
    fun updateNotificationTest() {
        runBlocking {
            whenever(notificationDao.updateNotification(notification)).thenReturn(Unit)
            val actual = viewModel.updateNotification(notification)
            assertEquals(Unit, actual)
        }
    }
}
