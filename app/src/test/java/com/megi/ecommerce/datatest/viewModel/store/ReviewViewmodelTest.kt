package com.megi.ecommerce.datatest.viewModel.store

import com.megi.ecommerce.datasource.network.GetProductReviewItemResponse
import com.megi.ecommerce.datasource.network.GetReview
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetReviewTest {

    @Test
    fun `test GetReview data integrity`() {
        // Given
        val getProductReviewItemResponseList = listOf(
            GetProductReviewItemResponse(
                "John Doe",
                "user_image_url",
                4,
                "Great product!"
            ),
            GetProductReviewItemResponse(
                "Jane Smith",
                "user_image_url",
                5,
                "Excellent quality!"
            )
        )
        val getReview = GetReview(
            200,
            "Success",
            getProductReviewItemResponseList
        )

        // Then
        assert(getReview.code == 200)
        assert(getReview.message == "Success")
        assert(getReview.data.size == 2)

        assert(getReview.data[0].userName == "John Doe")
        assert(getReview.data[0].userImage == "user_image_url")
        assert(getReview.data[0].userRating == 4)
        assert(getReview.data[0].userReview == "Great product!")

        assert(getReview.data[1].userName == "Jane Smith")
        assert(getReview.data[1].userImage == "user_image_url")
        assert(getReview.data[1].userRating == 5)
        assert(getReview.data[1].userReview == "Excellent quality!")
    }

    @Test
    fun `test GetProductReviewItemResponse data integrity`() {
        // Given
        val getProductReviewItemResponse = GetProductReviewItemResponse(
            "John Doe",
            "user_image_url",
            4,
            "Great product!"
        )

        // Then
        assert(getProductReviewItemResponse.userName == "John Doe")
        assert(getProductReviewItemResponse.userImage == "user_image_url")
        assert(getProductReviewItemResponse.userRating == 4)
        assert(getProductReviewItemResponse.userReview == "Great product!")
    }
}
