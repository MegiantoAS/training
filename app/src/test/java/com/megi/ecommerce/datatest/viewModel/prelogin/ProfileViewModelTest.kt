package com.megi.ecommerce.datatest.viewModel.prelogin

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.ProfileResponse
import com.megi.ecommerce.datasource.network.ProfileResultResponse
import com.megi.ecommerce.repository.PreLoginRepository
import com.megi.ecommerce.ui.main.profile.ProfileViewModel
import com.megi.ecommerce.utils.ResultResponse
import kotlinx.coroutines.runBlocking
import okhttp3.MultipartBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class ProfileViewModelTest {
    private lateinit var repository: PreLoginRepository
    private lateinit var apiService: ApiService
    private lateinit var sharedPref: SharedPreference
    private lateinit var viewModel: ProfileViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        apiService = mock()
        sharedPref = mock()
        repository = PreLoginRepository(apiService, sharedPref)
        viewModel = ProfileViewModel(repository, sharedPref)
    }

    @Test
    fun `test profileViewModel success`() = runBlocking {
        val expectedResponse = ProfileResponse(
            code = 200,
            data = ProfileResultResponse(
                "uname",
                "image"
            ),
            message = "Success"
        )

        val userNamePart = MultipartBody.Part.createFormData("userName", "uname")
        val userImagePart = MultipartBody.Part.createFormData("userImage", "image")

        whenever(apiService.saveToProfile("123", userNamePart, userImagePart)).thenReturn(
            expectedResponse
        )
        val liveDataResult = viewModel.doProfile("123", userNamePart, userImagePart)
        liveDataResult.observeForever {
            if (it is ResultResponse.Success) {
                assertEquals(expectedResponse.data, it.data)
            }
        }
    }

    @Test
    fun `test getAccessToken from SharedPreference`() {
        val fakeToken = "fakeToken"
        whenever(sharedPref.getAccessToken()).thenReturn(fakeToken)
        val accessToken = viewModel.getAccessToken()
        assertEquals(fakeToken, accessToken)
    }
}
