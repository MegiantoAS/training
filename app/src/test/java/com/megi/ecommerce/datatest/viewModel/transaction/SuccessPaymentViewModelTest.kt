package com.megi.ecommerce.datatest.viewModel.transaction

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.RatingReview
import com.megi.ecommerce.repository.TransactionRepository
import com.megi.ecommerce.ui.main.transaction.successpayment.SuccessPaymentViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.kotlin.eq
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class SuccessPaymentViewModelTest {
    @Mock
    lateinit var transactionRepository: TransactionRepository

    @Mock
    lateinit var pref: SharedPreference

    private lateinit var viewModel: SuccessPaymentViewModel

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = SuccessPaymentViewModel(transactionRepository, pref)
    }

    @Test
    fun testGetRateProduct() {
        val token = "exampleToken"
        val ratingReview = RatingReview("12345")
        viewModel.getRateProduct(token, ratingReview)

        verify(transactionRepository).ratingProduct(eq(token), eq(ratingReview))
    }

    @Test
    fun testGetAccessToken() {
        val accessToken = "exampleAccessToken"
        whenever(pref.getAccessToken()).thenReturn(accessToken)

        val result = viewModel.getAccessToken()
        assertEquals(accessToken, result)
        verify(pref).getAccessToken()
    }
}
