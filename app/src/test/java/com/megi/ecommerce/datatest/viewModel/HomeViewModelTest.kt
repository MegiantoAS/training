package com.megi.ecommerce.datatest.viewModel

import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.ui.main.home.HomeViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    @Mock
    lateinit var productDatabase: ProductDatabase

    @Mock
    lateinit var pref: SharedPreference

    private lateinit var viewModel: HomeViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = HomeViewModel(productDatabase, pref)
    }

    @Test
    fun `test getTheme`() {
        val expectedTheme = true
        Mockito.`when`(pref.getDarkMode()).thenReturn(expectedTheme)

        val observedTheme = viewModel.getTheme()
        assertEquals(expectedTheme, observedTheme)
    }

    @Test
    fun `test getLanguage`() {
        val expectedLanguage = false
        Mockito.`when`(pref.getLanguage()).thenReturn(expectedLanguage)

        val observedLanguage = viewModel.getLanguage()
        assertEquals(expectedLanguage, observedLanguage)
    }

    @Test
    fun `test saveDarkMode`() {
        val darkMode = true

        viewModel.saveDarkMode(darkMode)
        Mockito.verify(pref).saveDarkMode(darkMode)
    }

    @Test
    fun `test saveLanguage`() {
        val language = false

        viewModel.saveLanguage(language)
        Mockito.verify(pref).saveLanguage(language)
    }

    @Test
    fun `test clearDb`() {
        viewModel.clearDb()
        Mockito.verify(productDatabase).clearAllTables()
    }
}
