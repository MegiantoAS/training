package com.megi.ecommerce.datatest.viewModel.store

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.network.ProductParam
import com.megi.ecommerce.repository.StoreRepository
import com.megi.ecommerce.ui.main.store.main.StoreViewModel
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class StoreViewModelTest {

    private lateinit var productDatabase: ProductDatabase
    private lateinit var storeRepository: StoreRepository
    private lateinit var sharedPref: SharedPreference
    private lateinit var viewModel: StoreViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        storeRepository = mock()
        sharedPref = mock()
        productDatabase = mock()

        val token = "valid_token"
        whenever(sharedPref.getAccessToken()).thenReturn(token)

        viewModel = StoreViewModel(productDatabase, storeRepository, sharedPref)
    }

    @Test
    fun queryProductTest() {
        val brand = "SomeBrand"
        val lowest = 100
        val highest = 500
        val sort = "ASC"

        viewModel.queryProduct(brand, lowest, highest, sort)

        val expectedParam = ProductParam("valid_token", null, brand, lowest, highest, sort)
        assertEquals(expectedParam, viewModel.param.value)
    }

    @Test
    fun resetParamTest() {
        viewModel.resetParam()

        val expectedParam = ProductParam("valid_token")
        assertEquals(expectedParam, viewModel.param.value)
    }

    @Test
    fun setSearchTest() {
        val search = "SomeSearchQuery"
        viewModel.setSearch(search)

        val expectedParam = ProductParam("valid_token", search)
        assertEquals(expectedParam, viewModel.param.value)
    }

    @Test
    fun clearDbTest() =
        runBlockingTest {
            viewModel.clearDb()
            verify(productDatabase).clearAllTables()
        }
}
