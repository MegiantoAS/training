package com.megi.ecommerce.repository

import androidx.lifecycle.LiveData
import com.megi.ecommerce.datasource.local.room.dao.WishListDao
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct

class WishlistRepository(
    private val wishListDao: WishListDao,
) {
    fun getProductWishlistById(id: String): WishlistProduct? {
        return wishListDao.getProductWishListById(id)
    }

    fun getWishlistProduct(): LiveData<List<WishlistProduct>> {
        return wishListDao.getWishListProduct()
    }

    suspend fun deleteWishList(id: String) {
        wishListDao.deleteWishList(id)
    }

    suspend fun addWishList(wishlistItem: WishlistProduct) {
        wishListDao.addWishList(wishlistItem)
    }
}
