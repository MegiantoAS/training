package com.megi.ecommerce.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.Payment
import com.megi.ecommerce.datasource.network.PaymentMethodResponse
import com.megi.ecommerce.datasource.network.PaymentResponse
import com.megi.ecommerce.datasource.network.RatingProductResponse
import com.megi.ecommerce.datasource.network.RatingReview
import com.megi.ecommerce.datasource.network.TransactionResponse
import com.megi.ecommerce.utils.ResultResponse

class TransactionRepository(private val apiService: ApiService) {
    // payment buy now
    fun buyProducts(token: String, payment: Payment): LiveData<ResultResponse<PaymentResponse>> =
        liveData {
            emit(ResultResponse.Loading)
            try {
                val response = apiService.buyProducts("Bearer $token", payment)
                emit(ResultResponse.Success(response))
            } catch (e: Exception) {
                emit(ResultResponse.Error(e))
            }
        }

    fun ratingProduct(
        token: String,
        rating: RatingReview
    ): LiveData<ResultResponse<RatingProductResponse>> = liveData {
        emit(ResultResponse.Loading)
        try {
            val response = apiService.getRating("Bearer $token", rating)
            emit(ResultResponse.Success(response))
        } catch (e: Exception) {
            emit(ResultResponse.Error(e))
        }
    }

    // payment
    fun getPaymentMethods(token: String): LiveData<ResultResponse<PaymentMethodResponse>> =
        liveData {
            emit(ResultResponse.Loading)
            try {
                val response = apiService.getPaymentMethods("Bearer $token")
                emit(ResultResponse.Success(response))
            } catch (e: Exception) {
                emit(ResultResponse.Error(e))
            }
        }

    // transaction
    fun getTransaction(token: String): LiveData<ResultResponse<TransactionResponse>> = liveData {
        emit(ResultResponse.Loading)
        try {
            val response = apiService.getTransactionHistory("Bearer $token")
            emit(ResultResponse.Success(response))
        } catch (e: Exception) {
            emit(ResultResponse.Error(e))
        }
    }
}
