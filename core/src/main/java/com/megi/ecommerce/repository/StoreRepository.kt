package com.megi.ecommerce.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.GetProductDetailResponse
import com.megi.ecommerce.datasource.network.GetProductsItemsResponse
import com.megi.ecommerce.datasource.network.GetReview
import com.megi.ecommerce.datasource.network.ProductStorePaging
import com.megi.ecommerce.datasource.network.SearchResponse
import com.megi.ecommerce.utils.ResultResponse

class StoreRepository(
    private val apiService: ApiService,
) {
    // get product
    fun getProductsPaging(
        token: String,
        search: String?,
        brand: String?,
        lowest: Int?,
        highest: Int?,
        sort: String?,
    ): LiveData<PagingData<GetProductsItemsResponse>> {
        return Pager(
            config = PagingConfig(pageSize = 10, prefetchDistance = 1),
            pagingSourceFactory = {
                ProductStorePaging(
                    apiService,
                    "Bearer $token",
                    search,
                    brand,
                    lowest,
                    highest,
                    sort
                )
            }
        ).liveData
    }

    // search
    fun searchProduct(
        token: String,
        query: String
    ): LiveData<SearchResponse> = liveData {
        try {
            val response = apiService.searchProduct("Bearer $token", query)
            emit(response)
        } catch (e: Exception) {
            throw IllegalArgumentException("empty")
        }
    }

    fun getProductDetail(
        token: String,
        id: String
    ): LiveData<ResultResponse<GetProductDetailResponse>> =
        liveData {
            emit(ResultResponse.Loading)
            try {
                val response = apiService.getDetailProducts("Bearer $token", id)
                emit(ResultResponse.Success(response))
            } catch (e: Exception) {
                emit(ResultResponse.Error(e))
            }
        }

    fun getReviewProduct(token: String, id: String): LiveData<ResultResponse<GetReview>> =
        liveData {
            emit(ResultResponse.Loading)
            try {
                val response = apiService.getReviewProducts("Bearer $token", id)
                emit(ResultResponse.Success(response))
            } catch (e: Exception) {
                emit(ResultResponse.Error(e))
            }
        }
}
