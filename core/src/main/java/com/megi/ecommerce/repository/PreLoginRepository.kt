package com.megi.ecommerce.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.Auth
import com.megi.ecommerce.datasource.network.DataResponse
import com.megi.ecommerce.datasource.network.ProfileResultResponse
import com.megi.ecommerce.utils.ResultResponse
import okhttp3.MultipartBody

class PreLoginRepository(
    private val apiService: ApiService,
    private val pref: SharedPreference,
) {

    fun login(token: String, auth: Auth): LiveData<ResultResponse<DataResponse>> = liveData {
        emit(ResultResponse.Loading)
        try {
            val response = apiService.login(token, auth)
            pref.saveAccessToken(
                response.data?.accessToken,
                response.data?.refreshToken
            )
            pref.saveNameProfile(response.data?.userName ?: "empty")
            emit(ResultResponse.Success(response))
        } catch (e: Exception) {
            emit(ResultResponse.Error(e))
        }
    }

    fun register(token: String, auth: Auth): LiveData<ResultResponse<DataResponse>> = liveData {
        emit(ResultResponse.Loading)
        try {
            val response = apiService.register(token, auth)
            pref.saveAccessToken(
                response.data?.accessToken,
                response.data?.refreshToken
            )
            emit(ResultResponse.Success(response))
        } catch (e: Exception) {
            emit(ResultResponse.Error(e))
        }
    }

    // profile
    fun saveToProfile(
        bearer: String,
        name: MultipartBody.Part,
        image: MultipartBody.Part?,
    ): LiveData<ResultResponse<ProfileResultResponse>> = liveData {
        emit(ResultResponse.Loading)
        try {
            val response = apiService.saveToProfile("Bearer $bearer", name, image)
            val resultResponse = response.data
            pref.saveNameProfile(resultResponse!!.userName)
            emit(ResultResponse.Success(resultResponse))
        } catch (e: Exception) {
            emit(ResultResponse.Error(e))
        }
    }
}
