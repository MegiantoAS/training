package com.megi.ecommerce.repository

import androidx.lifecycle.LiveData
import com.megi.ecommerce.datasource.local.room.dao.NotificationDao
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class NotificationRepository(
    private val notificationDao: NotificationDao
) {
    fun getAllNotification(): LiveData<List<NotificationEcommerce>>? {
        return runBlocking {
            withContext(Dispatchers.IO) {
                notificationDao.getNotification()
            }
        }
    }

    fun addNotification(notification: NotificationEcommerce) {
        return runBlocking {
            withContext(Dispatchers.IO) {
                notificationDao.addToNotification(notification)
            }
        }
    }

    fun updateNotification(notification: NotificationEcommerce) {
        CoroutineScope(Dispatchers.IO).launch {
            notificationDao.updateNotification(notification)
        }
    }
}
