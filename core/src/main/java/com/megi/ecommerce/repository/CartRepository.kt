package com.megi.ecommerce.repository

import androidx.lifecycle.LiveData
import com.megi.ecommerce.datasource.local.room.dao.ProductDao
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CartRepository @Inject constructor(
    private val cartDao: ProductDao
) {
    suspend fun addToCart(cartItem: ProductLocalDb) {
        cartDao.addToCart(cartItem)
    }

    fun getCartItem(): LiveData<List<ProductLocalDb>> {
        return cartDao.getCartProducts()
    }

    fun getCartById(id: String): ProductLocalDb? {
//        return cartDao.getProductById(id)
        return runBlocking {
            withContext(Dispatchers.IO) {
                cartDao?.getProductById(id)
            }
        }
    }

    fun updateCartItemQuantity(productId: String, newQuantity: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            cartDao.updateCartItemQuantity(productId, newQuantity)
        }
    }

    fun deleteFromCart(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            cartDao.removeFromCart(id)
        }
    }

    fun removeFromCartAll(productId: List<String>) {
        CoroutineScope(Dispatchers.IO).launch {
            cartDao.removeFromCartAll(productId)
        }
    }

    fun updateCartItemCheckbox(productId: List<String>, isSelected: Boolean) {
        CoroutineScope(Dispatchers.IO).launch {
            cartDao.updateCartItemCheckbox(productId, isSelected)
        }
    }
}
