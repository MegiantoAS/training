package com.megi.ecommerce.utils

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.RefreshResponse
import com.megi.ecommerce.datasource.network.TokenRequest
import com.megi.ecommerce.utils.Constant.API_KEY
import com.megi.ecommerce.utils.Constant.BASE_URL
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Interceptor(private val pref: SharedPreference, private val context: Context) :
    Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        val refreshToken = pref.getRefreshToken().toString()
        synchronized(this) {
            return runBlocking {
                try {
                    val newToken = getToken(refreshToken, context)
                    if (newToken != null) {
                        pref.saveAccessToken(
                            newToken.data?.accessToken,
                            newToken.data?.refreshToken
                        )
                        response.request
                            .newBuilder()
                            .header("Authorization", "Bearer ${newToken.data?.accessToken}")
                            .build()
                    } else {
                        null
                    }
                } catch (error: Throwable) {
                    null
                }
            }
        }
    }

    private suspend fun getToken(token: String, context: Context): RefreshResponse {
        val loggingInterceptor =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        val client = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(ChuckerInterceptor(context))
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        val service = retrofit.create(ApiService::class.java)

        val tokenReq = TokenRequest(token!!)
        return service.refreshToken(API_KEY, tokenReq)
    }
}
