package com.megi.ecommerce.utils

sealed class ResultResponse<out T> {
    data object Loading : ResultResponse<Nothing>()
    data class Success<T>(val data: T) : ResultResponse<T>()
    data class Error(val exception: Exception) : ResultResponse<Nothing>()
}
