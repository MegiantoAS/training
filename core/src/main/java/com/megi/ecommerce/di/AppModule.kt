package com.megi.ecommerce.di

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.NotificationDao
import com.megi.ecommerce.datasource.local.room.dao.ProductDao
import com.megi.ecommerce.datasource.local.room.dao.WishListDao
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.repository.CartRepository
import com.megi.ecommerce.repository.NotificationRepository
import com.megi.ecommerce.repository.PreLoginRepository
import com.megi.ecommerce.repository.StoreRepository
import com.megi.ecommerce.repository.TransactionRepository
import com.megi.ecommerce.repository.WishlistRepository
import com.megi.ecommerce.utils.Constant.BASE_URL
import com.megi.ecommerce.utils.Interceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    // pre login
    @Singleton
    @Provides
    fun providePreLoginRepository(
        apiService: ApiService,
        sharedPreference: SharedPreference
    ): PreLoginRepository {
        return PreLoginRepository(apiService, sharedPreference)
    }

    // store
    @Singleton
    @Provides
    fun provideStore(apiService: ApiService): StoreRepository {
        return StoreRepository(apiService)
    }

    // wishlist
    @Singleton
    @Provides
    fun provideWishlistProduct(wishListDao: WishListDao): WishlistRepository {
        return WishlistRepository(wishListDao)
    }

    // cart
    @Singleton
    @Provides
    fun provideCartProduct(cartDao: ProductDao): CartRepository {
        return CartRepository(cartDao)
    }

    // transaction
    @Singleton
    @Provides
    fun provideTransaction(apiService: ApiService): TransactionRepository {
        return TransactionRepository(apiService)
    }

    // notification
    @Singleton
    @Provides
    fun provideNotification(notificationDao: NotificationDao): NotificationRepository {
        return NotificationRepository(notificationDao)
    }

    @Singleton
    @Provides
    fun providePref(@ApplicationContext context: Context): SharedPreference {
        return SharedPreference(context)
    }

    @Singleton
    @Provides
    fun getApiService(@ApplicationContext context: Context, pref: SharedPreference): ApiService {
        val loggingInterceptor =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        val client = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(ChuckerInterceptor(context))
            .authenticator(Interceptor(pref, context))
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        return retrofit.create(ApiService::class.java)
    }

    @Module
    @InstallIn(SingletonComponent::class)
    object DatabaseModule {

        @Provides
        @Singleton
        fun provideProductDatabase(@ApplicationContext context: Context): ProductDatabase {
            return ProductDatabase.getDatabase(context)
        }

        @Provides
        @Singleton
        fun provideWishListDao(productDatabase: ProductDatabase): WishListDao {
            return productDatabase.wishListDao()
        }

        @Provides
        @Singleton
        fun provideCartDao(productDatabase: ProductDatabase): ProductDao {
            return productDatabase.productDao()
        }

        @Provides
        @Singleton
        fun provideNotificationDao(productDatabase: ProductDatabase): NotificationDao {
            return productDatabase.notification()
        }
    }
}
