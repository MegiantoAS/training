package com.megi.ecommerce.datasource.local

import android.content.Context
import android.content.SharedPreferences

class SharedPreference(context: Context) {
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences("EcommercePref", Context.MODE_PRIVATE)

    // onboarding
    fun onBoarding(isFisrtInstall: Boolean) {
        val editor = sharedPreferences.edit()
        editor.putBoolean(INSTALL_APP, isFisrtInstall)
        editor.apply()
    }

    // first Install
    fun getIsFirstInstall(): Boolean {
        return sharedPreferences.getBoolean(INSTALL_APP, true)
    }

    // Access Token
    fun saveAccessToken(accessToken: String?, refreshToken: String?) {
        val editor = sharedPreferences.edit()
        editor.putString(ID_TOKEN, accessToken)
        editor.putString(ID_TOKEN_REFRESH, refreshToken)
        editor.apply()
    }

    // Refresh Token
    fun getRefreshToken(): String? {
        return sharedPreferences.getString(ID_TOKEN_REFRESH, null)
    }

    fun getAccessToken(): String? {
        return sharedPreferences.getString(ID_TOKEN, null)
    }

    // save name profile
    fun saveNameProfile(name: String) {
        val editor = sharedPreferences.edit()
        editor.putString(ID_NAME, name)
        editor.apply()
    }

    // get name profile
    fun getNameProfile(): String? {
        return sharedPreferences.getString(ID_NAME, null)
    }

    // get dark mode
    fun getDarkMode(): Boolean {
        return sharedPreferences.getBoolean("ID_DARK_THEME", false)
    }

    // get language
    fun getLanguage(): Boolean {
        return sharedPreferences.getBoolean("isLanguageIn", false)
    }

    // Save language
    fun saveLanguage(isLanguageIn: Boolean) {
        sharedPreferences.edit().putBoolean("isLanguageIn", isLanguageIn).apply()
    }

    // save dark mode
    fun saveDarkMode(isDarkTheme: Boolean) {
        val editor = sharedPreferences.edit()
        editor.putBoolean("ID_DARK_THEME", isDarkTheme)
        editor.apply()
    }

    // logout
    fun logout() {
        val editor = sharedPreferences.edit()
        editor.remove(ID_TOKEN)
        editor.remove(ID_TOKEN_REFRESH)
        editor.remove(ID_NAME)
        editor.apply()
    }

    companion object {
        private const val INSTALL_APP = "install_app"
        private const val ID_TOKEN = "id"
        private const val ID_TOKEN_REFRESH = "token_refresh"
        private const val ID_NAME = "name"
    }
}
