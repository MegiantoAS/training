package com.megi.ecommerce.datasource.network

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

data class GetProductResponse(
    val code: Int,
    val message: String,
    val data: GetProductsResultResponse,
)

data class GetProductsResultResponse(
    val itemsPerPage: Int,
    val currentItemCount: Int,
    val pageIndex: Int,
    val totalPages: Int,
    val items: List<GetProductsItemsResponse>,
)

data class GetProductsItemsResponse(
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: String,
    val brand: String,
    val store: String,
    val sale: Int,
    val productRating: Float,
)

data class ProductParam(
    val token: String,
    val search: String? = null,
    val brand: String? = null,
    val lowest: Int? = null,
    val highest: Int? = null,
    val sort: String? = null,
)

data class SearchResponse(
    val code: Int,
    val message: String,
    val data: List<String>,
)

data class GetProductDetailResponse(
    val code: Int,
    val message: String,
    val data: GetProductDetailItemResponse,
)

data class GetProductDetailItemResponse(
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: List<String>,
    val brand: String,
    val description: String,
    val store: String,
    val sale: Int,
    val stock: Int,
    val totalRating: Int,
    val totalReview: Int,
    val totalSatisfaction: Int,
    val productRating: Float,
    val productVariant: List<ProductVariant>,
)

data class ProductVariant(
    var variantName: String,
    val variantPrice: Int,
)

data class ProductReviewParam(
    val token: String,
    val productId: String? = null,
)

data class GetReview(
    val code: Int,
    val message: String,
    val data: List<GetProductReviewItemResponse>,
)

data class GetProductReviewItemResponse(
    val userName: String,
    val userImage: String,
    val userRating: Int,
    val userReview: String,
)

// payment
data class PaymentMethodResponse(
    val code: Int,
    val message: String,
    val data: List<PaymentMethodCategoryResponse>,
)

data class PaymentMethodCategoryResponse(
    val title: String,
    val item: List<PaymentMethodItemResponse>,
)

// rating
data class RatingReview(
    val invoiceId: String,
    val rating: Int? = null,
    val review: String? = null,
)

data class RatingProductResponse(
    val code: Int,
    val message: String,
)

// transaction
data class TransactionResponse(
    val code: Int,
    val message: String,
    val data: List<TransactionDataResponse>,
)

@Keep
@Parcelize
data class TransactionDataResponse(
    val invoiceId: String,
    val status: Boolean,
    val date: String,
    val time: String,
    val payment: String,
    val total: Int,
    val items: @RawValue List<PaymentItem>?,
    val rating: Int,
    val review: String?,
    val image: String,
    val name: String,
) : Parcelable

fun TransactionDataResponse.asPaymentDataResponse(
    review: String?,
    rating: Int?,
): PaymentDataResponse {
    return PaymentDataResponse(
        invoiceId,
        status,
        date,
        time,
        payment,
        total,
        review.toString(),
        rating
    )
}

fun GetProductDetailItemResponse.asWishlistProduct(
    variantName: String?,
    variantPrice: Int?,
): WishlistProduct {
    return WishlistProduct(
        productId,
        productName,
        productPrice,
        image.firstOrNull().toString(),
        brand,
        description,
        store,
        sale,
        stock,
        totalRating,
        totalReview,
        totalSatisfaction,
        productRating,
        variantName.toString(),
        variantPrice
    )
}

@Keep
@Entity
@Parcelize
data class CheckoutProduct(
    @PrimaryKey
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: String,
    val brand: String,
    val description: String,
    val store: String,
    val sale: Int,
    val stock: Int?,
    val totalRating: Int,
    val totalReview: Int,
    val totalSatisfaction: Int,
    val productRating: Float,
    var variantName: String,
    var variantPrice: Int?,
    var quantity: Int = 1,
    var selected: Boolean = false,
) : Parcelable

fun GetProductDetailItemResponse.asProductLocalDb(
    variantName: String?,
    variantPrice: Int?,
): ProductLocalDb {
    return ProductLocalDb(
        productId,
        productName,
        productPrice,
        image.firstOrNull().toString(),
        brand,
        description,
        store,
        sale,
        stock,
        totalRating,
        totalReview,
        totalSatisfaction,
        productRating,
        variantName.toString(),
        variantPrice
    )
}

fun ProductLocalDb.asCheckoutProduct(
    variantName: String?,
    variantPrice: Int?,
): CheckoutProduct {
    return CheckoutProduct(
        productId,
        productName,
        productPrice,
        image,
        brand,
        description,
        store,
        sale,
        stock,
        totalRating,
        totalReview,
        totalSatisfaction,
        productRating,
        variantName.toString(),
        variantPrice,
        quantity
    )
}
