package com.megi.ecommerce.datasource.local.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce

@Dao
interface NotificationDao {
    @Query("SELECT * FROM notification_table")
    fun getNotification(): LiveData<List<NotificationEcommerce>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addToNotification(notification: NotificationEcommerce)

    @Update
    suspend fun updateNotification(notification: NotificationEcommerce)
}
