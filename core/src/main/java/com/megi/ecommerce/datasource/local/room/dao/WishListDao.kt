package com.megi.ecommerce.datasource.local.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct

@Dao
interface WishListDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addWishList(wishList: WishlistProduct)

    @Query("SELECT * FROM WishlistProduct WHERE productId = :productId")
    fun getProductWishListById(productId: String): WishlistProduct?

    @Query("SELECT * FROM WishlistProduct")
    fun getWishListProduct(): LiveData<List<WishlistProduct>>

    @Query("DELETE FROM WishlistProduct WHERE WishlistProduct.productId= :id ")
    suspend fun deleteWishList(id: String): Int
}
