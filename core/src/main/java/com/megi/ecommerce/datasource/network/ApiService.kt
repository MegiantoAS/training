package com.megi.ecommerce.datasource.network

import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    // login
    @POST("login")
    suspend fun login(
        @Header("API_KEY") auth: String,
        @Body user: Auth,
    ): DataResponse

    // refresh Token
    @POST("refresh")
    suspend fun refreshToken(
        @Header("API_KEY") auth: String,
        @Body token: TokenRequest,
    ): RefreshResponse

    // register
    @POST("register")
    suspend fun register(
        @Header("API_KEY") auth: String,
        @Body user: Auth,
    ): DataResponse

    // profile
    @Multipart
    @POST("profile")
    suspend fun saveToProfile(
        @Header("Authorization") auth: String,
        @Part userName: MultipartBody.Part,
        @Part userImage: MultipartBody.Part?,
    ): ProfileResponse

    // store products
    @POST("products")
    suspend fun getProducts(
        @Header("Authorization") auth: String,
        @Query("search") search: String?,
        @Query("brand") brand: String?,
        @Query("lowest") lowest: Int?,
        @Query("highest") highest: Int?,
        @Query("sort") sort: String?,
        @Query("limit") limit: Int?,
        @Query("page") page: Int?,
    ): GetProductResponse

    // search
    @POST("search")
    suspend fun searchProduct(
        @Header("Authorization") auth: String,
        @Query("query") query: String,
    ): SearchResponse

    // detail product
    @GET("products/{id}")
    suspend fun getDetailProducts(
        @Header("Authorization") auth: String,
        @Path("id") id: String
    ): GetProductDetailResponse

    // review
    @GET("review/{id}")
    suspend fun getReviewProducts(
        @Header("Authorization") auth: String,
        @Path("id") id: String
    ): GetReview

    // payment
    @POST("fulfillment")
    suspend fun buyProducts(
        @Header("Authorization") auth: String,
        @Body payment: Payment,
    ): PaymentResponse

    @GET("payment")
    suspend fun getPaymentMethods(
        @Header("Authorization") auth: String,
    ): PaymentMethodResponse

    // rating
    @POST("rating")
    suspend fun getRating(
        @Header("Authorization") auth: String,
        @Body rating: RatingReview,
    ): RatingProductResponse

    // transaction
    @GET("transaction")
    suspend fun getTransactionHistory(
        @Header("Authorization") auth: String,
    ): TransactionResponse
}
