package com.megi.ecommerce.datasource.network

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

data class Auth(
    @SerializedName("email")
    val email: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("firebaseToken")
    val firebaseToken: String?,
)

data class DataResponse(
    @SerializedName("id")
    var code: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val data: ResultResponse?,
)

data class ErrorResponse(
    @SerializedName("code")
    var code: Int? = null,
    @SerializedName("message")
    val message: String? = null,
)

data class ResultResponse(
    @SerializedName("userName")
    val userName: String,
    @SerializedName("userImage")
    val userImage: String,
    @SerializedName("accessToken")
    val accessToken: String,
    @SerializedName("refreshToken")
    val refreshToken: String,
    @SerializedName("expiresAt")
    val expiresAt: Int,
)

data class TokenRequest(
    @field:SerializedName("token")
    val token: String?,
)

data class RefreshResponse(
    @field:SerializedName("code")
    val code: Int,
    @field:SerializedName("message")
    val message: String,
    @field:SerializedName("data")
    val data: RefreshDataResponse?,
)

data class RefreshDataResponse(
    @field:SerializedName("accessToken")
    val accessToken: String,
    @field:SerializedName("refreshToken")
    val refreshToken: String,
    @field:SerializedName("expiresAt")
    val expiresAt: Int,
)

data class ProfileResponse(
    @SerializedName("code")
    var code: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val data: ProfileResultResponse?,
)

data class ProfileResultResponse(
    @SerializedName("userName")
    val userName: String,
    @SerializedName("userImage")
    val userImage: String? = null,
)

// payment
@Parcelize
data class PaymentMethodItemResponse(
    val label: String,
    val image: String,
    val status: Boolean,
) : Parcelable

data class Payment(
    val payment: String,
    val items: @RawValue List<PaymentItem>,
)

@Parcelize
data class PaymentItem(
    val productId: String,
    val variantName: String,
    val quantity: Int,
) : Parcelable

@Parcelize
data class PaymentResponse(
    val code: Int,
    val message: String,
    val data: @RawValue PaymentDataResponse?,
) : Parcelable

@Parcelize
data class PaymentDataResponse(
    val invoiceId: String,
    val status: Boolean,
    val date: String,
    val time: String,
    val payment: String,
    val total: Int,
    val review: String?,
    val rating: Int?,
) : Parcelable
