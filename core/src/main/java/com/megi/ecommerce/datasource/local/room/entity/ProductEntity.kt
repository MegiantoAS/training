package com.megi.ecommerce.datasource.local.room.entity

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Keep
@Entity
@Parcelize
data class ProductLocalDb(
    @PrimaryKey
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: String,
    val brand: String,
    val description: String,
    val store: String,
    val sale: Int,
    val stock: Int?,
    val totalRating: Int,
    val totalReview: Int,
    val totalSatisfaction: Int,
    val productRating: Float,
    var variantName: String,
    var variantPrice: Int?,
    var quantity: Int = 1,
    var selected: Boolean = false,
) : Parcelable
