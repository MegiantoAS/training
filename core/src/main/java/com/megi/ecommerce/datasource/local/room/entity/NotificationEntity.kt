package com.megi.ecommerce.datasource.local.room.entity

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import kotlinx.parcelize.Parcelize

@Entity(tableName = "notification_table", primaryKeys = ["id"])
@Keep
@Parcelize
data class NotificationEcommerce(
    val id: String,
    val type: String,
    val date: String,
    val title: String,
    val body: String,
    val image: String,
    var isRead: Boolean,
) : Parcelable
