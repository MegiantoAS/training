package com.megi.ecommerce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.DataResponse
import com.megi.ecommerce.datasource.network.ProfileResponse
import com.megi.ecommerce.datasource.network.ProfileResultResponse
import com.megi.ecommerce.datasource.network.ResultResponse
import com.megi.ecommerce.utils.DummyRoom.requestAuth
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class PreLoginRepositoryTest {
    private lateinit var preLoginRepository: PreLoginRepository
    private lateinit var sharedPreference: SharedPreference
    private lateinit var apiService: ApiService

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        apiService = mock()
        sharedPreference = mock()
        preLoginRepository = PreLoginRepository(apiService, sharedPreference)
    }

    @Test
    fun loginTest() = runTest {
        val expected = DataResponse(
            code = 200,
            data = ResultResponse(
                "megi",
                "megi.com",
                "12345",
                "12345",
                600
            ),
            message = "Success"
        )

        whenever(apiService.login("123", requestAuth)).thenReturn(
            DataResponse(
                200,
                "Success",
                ResultResponse("megi", "megi.com", "12345", "12345", 600)
            )
        )
        val result = preLoginRepository.login("123", requestAuth)
        result.observeForever {
            if (it is com.megi.ecommerce.utils.ResultResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun registerTest() = runTest {
        val expected = DataResponse(
            code = 200,
            data = ResultResponse(
                "megi",
                "megi.com",
                "12345",
                "12345",
                600
            ),
            message = "Success"
        )

        whenever(apiService.register("123", requestAuth)).thenReturn(
            DataResponse(
                200,
                "Success",
                ResultResponse("megi", "megi.com", "12345", "12345", 600)
            )
        )
        val result = preLoginRepository.register("123", requestAuth)
        result.observeForever {
            if (it is com.megi.ecommerce.utils.ResultResponse.Success) {
                assertEquals(expected, it.data)
            }
        }
    }

    @Test
    fun saveProfile() = runTest {
        val expected = ProfileResponse(
            200,
            "OK",
            ProfileResultResponse(
                "megadi",
                "megi.com",
            )
        )

        val userNamePart = MultipartBody.Part.createFormData("userName", "megiadi")
        val userImagePart = MultipartBody.Part.createFormData("userImage", "megi.com")

        whenever(apiService.saveToProfile("1245", userNamePart, userImagePart)).thenReturn(
            ProfileResponse(200, "OK", ProfileResultResponse("megiadi", "megiadi"))
        )

        val result = preLoginRepository.saveToProfile("1235", userNamePart, userImagePart)

        result.observeForever {
            if (it is com.megi.ecommerce.utils.ResultResponse.Success) {
                assertEquals(expected.data, it.data)
            }
        }
    }
}
