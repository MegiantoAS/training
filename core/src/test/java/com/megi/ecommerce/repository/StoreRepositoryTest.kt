package com.megi.ecommerce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.SharedPreference
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.SearchResponse
import com.megi.ecommerce.utils.DummyRoom.detailExpected
import com.megi.ecommerce.utils.DummyRoom.reviewExpected
import com.megi.ecommerce.utils.ResultResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class StoreRepositoryTest {

    private lateinit var storeRepository: StoreRepository
    private lateinit var apiService: ApiService
    private lateinit var sharedPref: SharedPreference

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        apiService = mock()
        sharedPref = mock()
        storeRepository = StoreRepository(apiService)
    }

    @Test
    fun searchTest() {
        runBlockingTest {
            val mockSearchResponse = SearchResponse(200, "OK", listOf())
            whenever(apiService.searchProduct(anyString(), anyString())).thenReturn(
                mockSearchResponse
            )

            val result = storeRepository.searchProduct("mockToken", "mockQuery")

            val observer = object : Observer<SearchResponse> {
                override fun onChanged(searchResponse: SearchResponse) {
                    assert(searchResponse != null)
                }
            }

            result.observeForever(observer)
        }
    }

    @Test
    fun getProductDetailTest() = runTest {
        whenever(apiService.getDetailProducts("123", "12345")).thenReturn(detailExpected)
        val liveDataResult = storeRepository.getProductDetail("123", "12345")
        liveDataResult.observeForever {
            if (it is ResultResponse.Success) {
                assertEquals(detailExpected.data, it.data.data)
            }
        }
    }

    @Test
    fun `test getProductReview success`() = runTest {
        whenever(apiService.getReviewProducts("123", "1906")).thenReturn(reviewExpected)
        val liveDataResult = storeRepository.getReviewProduct("123", "1906")
        liveDataResult.observeForever {
            if (it is ResultResponse.Success) {
                assertEquals(reviewExpected.data, it.data.data)
            }
        }
    }
}
