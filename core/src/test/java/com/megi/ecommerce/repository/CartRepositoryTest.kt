package com.megi.ecommerce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.ProductDao
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.utils.DummyRoom.cartProduct
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class CartRepositoryTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var appDatabase: ProductDatabase
    private lateinit var cartDao: ProductDao
    private lateinit var cartRepository: CartRepository

    @Before
    fun setup() {
        appDatabase = mock()
        cartDao = mock()
        cartRepository = CartRepository(cartDao)
    }

    @Test
    fun addCartTest() = runTest {
        whenever(cartDao.addToCart(cartProduct)).thenReturn(Unit)
        val insert = cartRepository.addToCart(cartProduct)
        assertEquals(Unit, insert)
    }

    @Test
    fun getCartTest() = runTest {
        val product = listOf(cartProduct)
        val expected = MutableLiveData<List<ProductLocalDb>>()
        expected.value = product

        whenever(cartDao.getCartProducts()).thenReturn(expected)
        val checkData = cartRepository.getCartItem()
        checkData.observeForever {
            val expectedCartItem = expected.value?.firstOrNull()
            assertEquals(expectedCartItem, it.firstOrNull())
        }
    }

    @Test
    fun getProductCartByIdTest() = runTest {
        whenever(cartDao.getProductById("12345")).thenReturn(cartProduct)
        val actual = cartRepository.getCartById("12345")
        assertEquals(cartProduct, actual)
    }

    @Test
    fun deleteCartTest() = runTest {
        whenever(cartDao.removeFromCart("12345")).thenReturn(Unit)
        cartRepository.deleteFromCart("12345")
        val actual = cartRepository.deleteFromCart("12345")
        assertEquals(Unit, actual)
    }

    @Test
    fun deleteAllCartTest() {
        runBlocking {
            whenever(cartDao.removeFromCartAll(listOf("12345"))).thenReturn(Unit)
            cartRepository.removeFromCartAll(listOf("12345"))
            val actual = cartRepository.removeFromCartAll(listOf("12345"))
            assertEquals(Unit, actual)
        }
    }

    @Test
    fun updateCartTest() {
        runBlocking {
            whenever(cartDao.updateCartItemCheckbox(listOf("12345"), true)).thenReturn(Unit)
            cartRepository.updateCartItemCheckbox(listOf("12345"), true)
            val actual = cartRepository.updateCartItemCheckbox(listOf("12345"), true)
            assertEquals(Unit, actual)
        }
    }

    @Test
    fun updateCartItemQuantityTest() = runTest {
        runBlocking {
            whenever(cartDao.updateCartItemQuantity("12345", 4)).thenReturn(Unit)
            cartDao.updateCartItemQuantity("12345", 4)
            val checkData = cartDao.updateCartItemQuantity("12345", 4)
            assertEquals(Unit, checkData)
        }
    }
}
