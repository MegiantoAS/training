package com.megi.ecommerce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.Payment
import com.megi.ecommerce.datasource.network.PaymentItem
import com.megi.ecommerce.datasource.network.PaymentMethodCategoryResponse
import com.megi.ecommerce.datasource.network.PaymentMethodItemResponse
import com.megi.ecommerce.datasource.network.PaymentMethodResponse
import com.megi.ecommerce.datasource.network.RatingProductResponse
import com.megi.ecommerce.datasource.network.RatingReview
import com.megi.ecommerce.utils.DummyRoom.expectedResponseHistory
import com.megi.ecommerce.utils.DummyRoom.expectedTransaction
import com.megi.ecommerce.utils.ResultResponse
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class TransactionRepositoryTest {
    private lateinit var transactionRepository: TransactionRepository
    private lateinit var apiService: ApiService

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        apiService = mock()
        transactionRepository = TransactionRepository(apiService)
    }

    @Test
    fun buyProductTest() = runTest {
        whenever(
            apiService.buyProducts(
                "123",
                Payment(
                    "dummy",
                    listOf(PaymentItem("12345", "del", 5))
                )
            )
        ).thenReturn(expectedTransaction)

        val liveDataResult = transactionRepository.buyProducts(
            "123",
            Payment(
                "dummy",
                listOf(PaymentItem("12345", "del", 5))
            )
        )

        liveDataResult.observeForever {
            if (it is ResultResponse.Success) {
                Assert.assertEquals(expectedTransaction.data, it.data.data)
            }
        }
    }

    @Test
    fun ratingTest() = runTest {
        val expectedResponse = RatingProductResponse(
            code = 200,
            message = "good",
        )
        whenever(apiService.getRating("123", RatingReview("12345", 4, "barang bagus"))).thenReturn(
            expectedResponse
        )
        val liveDataResult =
            transactionRepository.ratingProduct("123", RatingReview("12345", 4, "barang bagus"))
        liveDataResult.observeForever {
            if (it is ResultResponse.Success) {
                Assert.assertEquals(expectedResponse.code, it.data.code)
            }
        }
    }

    @Test
    fun paymentMethodTest() = runTest {
        val expectedResponse = PaymentMethodResponse(
            code = 200,
            message = "Success",
            data = listOf(
                PaymentMethodCategoryResponse(
                    "dummy",
                    listOf(
                        PaymentMethodItemResponse(
                            "BRI",
                            "bri.com",
                            true
                        )
                    )
                )
            )
        )
        whenever(apiService.getPaymentMethods("123")).thenReturn(expectedResponse)
        val liveDataResult = transactionRepository.getPaymentMethods("123")
        liveDataResult.observeForever {
            if (it is ResultResponse.Success) {
                Assert.assertEquals(expectedResponse.data, it.data.data)
            }
        }
    }

    @Test
    fun `test getTransactionHistory success`() = runTest {
        whenever(apiService.getTransactionHistory("123")).thenReturn(expectedResponseHistory)
        val liveDataResult = transactionRepository.getTransaction("123")
        liveDataResult.observeForever {
            if (it is ResultResponse.Success) {
                Assert.assertEquals(expectedResponseHistory.data, it.data.data)
            }
        }
    }
}
