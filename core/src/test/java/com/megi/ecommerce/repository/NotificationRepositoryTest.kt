package com.megi.ecommerce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.NotificationDao
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce
import com.megi.ecommerce.utils.DummyRoom.notification
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class NotificationRepositoryTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var appDatabase: ProductDatabase
    private lateinit var notificationDao: NotificationDao
    private lateinit var notificationRepository: NotificationRepository

    @Before
    fun setup() {
        appDatabase = mock()
        notificationDao = mock()
        notificationRepository = NotificationRepository(notificationDao)
    }

    @Test
    fun testGetAllNotification() = runTest {
        // data
        val expected = MutableLiveData<List<NotificationEcommerce>>()
        expected.value = listOf(notification)

        // response dao
        whenever(notificationDao.getNotification()).thenReturn(expected)

        // notif repo
        notificationRepository.addNotification(notification)

        // get notif data from repository
        val getData = notificationRepository.getAllNotification()

        getData?.observeForever { observedNotifications ->
            val expectedNotification = expected.value?.firstOrNull()
            val observedNotification = observedNotifications.firstOrNull()
            assertEquals(expectedNotification, observedNotification)
        }
    }

    @Test
    fun updateNotificationTest() = runTest {
        whenever(notificationDao.updateNotification(notification)).thenReturn(Unit)
        val update = notificationRepository.updateNotification(notification)
        assertEquals(Unit, update)
    }
}
