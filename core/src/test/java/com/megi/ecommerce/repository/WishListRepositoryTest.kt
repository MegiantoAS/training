package com.megi.ecommerce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.megi.ecommerce.MainDispatcherRule
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.WishListDao
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import com.megi.ecommerce.utils.DummyRoom.productWishlist
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class WishListRepositoryTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var appDatabase: ProductDatabase
    private lateinit var wishListDao: WishListDao
    private lateinit var wishlistRepository: WishlistRepository

    @Before
    fun setup() {
        appDatabase = mock()
        wishListDao = mock()
        wishlistRepository = WishlistRepository(wishListDao)
    }

    @Test
    fun addWishlistTest() = runTest {
        whenever(wishListDao.addWishList(productWishlist)).thenReturn(Unit)
        val actual = wishlistRepository.addWishList(productWishlist)
        assertEquals(Unit, actual)
    }

    @Test
    fun getProductWishlistByIdTest() = runTest {
        whenever(wishListDao.getProductWishListById("12345")).thenReturn(productWishlist)
        val actual = wishlistRepository.getProductWishlistById("12345")
        assertEquals(productWishlist, actual)
    }

    @Test
    fun getWishListProduct() = runTest {
        val productWishlist = listOf(productWishlist)
        val expected = MutableLiveData<List<WishlistProduct>>()
        expected.value = productWishlist

        whenever(wishListDao.getWishListProduct()).thenReturn(expected)
        val actual = wishlistRepository.getWishlistProduct()
        actual.observeForever {
            val expectedWishlistItem = expected.value!!.firstOrNull()
            assertEquals(expectedWishlistItem, it.firstOrNull())
        }
    }

    @Test
    fun deleteWishlistTest() = runTest {
        whenever(wishListDao.deleteWishList("12345")).thenReturn(productWishlist.productId.length)
        wishlistRepository.deleteWishList("12345")
        val actual = wishlistRepository.deleteWishList("12345")
        assertEquals(Unit, actual)
    }
}
