package com.megi.ecommerce.utils

import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce
import com.megi.ecommerce.datasource.local.room.entity.ProductLocalDb
import com.megi.ecommerce.datasource.local.room.entity.WishlistProduct
import com.megi.ecommerce.datasource.network.Auth
import com.megi.ecommerce.datasource.network.GetProductDetailItemResponse
import com.megi.ecommerce.datasource.network.GetProductDetailResponse
import com.megi.ecommerce.datasource.network.GetProductReviewItemResponse
import com.megi.ecommerce.datasource.network.GetReview
import com.megi.ecommerce.datasource.network.PaymentDataResponse
import com.megi.ecommerce.datasource.network.PaymentItem
import com.megi.ecommerce.datasource.network.PaymentResponse
import com.megi.ecommerce.datasource.network.ProductVariant
import com.megi.ecommerce.datasource.network.TransactionDataResponse
import com.megi.ecommerce.datasource.network.TransactionResponse

object DummyRoom {
    val cartProduct = ProductLocalDb(
        productId = "12345",
        productName = "Sample Product",
        productPrice = 1000,
        image = "sample_image_url.jpg",
        brand = "Sample Brand",
        description = "This is a sample product description.",
        store = "Sample Store",
        sale = 10,
        stock = 50,
        totalRating = 4,
        totalReview = 20,
        totalSatisfaction = 90,
        productRating = 4.5f,
        variantName = "Sample Variant",
        variantPrice = 1200,
        quantity = 2,
        selected = true
    )

    val notification = NotificationEcommerce(
        "123",
        "promo",
        "24",
        "dummy",
        "dummy",
        "img.com",
        false
    )

    val productWishlist = WishlistProduct(
        productId = "12345",
        productName = "Sample Product",
        productPrice = 1000,
        image = "sample_image_url.jpg",
        brand = "Sample Brand",
        description = "This is a sample product description.",
        store = "Sample Store",
        sale = 10,
        stock = 50,
        totalRating = 4,
        totalReview = 20,
        totalSatisfaction = 90,
        productRating = 4.5f,
        variantName = "Sample Variant",
        variantPrice = 1200,
        quantity = 2,
    )

    // repository
    val requestAuth = Auth(
        "megi@gmail.com",
        "12345",
        "12345"
    )

    // store
    val detailExpected = GetProductDetailResponse(
        code = 200,
        message = "Success",
        data = GetProductDetailItemResponse(
            productId = "12345",
            productName = "Asus",
            productPrice = 999,
            image = listOf(
                "https://Asus.com/image1.jpg"
            ),
            brand = "Sample Brand",
            description = "This is a sample product description.",
            store = "Sample Store",
            sale = 20,
            stock = 100,
            totalRating = 4,
            totalReview = 50,
            totalSatisfaction = 90,
            productRating = 4.5f,
            productVariant = listOf(
                ProductVariant("dummy", 1000),
            )
        )
    )

    val reviewExpected = GetReview(
        code = 200,
        message = "good",
        data = listOf(
            GetProductReviewItemResponse(
                "megi",
                "b1.png",
                4,
                "good"
            )
        )
    )

    //
    val expectedTransaction = PaymentResponse(
        code = 200,
        message = "good",
        data = PaymentDataResponse(
            invoiceId = "Del12345",
            status = true,
            date = "2023-12-22",
            time = "14:30:00",
            payment = "Credit Card",
            total = 100,
            review = "good",
            rating = 4
        )
    )

    val expectedResponseHistory = TransactionResponse(
        code = 200,
        message = "success",
        data = listOf(
            TransactionDataResponse(
                invoiceId = "Del123",
                status = true,
                date = "2023-12-25",
                time = "08:20:00",
                payment = "Credit Card",
                total = 100,
                items = listOf(
                    PaymentItem("Item1", "16GB", 2),
                    PaymentItem("Item2", "16GB", 1),
                    PaymentItem("Item3", "16GB", 3)
                ),
                rating = 4,
                review = "review",
                image = "https://abc.com/image.jpg",
                name = "John Doe"
            )
        )
    )
}
