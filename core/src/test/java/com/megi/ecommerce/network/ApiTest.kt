package com.megi.ecommerce.network

import com.megi.ecommerce.datasource.network.ApiService
import com.megi.ecommerce.datasource.network.Auth
import com.megi.ecommerce.datasource.network.DataResponse
import com.megi.ecommerce.datasource.network.GetProductDetailItemResponse
import com.megi.ecommerce.datasource.network.GetProductDetailResponse
import com.megi.ecommerce.datasource.network.GetProductResponse
import com.megi.ecommerce.datasource.network.GetProductReviewItemResponse
import com.megi.ecommerce.datasource.network.GetProductsItemsResponse
import com.megi.ecommerce.datasource.network.GetProductsResultResponse
import com.megi.ecommerce.datasource.network.GetReview
import com.megi.ecommerce.datasource.network.Payment
import com.megi.ecommerce.datasource.network.PaymentDataResponse
import com.megi.ecommerce.datasource.network.PaymentItem
import com.megi.ecommerce.datasource.network.PaymentMethodCategoryResponse
import com.megi.ecommerce.datasource.network.PaymentMethodItemResponse
import com.megi.ecommerce.datasource.network.PaymentMethodResponse
import com.megi.ecommerce.datasource.network.PaymentResponse
import com.megi.ecommerce.datasource.network.ProductVariant
import com.megi.ecommerce.datasource.network.ProfileResponse
import com.megi.ecommerce.datasource.network.ProfileResultResponse
import com.megi.ecommerce.datasource.network.RatingProductResponse
import com.megi.ecommerce.datasource.network.RatingReview
import com.megi.ecommerce.datasource.network.RefreshDataResponse
import com.megi.ecommerce.datasource.network.RefreshResponse
import com.megi.ecommerce.datasource.network.ResultResponse
import com.megi.ecommerce.datasource.network.SearchResponse
import com.megi.ecommerce.datasource.network.TokenRequest
import com.megi.ecommerce.datasource.network.TransactionDataResponse
import com.megi.ecommerce.datasource.network.TransactionResponse
import com.megi.ecommerce.utils.ConvertJSON
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class ApiTest {
    private lateinit var mockWebServer: MockWebServer
    private lateinit var apiService: ApiService

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        apiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @After
    fun close() {
        mockWebServer.shutdown()
    }

    @Test
    fun loginTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("login.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.login(
            "123",
            Auth(
                email = "megi@gmail.com",
                password = "190501",
                firebaseToken = "23232"
            )
        )
        val expected = DataResponse(
            code = 200,
            message = "OK",
            ResultResponse(
                userName = "megi",
                userImage = "abc.com",
                accessToken = "123",
                refreshToken = "23232",
                expiresAt = 600
            )
        )
        assertEquals(expected.data, actual.data)
    }

    @Test
    fun registerTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("register.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.register("12345678", Auth("megi@gmail.com", "1234", "123"))
        val expected = DataResponse(
            code = 200,
            message = "OK",
            ResultResponse(
                userName = "megi",
                userImage = "abc.com",
                accessToken = "123",
                refreshToken = "23232",
                expiresAt = 600
            )
        )

        assertEquals(expected.data, actual.data)
    }

    @Test
    fun refreshTokenTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("refreshToken.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.refreshToken("123", TokenRequest("1234567"))
        val expected = RefreshResponse(
            code = 200,
            message = "OK",
            RefreshDataResponse(
                accessToken = "12345",
                refreshToken = "190501",
                expiresAt = 600
            )
        )
        assertEquals(expected, actual)
    }

    @Test
    fun saveProfile() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("saveProfile.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.saveToProfile(
            auth = "123",
            MultipartBody.Part.createFormData("userName", "megi"),
            MultipartBody.Part.createFormData("userImage", "photo.com")
        )
        val expected = ProfileResponse(
            code = 200,
            message = "OK",
            ProfileResultResponse(
                userName = "megi",
                userImage = "photo.com"
            )
        )
        assertEquals(expected.data, actual.data)
    }

    @Test
    fun getProductTest() = runTest {
        // fake response
        val mockedResponse = ConvertJSON.createMockResponse("product.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.getProducts(
            auth = "123",
            "",
            "",
            null,
            null,
            "",
            null,
            null
        )

        val expected = GetProductResponse(
            code = 200,
            message = "OK",
            GetProductsResultResponse(
                itemsPerPage = 10,
                currentItemCount = 10,
                pageIndex = 1,
                totalPages = 2,
                listOf(
                    GetProductsItemsResponse(
                        productId = "29",
                        productName = "macbook pro m2",
                        productPrice = 25000000,
                        image = "mac.com",
                        brand = "Apple",
                        store = "Ibox",
                        sale = 1,
                        productRating = 4F
                    )
                )
            )
        )
        assertEquals(expected, actual)
    }

    @Test
    fun searchProductTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("search.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.searchProduct("12345", "Macbook")

        val expected = SearchResponse(
            code = 200,
            message = "OK",
            listOf(
                "Macbook"
            )
        )

        assertEquals(expected, actual)
    }

    @Test
    fun detailProductTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("detail.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.getDetailProducts("12345", "1909")

        val expected = GetProductDetailResponse(
            code = 200,
            message = "OK",
            GetProductDetailItemResponse(
                "A-123",
                "Acer",
                25000000,
                listOf(
                    "acer1.com",
                    "acer2.com"
                ),
                "Acer",
                "ACER",
                "acerStore",
                18,
                5,
                4,
                6,
                9,
                4F,
                productVariant = listOf(
                    ProductVariant("16GB", 0),
                    ProductVariant("32GB", 5000)
                )
            )
        )
        assertEquals(expected, actual)
    }

    @Test
    fun reviewTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("review.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.getReviewProducts("1234", "125")
        val expected = GetReview(
            code = 200,
            message = "OK",
            listOf(
                GetProductReviewItemResponse("John", "image1.com", 1, "Lorem Ipsum"),
                GetProductReviewItemResponse("Doe", "image2.com", 5, "Lorem Ipsum"),
            )
        )
        assertEquals(expected, actual)
    }

    @Test
    fun buyProductTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("buyProduct.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.buyProducts(
            "1234",
            Payment("123", listOf(PaymentItem("133", "121", 6)))
        )

        val expected = PaymentResponse(
            code = 200,
            message = "OK",
            PaymentDataResponse(
                "invoiceId",
                true,
                "23 Nov 2023",
                "10:05",
                "Bank BRI",
                12000000,
                null,
                null
            )
        )
        assertEquals(expected, actual)
    }

    @Test
    fun paymentMethodTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("paymentMethod.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.getPaymentMethods("12345")

        val expected = PaymentMethodResponse(
            code = 200,
            message = "OK",
            listOf(
                PaymentMethodCategoryResponse(
                    "Transfer Virtual Account",
                    listOf(
                        PaymentMethodItemResponse("BNI Virtual Account", "bni.com", true),
                        PaymentMethodItemResponse("BRI Virtual Account", "bri.com", true)
                    )
                ),
                PaymentMethodCategoryResponse(
                    "Transfer Bank",
                    listOf(
                        PaymentMethodItemResponse("Bank BCA", "bca.com", true),
                        PaymentMethodItemResponse("Bank BNI", "bni.com", true)
                    )
                ),
                PaymentMethodCategoryResponse(
                    "Pembayaran Instan",
                    listOf(
                        PaymentMethodItemResponse("GoPay", "gopay.com", true),
                        PaymentMethodItemResponse("OVO", "ovo.com", true)

                    )
                )
            )
        )
        assertEquals(expected, actual)
    }

    @Test
    fun ratingTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("rating.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.getRating("", RatingReview("1234", 3, "good"))

        val expected = RatingProductResponse(
            code = 200,
            message = "Fulfillment rating and review success",
        )
        assertEquals(expected, actual)
    }

    @Test
    fun historyTest() = runTest {
        val mockedResponse = ConvertJSON.createMockResponse("history.json")
        mockWebServer.enqueue(mockedResponse)

        val actual = apiService.getTransactionHistory("1123")

        val expected = TransactionResponse(
            code = 200,
            message = "OK",
            listOf(
                TransactionDataResponse(
                    "invoiceId",
                    true,
                    "23 Nov 2023",
                    "10:15",
                    "Bank BRI",
                    15000000,
                    listOf(
                        PaymentItem("12345", "16GB", 2)
                    ),
                    4,
                    "good",
                    "acer.com",
                    "acer"
                )
            )
        )
        assertEquals(expected, actual)
    }
}
