package com.megi.ecommerce.local

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.megi.ecommerce.datasource.local.room.ProductDatabase
import com.megi.ecommerce.datasource.local.room.dao.NotificationDao
import com.megi.ecommerce.datasource.local.room.dao.ProductDao
import com.megi.ecommerce.datasource.local.room.dao.WishListDao
import com.megi.ecommerce.datasource.local.room.entity.NotificationEcommerce
import com.megi.ecommerce.utils.DummyRoom.cartProduct
import com.megi.ecommerce.utils.DummyRoom.notification
import com.megi.ecommerce.utils.DummyRoom.productWishlist
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class RoomTest {
    private lateinit var context: Context
    private lateinit var notificationDao: NotificationDao
    private lateinit var cartDao: ProductDao
    private lateinit var wishListDao: WishListDao
    private lateinit var appDatabase: ProductDatabase

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        appDatabase = Room.inMemoryDatabaseBuilder(context, ProductDatabase::class.java)
            .allowMainThreadQueries()
            .build()

        notificationDao = appDatabase.notification()
        cartDao = appDatabase.productDao()
        wishListDao = appDatabase.wishListDao()
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    // notification Test
    @Test
    fun insertAndGetNotification() = runTest {
        notificationDao.addToNotification(notification)
        notificationDao.getNotification().observeForever {
            assertEquals(listOf(notification), it)
        }
    }

    @Test
    fun updateNotificationTest() = runTest {
        notificationDao.addToNotification(notification)
        notificationDao.updateNotification(
            NotificationEcommerce(
                "123",
                "promo",
                "24",
                "dummy",
                "dummy",
                "img.com",
                true
            )
        )
        notificationDao.getNotification().observeForever {
            it.forEach {
                assertEquals(true, it.isRead)
            }
        }
    }

    // cart test
    @Test
    fun addCartTest() = runTest {
        cartDao.addToCart(cartProduct)
        cartDao.getCartProducts().observeForever {
            assertEquals(listOf(cartProduct), it)
        }
    }

    @Test
    fun getProductByIdTest() = runTest {
        cartDao.addToCart(cartProduct)
        val productById = cartDao.getProductById("12345")
        assertEquals(cartProduct, productById)
    }

    @Test
    fun itemQuantityTest() = runTest {
        cartDao.addToCart(cartProduct)
        cartDao.updateCartItemCheckbox(listOf("12345"), true)
        val updateQuantity = cartDao.getProductById("12345")
        assertEquals(true, updateQuantity?.selected)
    }

    @Test
    fun deleteCartTest() {
        runTest {
            cartDao.addToCart(cartProduct)
            cartDao.removeFromCart("12345")
            cartDao.getCartProducts().observeForever {
                assertEquals(0, it.size)
            }
        }
    }

    @Test
    fun deleteAllCartTest() = runTest {
        cartDao.addToCart(cartProduct)
        cartDao.removeFromCartAll(listOf("12345"))
        cartDao.getCartProducts().observeForever {
            assertEquals(0, it.size)
        }
    }

    // wishlistTest
    @Test
    fun insertAndGetWishlistTest() = runTest {
        wishListDao.addWishList(productWishlist)
        wishListDao.getWishListProduct().observeForever {
            assertEquals(listOf(productWishlist), it)
        }
    }

    @Test
    fun getProductWishlistByIdTest() {
        runTest {
            wishListDao.addWishList(productWishlist)
            val checkWishlist = wishListDao.getProductWishListById("12345")
            assertEquals(productWishlist, checkWishlist)
        }
    }

    @Test
    fun deleteWishlistTest() {
        runTest {
            wishListDao.addWishList(productWishlist)
            wishListDao.deleteWishList("12345")
            wishListDao.getWishListProduct().observeForever {
                assertEquals(0, it.size)
            }
        }
    }
}
