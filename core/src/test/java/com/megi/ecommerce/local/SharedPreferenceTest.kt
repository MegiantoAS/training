package com.megi.ecommerce.local

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.megi.ecommerce.datasource.local.SharedPreference
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class SharedPreferenceTest {

    private lateinit var sharedPref: SharedPreference

    @Before
    fun setTup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        sharedPref = SharedPreference(context)
    }

    @Test
    fun testFirstInstall() {
        sharedPref.onBoarding(true)
        val actual = sharedPref.getIsFirstInstall()
        assertEquals(true, actual)
    }

    @Test
    fun darkModeTest() {
        sharedPref.saveDarkMode(true)
        val actual = sharedPref.getDarkMode()
        assertEquals(true, actual)
    }

    @Test
    fun languageTest() {
        sharedPref.saveLanguage(true)
        val actual = sharedPref.getLanguage()
        assertEquals(true, actual)
    }

    @Test
    fun logoutTest() {
        sharedPref.saveAccessToken("12345", "2133")
        sharedPref.logout()
        val actual = sharedPref.getAccessToken()
        assertEquals(null, actual)
    }

    @Test
    fun setNameTest() {
        sharedPref.saveNameProfile("megianto")
        val actual = sharedPref.getNameProfile()
        assertEquals("megianto", actual)
    }

    @Test
    fun tokenAccessTest() {
        sharedPref.saveAccessToken("12345", "12345")
        val actual = sharedPref.getAccessToken()
        assertEquals("12345", actual)
    }

    @Test
    fun getDataTest() {
        sharedPref.saveAccessToken("123", "112")
        sharedPref.saveDarkMode(true)
        sharedPref.saveNameProfile("megi")
        sharedPref.onBoarding(true)

        val checkAccessTokenData = sharedPref.getAccessToken()
        val checkRefreshTokenData = sharedPref.getRefreshToken()
        val checkDarkThemeData = sharedPref.getDarkMode()
        val checkNameProfileData = sharedPref.getNameProfile()
        val checkFirstInstallData = sharedPref.getIsFirstInstall()

        assertEquals("123", checkAccessTokenData)
        assertEquals("112", checkRefreshTokenData)
        assertEquals(true, checkDarkThemeData)
        assertEquals("megi", checkNameProfileData)
        assertEquals(true, checkFirstInstallData)
    }
}
